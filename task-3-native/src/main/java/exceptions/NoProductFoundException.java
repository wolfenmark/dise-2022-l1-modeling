/**
 * 
 */
package exceptions;

/**
 * @author wolfenmark and tabholt
 *
 */
public class NoProductFoundException extends Exception {
	public NoProductFoundException(String error) {
		super(error);
	}
}
