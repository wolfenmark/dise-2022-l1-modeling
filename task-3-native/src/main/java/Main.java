import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import exceptions.NoProductFoundException;
import exceptions.NoSupplierFoundException;
import pojo.Customer;
import pojo.ProductWithDetails;
import pojo.Supplier;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.ScanParams;
import redis.clients.jedis.resps.ScanResult;

/**
 * Native DB interfaces-based implementation of the required searches after data migration.
 */

/**
 * @author wolfenmark and tabholt
 *
 */
public class Main {

	// DB Connection parameters
	static final String MYSQL_DB_URL = "jdbc:mysql://hydra.unamurcs.be:33062/reldata";
	static final String MYSQL_USER = "root";
	static final String MYSQL_PASS = "password";

	static final String MONGO_DB_URL = "hydra.unamurcs.be";
	static final int MONGO_DB_PORT = 27012;
	static final String MONGO_DB_NAME = "myMongoDB";
	
	static final String REDIS_DB_URL = "hydra.unamurcs.be";
	static final int REDIS_DB_PORT = 63792;
	static final String redisKeypattern = "ORDER:*";
	static final int employeeRefIndexInRedis = 11;
	static final int customerRefIndexInRedis = 10;
	
	// "Shared" DB connection objects
	private static Connection mySQLConnection = null;
	private static MongoClient mongoDBConnection = null;
	private static MongoDatabase mongoDatabase = null;
	private static Jedis redisDBConnection = null;
	
	
	/**
	 * Return a Document containing the Supplier of the product with name productName
	 * We assume that product names are unique, thus only one or no Supplier is returned
	 * @param productName: the name of the product
	 * @return a Supplier if found, null otherwise
	 * @throws NoProductFoundException if no product with the given name is found
	 * @throws NoSupplierFoundException if no supplier is found for the product with the given name
	 */
	public static Supplier findSupplierByProductName(String productName) throws NoProductFoundException, NoSupplierFoundException {
		String query = "SELECT SupplierRef FROM Products WHERE ProductName = ?";
		Integer supplierID = null;
		try {
			PreparedStatement stmt = mySQLConnection.prepareStatement(query);
			stmt.setString(1, productName);
			ResultSet rs = stmt.executeQuery();

			// Extract data from result set, since we assumed max one product with the same name, we only check the first result
			if(rs.next() == false) {
				throw new NoProductFoundException("Product \"" + productName + "\" could not be found.");
			} else {
				supplierID = rs.getInt("SupplierRef");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		MongoCollection<Document> collection = mongoDatabase.getCollection("Suppliers");
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("SupplierID", supplierID);
		FindIterable<Document> documentCursor = collection.find(searchQuery);

		// If no supplier is found return null
		MongoCursor<Document> mongoCursor = documentCursor.iterator();
		Document doc = mongoCursor.tryNext();
		if (doc == null) {
			throw new NoSupplierFoundException("Supplier \"" + supplierID + "\" could not be found.");
		} else {
			assert(mongoCursor.hasNext() == false);
			return new Supplier(doc);
		}

	}

	/**
	 * Returns a list of ids of all the employees with first name firstName
	 * @param firstName first name of the employee
	 * @return list of employee ids
	 */
	public static ArrayList<Integer> getEmployeesIDByFirstName(String firstName) {
		String query = "SELECT EmployeeID FROM Employees WHERE FirstName = ?";
		ArrayList<Integer> ids = new ArrayList<Integer>();
		try {
			PreparedStatement stmt = mySQLConnection.prepareStatement(query);
			stmt.setString(1, firstName);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				ids.add(rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ids;
	}

	/**
	 * Return a map of order keys and customer refs with for each order its customer, where the order has been encoded by the employee with id employeeID
	 * @param employeeID the id of the employee who encoded the order
	 * @return a map of order keys to customer refs
	 */
	public static Map<String, String> getAllCustomerRefsInOrdersByEmployee(Integer employeeID) {
		
		ScanParams scanParams = new ScanParams().match(redisKeypattern).count(100);
		String cursor = ScanParams.SCAN_POINTER_START;
		boolean hasMoreResults = true;
		Map<String, String> results = new HashMap<String, String>();
	    while (hasMoreResults) {
	    	ScanResult<String> scanResult = redisDBConnection.scan(cursor, scanParams);
	    	List<String> keys = scanResult.getResult();
			for (String key : keys) {
				
	    		List<String> refs = redisDBConnection.hmget(key, "CustomerRef", "EmployeeRef");
	    		String customerRef = refs.get(0);
	    		String employeeRef = refs.get(1);
	    		if(Integer.parseInt(employeeRef) == employeeID) {
	    			results.put(key, customerRef);
	    		}
	    	}
			cursor = scanResult.getCursor();
			if (cursor.equals("0")) {
				hasMoreResults = false;
			}
	    }
	    return results;
	}
	
	/**
	 * Return a list of customers from MongoDB where the customer ids are in the list of customerRefs
	 * @param customerRefs list of customerIDs to search for in the Customers collection
	 * @return a list of customers
	 */
	public static List<Customer> getCustomersByRefs(List<String> customerRefs) {
		MongoCollection<Document> collection = mongoDatabase.getCollection("Customers");
		BasicDBObject searchQuery = new BasicDBObject();
		
		searchQuery.put("CustomerID", new BasicDBObject("$in", customerRefs));
		FindIterable<Document> documentCursor = collection.find(searchQuery);

		// If no supplier is found return an empty list
		MongoCursor<Document> mongoCursor = documentCursor.iterator();
		List<Customer> results = new LinkedList<Customer>();
		while (mongoCursor.hasNext()) {
			Document doc = mongoCursor.next();
			results.add(new Customer(doc));
		}
		return results;
	}
	
	/**
	 * Return a list of products with additional details for each product, based on the order whose id is 'orderID'.
	 * @param orderID the id of the order of interest
	 * @return a list of products with details
	 */
	public static List<ProductWithDetails> getProductDetailsByOrderID(Integer orderID) {
//		String query = "SELECT ProductRef, UnitPrice, Quantity, Discount FROM Order_Details WHERE OrderRef = ?";
		String innerQuery = "SELECT ProductID, ProductName, SupplierRef, QuantityPerUnit, UnitPrice, UnitsInStock, "
				+ "UnitsOnOrder, ReorderLevel, Discontinued from Products WHERE ProductID = ?";
		List<ProductWithDetails> productDetails = new ArrayList<ProductWithDetails>();
		try {

			
			MongoCollection<Document> collection = mongoDatabase.getCollection("Customers");
			BasicDBObject searchQuery = new BasicDBObject();
//			BasicDBObject projection = new BasicDBObject();
			
			searchQuery.put("orders.OrderID", orderID);
			
//			projection.put("orders.$", orderID);
			FindIterable<Document> documentCursor = collection.find(searchQuery);

			// If no supplier is found return null
			MongoCursor<Document> mongoCursor = documentCursor.iterator();
			while (mongoCursor.hasNext()) {
				Document doc = mongoCursor.next();
				List<Document> orders = doc.getList("orders", Document.class);
				for (Document order : orders) {
					if (order.getInteger("OrderID").equals(orderID)) {
						List<Document> products = order.getList("products", Document.class);
						for (Document product : products) {
							ProductWithDetails productWithDetails = new ProductWithDetails();
							
							productWithDetails.OrderUnitPrice = product.getDouble("UnitPrice").floatValue();
							productWithDetails.Quantity = product.getInteger("Quantity");
							productWithDetails.Discount = product.getDouble("Discount");
							Integer productRef = product.getInteger("ProductID");
							PreparedStatement stmt2 = mySQLConnection.prepareStatement(innerQuery);
							stmt2.setInt(1, productRef);
							ResultSet innerRs = stmt2.executeQuery();
							innerRs.next();
							productWithDetails.ProductID = innerRs.getInt(1);
							productWithDetails.ProductName = innerRs.getString(2);
							productWithDetails.SupplierRef = innerRs.getInt(3);
							productWithDetails.QuantityPerUnit = innerRs.getString(4);
							productWithDetails.ProductUnitPrice = innerRs.getFloat(5);
							productWithDetails.UnitsInStock = innerRs.getInt(6);
							productWithDetails.UnitsOnOrder = innerRs.getInt(7);
							productWithDetails.ReorderLevel = innerRs.getInt(8);
							productWithDetails.Discontinued = innerRs.getBoolean(9);
							
							productDetails.add(productWithDetails);
						}
					}
				}
				


				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return productDetails;
	}

	// TODO Database connection initialization and cleanup should be done with a finer granularity, depending on the specific case

	/**
	 * Initalize all the connection objects of the used databases
	 */
	private static void initializeDatabases() {
		try {
			mySQLConnection = DriverManager.getConnection(MYSQL_DB_URL, MYSQL_USER, MYSQL_PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		mongoDBConnection = new MongoClient(MONGO_DB_URL, MONGO_DB_PORT);
		mongoDatabase = mongoDBConnection.getDatabase(MONGO_DB_NAME);
		
		redisDBConnection = new Jedis(REDIS_DB_URL, REDIS_DB_PORT);
	}
	
	/**
	 * Properly dispose of initialized connections to databases
	 */
	private static void closeDatabases() {
		try {
			if (mySQLConnection != null) {
//				System.out.println("MySQLConnection closed");
				mySQLConnection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		if (mongoDBConnection != null) {
//			System.out.println("MongoDBConnection closed");
			mongoDBConnection.close();
		}
		if (redisDBConnection != null) {
//			System.out.println("RedisDBConnection closed");
			redisDBConnection.close();
		}
	}

	/**
	 * @param args
	 * Native interface access to retrieve Task 2 objects
	 */
	public static void main(String[] args) {
		
		try {
			initializeDatabases();
			// Returns a Supplier object containing all information about the supplier of the
			// product with the name "Escargots de Bourgogne"
			try {
				Supplier supplier = findSupplierByProductName("Escargots de Bourgogne");
				System.out.println("\nThe supplier of Escargots de Bourgogne is:");
				System.out.println(supplier.toString() + "\n");
			} catch(Exception e) {
				e.printStackTrace();
			}
				
			// Returns a List of Customers objects containing all Customers which have
			// made an Order encoded by Employee with firstname “Margaret”
			ArrayList<Integer> margarets = getEmployeesIDByFirstName("Margaret");
			Set<String> uniqueCustomerRefs = new HashSet<String>();
			for ( Integer margaret : margarets ) {
				Map<String, String> customerRefs = getAllCustomerRefsInOrdersByEmployee(margaret);
				uniqueCustomerRefs.addAll(customerRefs.values());
			}
			List<Customer> customers = getCustomersByRefs(new ArrayList<String>(uniqueCustomerRefs));
			System.out.println("\nThe customers for whom at least one order has been encoded by Margaret are:");
			for (Customer customer : customers) {
				System.out.println(customer.toString());
			}
			System.out.println("");
			
			// Get all detailed Products of Order with ID 10266
			List<ProductWithDetails> productWithDetails = getProductDetailsByOrderID(10266);
			System.out.println("\nThe products (with details) of the order with ID 10266 are:");
			for (ProductWithDetails product : productWithDetails) {
				System.out.println(product.toString());
			}
			System.out.println("");
		} finally {
			closeDatabases();
		}
	}

}
