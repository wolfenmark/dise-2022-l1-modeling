package pojo;

import org.bson.Document;

public class Supplier {
	public Integer supplierID;
	public String address;
	public String city;
	public String companyName;
	public String contactName;
	public String contactTitle;
	public String country;
	public String fax;
	public String homePage;
	public String phone;
	public String postalCode;
	public String region;
	
	public Supplier(Document doc) {
		this.address = doc.getString("Address");
		this.city = doc.getString("City");
		this.companyName = doc.getString("CompanyName");
		this.contactName = doc.getString("ContactName");
		this.contactTitle = doc.getString("ContactTitle");
		this.country = doc.getString("Country");
		this.supplierID = doc.getInteger("SupplierID");
		this.fax = doc.getString("Fax");
		this.homePage = doc.getString("HomePage");
		this.phone = doc.getString("Phone");
		this.postalCode = doc.getString("PostalCode");
		this.region = doc.getString("Region");
	}

	public String toString() {
		return "Supplier: { SupplierID: " + this.supplierID + ", " +
				"Address: " + this.address + ", " +
				"City: " + this.city + ", " +
				"CompanyName: " + this.companyName + ", " +
				"ContactName: " + this.contactName + ", " +
				"ContactTitle: " + this.contactTitle + ", " +
				"Country: " + this.country + ", " +
				"Fax: " + this.fax + ", " +
				"HomePage: " + this.homePage + ", " +
				"Phone: " + this.phone + ", " +
				"PostalCode: " + this.postalCode + ", " +
				"Region: " + this.region + " }";
	}
}
