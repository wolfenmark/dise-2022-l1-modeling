package pojo;

import org.bson.Document;

public class Customer {
	public String address;
	public String city;
	public String companyName;
	public String contactName;
	public String contactTitle;
	public String country;
	public String customerID;
	public String fax;
	public String phone; 
	public String postalCode;
	public String region;
	
	public Customer(Document doc) {
		this.address = doc.getString("Address");
		this.city = doc.getString("City");
		this.companyName = doc.getString("CompanyName");
		this.contactName = doc.getString("ContactName");
		this.contactTitle = doc.getString("ContactTitle");
		this.country = doc.getString("Country");
		this.customerID = doc.getString("CustomerID");
		this.fax = doc.getString("Fax");
		this.phone = doc.getString("Phone");
		this.postalCode = doc.getString("PostalCode");
		this.region = doc.getString("Region");
	}
	
	public String toString() {
		return "Customer: { CustomerID: " + this.customerID + ", " +
				"CompanyName: " + this.companyName + ", " +
				"ContactName: " + this.contactName + ", " +
				"ContactTitle: " + this.contactTitle + ", " +
				"Address: " + this.address + ", " +
				"City: " + this.city + ", " +
				"Region: " + this.region + ", " +
				"PostalCode: " + this.postalCode + ", " +
				"Country: " + this.country + ", " +
				"Phone: " + this.phone + ", " +
				"Fax: " + this.fax + " }";
	}
}
