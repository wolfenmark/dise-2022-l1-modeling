package pojo;

public class ProductWithDetails extends Product {
	public Float OrderUnitPrice;
	public Integer Quantity;
	public Double Discount;
	
	public String toString() {
		return super.toString() + ", OrderUnitPrice: " + OrderUnitPrice + ", " +
				"Quantity: " + Quantity + ", " +
				"Discount: " + Discount;
	}
}
