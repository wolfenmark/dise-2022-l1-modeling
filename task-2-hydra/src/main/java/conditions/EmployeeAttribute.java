package conditions;

public enum EmployeeAttribute implements Attributes{
	employeeID, lastName, firstName, title, titleOfCourtesy, birthDate, hireDate, address, city, region, postalCode, country, homePhone, extension, photo, notes, photoPath, salary
}
