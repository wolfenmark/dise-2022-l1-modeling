package conditions;

public enum ProductAttribute implements Attributes{
	productID, productName, supplierRef, quantityPerUnit, unitPrice, unitsInStock, unitsOnOrder, reorderLevel, discontinued
}
