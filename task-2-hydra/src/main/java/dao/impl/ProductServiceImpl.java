package dao.impl;
import exceptions.PhysicalStructureException;
import java.util.Arrays;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import pojo.Product;
import conditions.*;
import dao.services.ProductService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import util.Dataset;
import org.apache.spark.sql.Encoders;
import util.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.*;
import org.apache.spark.api.java.function.MapFunction;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.api.java.JavaSparkContext;
import com.mongodb.spark.MongoSpark;
import org.bson.Document;
import static java.util.Collections.singletonList;
import dbconnection.SparkConnectionMgr;
import dbconnection.DBConnectionMgr;
import util.WrappedArray;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.FilterFunction;
import java.util.ArrayList;
import org.apache.commons.lang3.mutable.MutableBoolean;
import tdo.*;
import pojo.*;
import util.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.ArrayType;
import scala.Tuple2;
import org.bson.Document;
import org.bson.conversions.Bson;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;


public class ProductServiceImpl extends ProductService {
	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ProductServiceImpl.class);
	
	
	
	
	public static Pair<String, List<String>> getSQLWhereClauseInProductsFromReldata(Condition<ProductAttribute> condition, MutableBoolean refilterFlag) {
		return getSQLWhereClauseInProductsFromReldataWithTableAlias(condition, refilterFlag, "");
	}
	
	public static List<String> getSQLSetClauseInProductsFromReldata(conditions.SetClause<ProductAttribute> set) {
		List<String> res = new ArrayList<String>();
		if(set != null) {
			java.util.Map<String, java.util.Map<String, String>> longFieldValues = new java.util.HashMap<String, java.util.Map<String, String>>();
			java.util.Map<ProductAttribute, Object> clause = set.getClause();
			for(java.util.Map.Entry<ProductAttribute, Object> e : clause.entrySet()) {
				ProductAttribute attr = e.getKey();
				Object value = e.getValue();
				if(attr == ProductAttribute.productID ) {
					res.add("ProductID = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ProductAttribute.productName ) {
					res.add("ProductName = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ProductAttribute.supplierRef ) {
					res.add("SupplierRef = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ProductAttribute.quantityPerUnit ) {
					res.add("QuantityPerUnit = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ProductAttribute.unitPrice ) {
					res.add("UnitPrice = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ProductAttribute.unitsInStock ) {
					res.add("UnitsInStock = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ProductAttribute.unitsOnOrder ) {
					res.add("UnitsOnOrder = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ProductAttribute.reorderLevel ) {
					res.add("ReorderLevel = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ProductAttribute.discontinued ) {
					res.add("Discontinued = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
			}
	
			for(java.util.Map.Entry<String, java.util.Map<String, String>> entry : longFieldValues.entrySet()) {
				String longField = entry.getKey();
				java.util.Map<String, String> values = entry.getValue();
			}
	
		}
		return res;
	}
	
	public static Pair<String, List<String>> getSQLWhereClauseInProductsFromReldataWithTableAlias(Condition<ProductAttribute> condition, MutableBoolean refilterFlag, String tableAlias) {
		String where = null;	
		List<String> preparedValues = new java.util.ArrayList<String>();
		if(condition != null) {
			
			if(condition instanceof SimpleCondition) {
				ProductAttribute attr = ((SimpleCondition<ProductAttribute>) condition).getAttribute();
				Operator op = ((SimpleCondition<ProductAttribute>) condition).getOperator();
				Object value = ((SimpleCondition<ProductAttribute>) condition).getValue();
				if(value != null) {
					boolean isConditionAttrEncountered = false;
					if(attr == ProductAttribute.productID ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "ProductID " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ProductAttribute.productName ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "ProductName " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ProductAttribute.supplierRef ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "SupplierRef " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ProductAttribute.quantityPerUnit ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "QuantityPerUnit " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ProductAttribute.unitPrice ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "UnitPrice " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ProductAttribute.unitsInStock ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "UnitsInStock " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ProductAttribute.unitsOnOrder ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "UnitsOnOrder " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ProductAttribute.reorderLevel ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "ReorderLevel " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ProductAttribute.discontinued ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Discontinued " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(!isConditionAttrEncountered) {
						refilterFlag.setValue(true);
						where = "1 = 1";
					}
				} else {
					if(attr == ProductAttribute.productID ) {
						if(op == Operator.EQUALS)
							where =  "ProductID IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "ProductID IS NOT NULL";
					}
					if(attr == ProductAttribute.productName ) {
						if(op == Operator.EQUALS)
							where =  "ProductName IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "ProductName IS NOT NULL";
					}
					if(attr == ProductAttribute.supplierRef ) {
						if(op == Operator.EQUALS)
							where =  "SupplierRef IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "SupplierRef IS NOT NULL";
					}
					if(attr == ProductAttribute.quantityPerUnit ) {
						if(op == Operator.EQUALS)
							where =  "QuantityPerUnit IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "QuantityPerUnit IS NOT NULL";
					}
					if(attr == ProductAttribute.unitPrice ) {
						if(op == Operator.EQUALS)
							where =  "UnitPrice IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "UnitPrice IS NOT NULL";
					}
					if(attr == ProductAttribute.unitsInStock ) {
						if(op == Operator.EQUALS)
							where =  "UnitsInStock IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "UnitsInStock IS NOT NULL";
					}
					if(attr == ProductAttribute.unitsOnOrder ) {
						if(op == Operator.EQUALS)
							where =  "UnitsOnOrder IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "UnitsOnOrder IS NOT NULL";
					}
					if(attr == ProductAttribute.reorderLevel ) {
						if(op == Operator.EQUALS)
							where =  "ReorderLevel IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "ReorderLevel IS NOT NULL";
					}
					if(attr == ProductAttribute.discontinued ) {
						if(op == Operator.EQUALS)
							where =  "Discontinued IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Discontinued IS NOT NULL";
					}
				}
			}
	
			if(condition instanceof AndCondition) {
				Pair<String, List<String>> pairLeft = getSQLWhereClauseInProductsFromReldata(((AndCondition) condition).getLeftCondition(), refilterFlag);
				Pair<String, List<String>> pairRight = getSQLWhereClauseInProductsFromReldata(((AndCondition) condition).getRightCondition(), refilterFlag);
				String whereLeft = pairLeft.getKey();
				String whereRight = pairRight.getKey();
				List<String> leftValues = pairLeft.getValue();
				List<String> rightValues = pairRight.getValue();
				if(whereLeft != null || whereRight != null) {
					if(whereLeft == null)
						where = whereRight;
					else
						if(whereRight == null)
							where = whereLeft;
						else
							where = "(" + whereLeft + " AND " + whereRight + ")";
					preparedValues.addAll(leftValues);
					preparedValues.addAll(rightValues);
				}
			}
	
			if(condition instanceof OrCondition) {
				Pair<String, List<String>> pairLeft = getSQLWhereClauseInProductsFromReldata(((OrCondition) condition).getLeftCondition(), refilterFlag);
				Pair<String, List<String>> pairRight = getSQLWhereClauseInProductsFromReldata(((OrCondition) condition).getRightCondition(), refilterFlag);
				String whereLeft = pairLeft.getKey();
				String whereRight = pairRight.getKey();
				List<String> leftValues = pairLeft.getValue();
				List<String> rightValues = pairRight.getValue();
				if(whereLeft != null || whereRight != null) {
					if(whereLeft == null)
						where = whereRight;
					else
						if(whereRight == null)
							where = whereLeft;
						else
							where = "(" + whereLeft + " OR " + whereRight + ")";
					preparedValues.addAll(leftValues);
					preparedValues.addAll(rightValues);
				}
			}
	
		}
	
		return new ImmutablePair<String, List<String>>(where, preparedValues);
	}
	
	
	
	public Dataset<Product> getProductListInProductsFromReldata(conditions.Condition<conditions.ProductAttribute> condition, MutableBoolean refilterFlag){
	
		Pair<String, List<String>> whereClause = ProductServiceImpl.getSQLWhereClauseInProductsFromReldata(condition, refilterFlag);
		String where = whereClause.getKey();
		List<String> preparedValues = whereClause.getValue();
		for(String preparedValue : preparedValues) {
			where = where.replaceFirst("\\?", preparedValue);
		}
		
		Dataset<Row> d = dbconnection.SparkConnectionMgr.getDataset("reldata", "Products", where);
		
	
		Dataset<Product> res = d.map((MapFunction<Row, Product>) r -> {
					Product product_res = new Product();
					Integer groupIndex = null;
					String regex = null;
					String value = null;
					Pattern p = null;
					Matcher m = null;
					boolean matches = false;
					
					// attribute [Product.ProductID]
					Integer productID = Util.getIntegerValue(r.getAs("ProductID"));
					product_res.setProductID(productID);
					
					// attribute [Product.ProductName]
					String productName = Util.getStringValue(r.getAs("ProductName"));
					product_res.setProductName(productName);
					
					// attribute [Product.SupplierRef]
					Integer supplierRef = Util.getIntegerValue(r.getAs("SupplierRef"));
					product_res.setSupplierRef(supplierRef);
					
					// attribute [Product.QuantityPerUnit]
					String quantityPerUnit = Util.getStringValue(r.getAs("QuantityPerUnit"));
					product_res.setQuantityPerUnit(quantityPerUnit);
					
					// attribute [Product.UnitPrice]
					Double unitPrice = Util.getDoubleValue(r.getAs("UnitPrice"));
					product_res.setUnitPrice(unitPrice);
					
					// attribute [Product.UnitsInStock]
					Integer unitsInStock = Util.getIntegerValue(r.getAs("UnitsInStock"));
					product_res.setUnitsInStock(unitsInStock);
					
					// attribute [Product.UnitsOnOrder]
					Integer unitsOnOrder = Util.getIntegerValue(r.getAs("UnitsOnOrder"));
					product_res.setUnitsOnOrder(unitsOnOrder);
					
					// attribute [Product.ReorderLevel]
					Integer reorderLevel = Util.getIntegerValue(r.getAs("ReorderLevel"));
					product_res.setReorderLevel(reorderLevel);
					
					// attribute [Product.Discontinued]
					Boolean discontinued = Util.getBooleanValue(r.getAs("Discontinued"));
					product_res.setDiscontinued(discontinued);
	
	
	
					return product_res;
				}, Encoders.bean(Product.class));
	
	
		return res;
		
	}
	
	
	
	
	
	
	public Dataset<Product> getProductListInComposedOf(conditions.Condition<conditions.OrderAttribute> order_condition,conditions.Condition<conditions.ProductAttribute> product_condition, conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition)		{
		MutableBoolean product_refilter = new MutableBoolean(false);
		List<Dataset<Product>> datasetsPOJO = new ArrayList<Dataset<Product>>();
		Dataset<Order> all = null;
		boolean all_already_persisted = false;
		MutableBoolean order_refilter;
		org.apache.spark.sql.Column joinCondition = null;
		// join physical structure A<-AB->B
		
		//join between 2 SQL tables and a non-relational structure
		// (A - AB) (B)
		order_refilter = new MutableBoolean(false);
		MutableBoolean composedOf_refilter = new MutableBoolean(false);
		Dataset<ComposedOfTDO> res_composedOf_productRef_orderRef = composedOfService.getComposedOfTDOListInProductsAndOrder_DetailsFromreldata(product_condition, composedOf_condition, product_refilter, composedOf_refilter);
		Dataset<OrderTDO> res_orderRef_productRef = composedOfService.getOrderTDOListOrderInOrderRefInOrderShippingFromMyRedis(order_condition, order_refilter);
		if(order_refilter.booleanValue()) {
			if(all == null)
					all = new OrderServiceImpl().getOrderList(order_condition);
			joinCondition = null;
				joinCondition = res_orderRef_productRef.col("orderID").equalTo(all.col("orderID"));
				res_orderRef_productRef = res_orderRef_productRef.as("A").join(all, joinCondition).select("A.*").as(Encoders.bean(OrderTDO.class));
		}
		
		Dataset<Row> res_row_productRef_orderRef = res_composedOf_productRef_orderRef.join(res_orderRef_productRef.withColumnRenamed("logEvents", "composedOf_logEvents"),
																														res_composedOf_productRef_orderRef.col("reldata_Order_Details_orderRef_source_OrderRef").equalTo(res_orderRef_productRef.col("reldata_Order_Details_orderRef_target_orderid")));																												
																														
		Dataset<Product> res_Product_productRef = res_row_productRef_orderRef.select("product.*").as(Encoders.bean(Product.class));
		datasetsPOJO.add(res_Product_productRef.dropDuplicates(new String[] {"productID"}));	
		
		
		
		Dataset<ComposedOf> res_composedOf_product;
		Dataset<Product> res_Product;
		
		
		//Join datasets or return 
		Dataset<Product> res = fullOuterJoinsProduct(datasetsPOJO);
		if(res == null)
			return null;
	
		if(product_refilter.booleanValue())
			res = res.filter((FilterFunction<Product>) r -> product_condition == null || product_condition.evaluate(r));
		
	
		return res;
		}
	public Dataset<Product> getProductListInSupplies(conditions.Condition<conditions.ProductAttribute> product_condition,conditions.Condition<conditions.SupplierAttribute> supplier_condition)		{
		MutableBoolean product_refilter = new MutableBoolean(false);
		List<Dataset<Product>> datasetsPOJO = new ArrayList<Dataset<Product>>();
		Dataset<Supplier> all = null;
		boolean all_already_persisted = false;
		MutableBoolean supplier_refilter;
		org.apache.spark.sql.Column joinCondition = null;
		// For role 'product' in reference 'supplierRef'. A->B Scenario
		supplier_refilter = new MutableBoolean(false);
		Dataset<ProductTDO> productTDOsupplierRefproduct = suppliesService.getProductTDOListProductInSupplierRefInProductsFromReldata(product_condition, product_refilter);
		Dataset<SupplierTDO> supplierTDOsupplierRefsupplier = suppliesService.getSupplierTDOListSupplierInSupplierRefInProductsFromReldata(supplier_condition, supplier_refilter);
		if(supplier_refilter.booleanValue()) {
			if(all == null)
				all = new SupplierServiceImpl().getSupplierList(supplier_condition);
			joinCondition = null;
			joinCondition = supplierTDOsupplierRefsupplier.col("supplierID").equalTo(all.col("supplierID"));
			if(joinCondition == null)
				supplierTDOsupplierRefsupplier = supplierTDOsupplierRefsupplier.as("A").join(all).select("A.*").as(Encoders.bean(SupplierTDO.class));
			else
				supplierTDOsupplierRefsupplier = supplierTDOsupplierRefsupplier.as("A").join(all, joinCondition).select("A.*").as(Encoders.bean(SupplierTDO.class));
		}
	
		
		Dataset<Row> res_supplierRef = productTDOsupplierRefproduct.join(supplierTDOsupplierRefsupplier
				.withColumnRenamed("supplierID", "Supplier_supplierID")
				.withColumnRenamed("address", "Supplier_address")
				.withColumnRenamed("city", "Supplier_city")
				.withColumnRenamed("companyName", "Supplier_companyName")
				.withColumnRenamed("contactName", "Supplier_contactName")
				.withColumnRenamed("contactTitle", "Supplier_contactTitle")
				.withColumnRenamed("country", "Supplier_country")
				.withColumnRenamed("fax", "Supplier_fax")
				.withColumnRenamed("homePage", "Supplier_homePage")
				.withColumnRenamed("phone", "Supplier_phone")
				.withColumnRenamed("postalCode", "Supplier_postalCode")
				.withColumnRenamed("region", "Supplier_region")
				.withColumnRenamed("logEvents", "Supplier_logEvents"),
				productTDOsupplierRefproduct.col("reldata_Products_supplierRef_source_SupplierRef").equalTo(supplierTDOsupplierRefsupplier.col("reldata_Products_supplierRef_target_SupplierID")));
		Dataset<Product> res_Product_supplierRef = res_supplierRef.select( "productID", "productName", "supplierRef", "quantityPerUnit", "unitPrice", "unitsInStock", "unitsOnOrder", "reorderLevel", "discontinued", "logEvents").as(Encoders.bean(Product.class));
		
		res_Product_supplierRef = res_Product_supplierRef.dropDuplicates(new String[] {"productID"});
		datasetsPOJO.add(res_Product_supplierRef);
		
		
		Dataset<Supplies> res_supplies_product;
		Dataset<Product> res_Product;
		
		
		//Join datasets or return 
		Dataset<Product> res = fullOuterJoinsProduct(datasetsPOJO);
		if(res == null)
			return null;
	
		if(product_refilter.booleanValue())
			res = res.filter((FilterFunction<Product>) r -> product_condition == null || product_condition.evaluate(r));
		
	
		return res;
		}
	
	public boolean insertProduct(
		Product product,
		Supplier	supplierSupplies){
			boolean inserted = false;
			// Insert in standalone structures
			// Insert in structures containing double embedded role
			// Insert in descending structures
			// Insert in ascending structures 
			// Insert in ref structures 
			inserted = insertProductInProductsFromReldata(product,supplierSupplies)|| inserted ;
			// Insert in ref structures mapped to opposite role of mandatory role  
			return inserted;
		}
	
	public boolean insertProductInProductsFromReldata(Product product,
		Supplier	supplierSupplies)	{
			 // Implement Insert in structures with mandatory references
			List<String> columns = new ArrayList<>();
			List<Object> values = new ArrayList<>();
			List<List<Object>> rows = new ArrayList<>();
			Object productId;
		columns.add("ProductID");
		values.add(product.getProductID());
		columns.add("ProductName");
		values.add(product.getProductName());
		columns.add("SupplierRef");
		values.add(product.getSupplierRef());
		columns.add("QuantityPerUnit");
		values.add(product.getQuantityPerUnit());
		columns.add("UnitPrice");
		values.add(product.getUnitPrice());
		columns.add("UnitsInStock");
		values.add(product.getUnitsInStock());
		columns.add("UnitsOnOrder");
		values.add(product.getUnitsOnOrder());
		columns.add("ReorderLevel");
		values.add(product.getReorderLevel());
		columns.add("Discontinued");
		values.add(product.getDiscontinued());
			// Ref 'supplierRef' mapped to role 'product'
			columns.add("SupplierRef");
			values.add(supplierSupplies.getSupplierID());
			rows.add(values);
			DBConnectionMgr.insertInTable(columns, rows, "Products", "reldata");
			return true;
		
		}
	private boolean inUpdateMethod = false;
	private List<Row> allProductIdList = null;
	public void updateProductList(conditions.Condition<conditions.ProductAttribute> condition, conditions.SetClause<conditions.ProductAttribute> set){
		inUpdateMethod = true;
		try {
	
	
		} finally {
			inUpdateMethod = false;
		}
	}
	
	
	
	
	
	public void updateProduct(pojo.Product product) {
		//TODO using the id
		return;
	}
	public void updateProductListInComposedOf(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf,
		conditions.SetClause<conditions.ProductAttribute> set
	){
		//TODO
	}
	
	public void updateProductListInComposedOfByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.SetClause<conditions.ProductAttribute> set
	){
		updateProductListInComposedOf(order_condition, null, null, set);
	}
	
	public void updateProductListInComposedOfByOrder(
		pojo.Order order,
		conditions.SetClause<conditions.ProductAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	public void updateProductListInComposedOfByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.SetClause<conditions.ProductAttribute> set
	){
		updateProductListInComposedOf(null, product_condition, null, set);
	}
	public void updateProductListInComposedOfByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition,
		conditions.SetClause<conditions.ProductAttribute> set
	){
		updateProductListInComposedOf(null, null, composedOf_condition, set);
	}
	public void updateProductListInSupplies(
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.SupplierAttribute> supplier_condition,
		
		conditions.SetClause<conditions.ProductAttribute> set
	){
		//TODO
	}
	
	public void updateProductListInSuppliesByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.SetClause<conditions.ProductAttribute> set
	){
		updateProductListInSupplies(product_condition, null, set);
	}
	public void updateProductListInSuppliesBySupplierCondition(
		conditions.Condition<conditions.SupplierAttribute> supplier_condition,
		conditions.SetClause<conditions.ProductAttribute> set
	){
		updateProductListInSupplies(null, supplier_condition, set);
	}
	
	public void updateProductListInSuppliesBySupplier(
		pojo.Supplier supplier,
		conditions.SetClause<conditions.ProductAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	
	
	public void deleteProductList(conditions.Condition<conditions.ProductAttribute> condition){
		//TODO
	}
	
	public void deleteProduct(pojo.Product product) {
		//TODO using the id
		return;
	}
	public void deleteProductListInComposedOf(	
		conditions.Condition<conditions.OrderAttribute> order_condition,	
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf){
			//TODO
		}
	
	public void deleteProductListInComposedOfByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteProductListInComposedOf(order_condition, null, null);
	}
	
	public void deleteProductListInComposedOfByOrder(
		pojo.Order order 
	){
		//TODO get id in condition
		return;	
	}
	
	public void deleteProductListInComposedOfByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition
	){
		deleteProductListInComposedOf(null, product_condition, null);
	}
	public void deleteProductListInComposedOfByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition
	){
		deleteProductListInComposedOf(null, null, composedOf_condition);
	}
	public void deleteProductListInSupplies(	
		conditions.Condition<conditions.ProductAttribute> product_condition,	
		conditions.Condition<conditions.SupplierAttribute> supplier_condition){
			//TODO
		}
	
	public void deleteProductListInSuppliesByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition
	){
		deleteProductListInSupplies(product_condition, null);
	}
	public void deleteProductListInSuppliesBySupplierCondition(
		conditions.Condition<conditions.SupplierAttribute> supplier_condition
	){
		deleteProductListInSupplies(null, supplier_condition);
	}
	
	public void deleteProductListInSuppliesBySupplier(
		pojo.Supplier supplier 
	){
		//TODO get id in condition
		return;	
	}
	
	
}
