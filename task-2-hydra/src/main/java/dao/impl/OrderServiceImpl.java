package dao.impl;
import exceptions.PhysicalStructureException;
import java.util.Arrays;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import pojo.Order;
import conditions.*;
import dao.services.OrderService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import util.Dataset;
import org.apache.spark.sql.Encoders;
import util.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.*;
import org.apache.spark.api.java.function.MapFunction;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.api.java.JavaSparkContext;
import com.mongodb.spark.MongoSpark;
import org.bson.Document;
import static java.util.Collections.singletonList;
import dbconnection.SparkConnectionMgr;
import dbconnection.DBConnectionMgr;
import util.WrappedArray;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.FilterFunction;
import java.util.ArrayList;
import org.apache.commons.lang3.mutable.MutableBoolean;
import tdo.*;
import pojo.*;
import util.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.ArrayType;
import scala.Tuple2;
import org.bson.Document;
import org.bson.conversions.Bson;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;


public class OrderServiceImpl extends OrderService {
	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OrderServiceImpl.class);
	
	
	
	
	
	
	
	//TODO redis
	public Dataset<Order> getOrderListInOrderShippingFromMyRedis(conditions.Condition<conditions.OrderAttribute> condition, MutableBoolean refilterFlag){
		// Build the key pattern
		//  - If the condition attribute is in the key pattern, replace by the value. Only if operator is EQUALS.
		//  - Replace all other fields of key pattern by a '*' 
		String keypattern= "", keypatternAllVariables="";
		String valueCond=null;
		String finalKeypattern;
		List<String> fieldsListInKey = new ArrayList<>();
		Set<OrderAttribute> keyAttributes = new HashSet<>();
		keypattern=keypattern.concat("ORDER:");
		keypatternAllVariables=keypatternAllVariables.concat("ORDER:");
		if(!Util.containsOrCondition(condition)){
			valueCond=Util.getStringValue(Util.getValueOfAttributeInEqualCondition(condition,OrderAttribute.orderID));
			keyAttributes.add(OrderAttribute.orderID);
		}
		else{
			valueCond=null;
			refilterFlag.setValue(true);
		}
		if(valueCond==null)
			keypattern=keypattern.concat("*");
		else
			keypattern=keypattern.concat(valueCond);
		fieldsListInKey.add("orderid");
		keypatternAllVariables=keypatternAllVariables.concat("*");
		if(!refilterFlag.booleanValue()){
			Set<OrderAttribute> conditionAttributes = Util.getConditionAttributes(condition);
			for (OrderAttribute a : conditionAttributes) {
				if (!keyAttributes.contains(a)) {
					refilterFlag.setValue(true);
					break;
				}
			}
		}
	
			
		// Find the type of query to perform in order to retrieve a Dataset<Row>
		// Based on the type of the value. Is a it a simple string or a hash or a list... 
		Dataset<Row> rows;
		StructType structType = new StructType(new StructField[] {
			DataTypes.createStructField("_id", DataTypes.StringType, true), //technical field to store the key.
			DataTypes.createStructField("OrderDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("RequiredDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShippedDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("Freight", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipName", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipAddress", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipCity", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipRegion", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipPostalCode", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipCountry", DataTypes.StringType, true)
	,		DataTypes.createStructField("CustomerRef", DataTypes.StringType, true)
	,		DataTypes.createStructField("EmployeeRef", DataTypes.StringType, true)
		});
		rows = SparkConnectionMgr.getRowsFromKeyValueHashes("myRedis",keypattern, structType);
		if(rows == null || rows.isEmpty())
				return null;
		boolean isStriped = false;
		String prefix=isStriped?keypattern.substring(0, keypattern.length() - 1):"";
		finalKeypattern = keypatternAllVariables;
		Dataset<Order> res = rows.map((MapFunction<Row, Order>) r -> {
					Order order_res = new Order();
					Integer groupindex = null;
					String regex = null;
					String value = null;
					Pattern p, pattern = null;
					Matcher m, match = null;
					boolean matches = false;
					String key = isStriped ? prefix + r.getAs("_id") : r.getAs("_id");
					// Spark Redis automatically strips leading character if the pattern provided contains a single '*' at the end.				
					pattern = Pattern.compile("\\*");
			        match = pattern.matcher(finalKeypattern);
					regex = finalKeypattern.replaceAll("\\*","(.*)");
					p = Pattern.compile(regex);
					m = p.matcher(key);
					matches = m.find();
					// attribute [Order.OrderID]
					// Attribute mapped in a key.
					groupindex = fieldsListInKey.indexOf("orderid")+1;
					if(groupindex==null) {
						logger.warn("Attribute 'Order' mapped physical field 'orderid' found in key but can't get index in build keypattern '{}'.", finalKeypattern);
					}
					String orderID = null;
					if(matches) {
						orderID = m.group(groupindex.intValue());
					} else {
						logger.warn("Cannot retrieve value for OrderorderID attribute stored in db myRedis. Regex [{}] Value [{}]",regex,value);
						order_res.addLogEvent("Cannot retrieve value for Order.orderID attribute stored in db myRedis. Probably due to an ambiguous regex.");
					}
					order_res.setOrderID(orderID == null ? null : Integer.parseInt(orderID));
					// attribute [Order.OrderDate]
					LocalDate orderDate = r.getAs("OrderDate") == null ? null : LocalDate.parse(r.getAs("OrderDate"));
					order_res.setOrderDate(orderDate);
					// attribute [Order.RequiredDate]
					LocalDate requiredDate = r.getAs("RequiredDate") == null ? null : LocalDate.parse(r.getAs("RequiredDate"));
					order_res.setRequiredDate(requiredDate);
					// attribute [Order.ShippedDate]
					LocalDate shippedDate = r.getAs("ShippedDate") == null ? null : LocalDate.parse(r.getAs("ShippedDate"));
					order_res.setShippedDate(shippedDate);
					// attribute [Order.Freight]
					Double freight = r.getAs("Freight") == null ? null : Double.parseDouble(r.getAs("Freight"));
					order_res.setFreight(freight);
					// attribute [Order.ShipName]
					String shipName = r.getAs("ShipName") == null ? null : r.getAs("ShipName");
					order_res.setShipName(shipName);
					// attribute [Order.ShipAddress]
					String shipAddress = r.getAs("ShipAddress") == null ? null : r.getAs("ShipAddress");
					order_res.setShipAddress(shipAddress);
					// attribute [Order.ShipCity]
					String shipCity = r.getAs("ShipCity") == null ? null : r.getAs("ShipCity");
					order_res.setShipCity(shipCity);
					// attribute [Order.ShipRegion]
					String shipRegion = r.getAs("ShipRegion") == null ? null : r.getAs("ShipRegion");
					order_res.setShipRegion(shipRegion);
					// attribute [Order.ShipPostalCode]
					String shipPostalCode = r.getAs("ShipPostalCode") == null ? null : r.getAs("ShipPostalCode");
					order_res.setShipPostalCode(shipPostalCode);
					// attribute [Order.ShipCountry]
					String shipCountry = r.getAs("ShipCountry") == null ? null : r.getAs("ShipCountry");
					order_res.setShipCountry(shipCountry);
	
						return order_res;
				}, Encoders.bean(Order.class));
		res=res.dropDuplicates(new String[] {"orderID"});
		return res;
		
	}
	
	
	
	public static Pair<List<String>, List<String>> getBSONUpdateQueryInCustomersFromMyMongoDB(conditions.SetClause<OrderAttribute> set) {
		List<String> res = new ArrayList<String>();
		Set<String> arrayFields = new HashSet<String>();
		if(set != null) {
			java.util.Map<String, java.util.Map<String, String>> longFieldValues = new java.util.HashMap<String, java.util.Map<String, String>>();
			java.util.Map<OrderAttribute, Object> clause = set.getClause();
			for(java.util.Map.Entry<OrderAttribute, Object> e : clause.entrySet()) {
				OrderAttribute attr = e.getKey();
				Object value = e.getValue();
				if(attr == OrderAttribute.orderID ) {
					String fieldName = "OrderID";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.orderDate ) {
					String fieldName = "OrderDate";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.requiredDate ) {
					String fieldName = "RequiredDate";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.shippedDate ) {
					String fieldName = "ShippedDate";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.freight ) {
					String fieldName = "Freight";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.shipName ) {
					String fieldName = "ShipName";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.shipAddress ) {
					String fieldName = "ShipAddress";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.shipCity ) {
					String fieldName = "ShipCity";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.shipRegion ) {
					String fieldName = "ShipRegion";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.shipPostalCode ) {
					String fieldName = "ShipPostalCode";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == OrderAttribute.shipCountry ) {
					String fieldName = "ShipCountry";
					fieldName = "orders.$[orders0]." + fieldName;
					arrayFields.add("orders0");
					fieldName = "'" + fieldName + "'";
					res.add(fieldName + " : " + Util.getDelimitedMongoValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
			}
	
			for(java.util.Map.Entry<String, java.util.Map<String, String>> entry : longFieldValues.entrySet()) {
				String longField = entry.getKey();
				java.util.Map<String, String> values = entry.getValue();
			}
	
		}
		return new ImmutablePair<List<String>, List<String>>(res, new ArrayList<String>(arrayFields));
	}
	
	public static String getBSONMatchQueryInCustomersFromMyMongoDB(Condition<OrderAttribute> condition, MutableBoolean refilterFlag) {	
		String res = null;	
		if(condition != null) {
			if(condition instanceof SimpleCondition) {
				OrderAttribute attr = ((SimpleCondition<OrderAttribute>) condition).getAttribute();
				Operator op = ((SimpleCondition<OrderAttribute>) condition).getOperator();
				Object value = ((SimpleCondition<OrderAttribute>) condition).getValue();
				if(value != null) {
					String valueString = Util.transformBSONValue(value);
					boolean isConditionAttrEncountered = false;
	
					if(attr == OrderAttribute.orderID ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "OrderID': {" + mongoOp + ": " + preparedValue + "}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.orderDate ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "OrderDate': {" + mongoOp + ": " +  "ISODate("+preparedValue + ")}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.requiredDate ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "RequiredDate': {" + mongoOp + ": " +  "ISODate("+preparedValue + ")}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.shippedDate ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "ShippedDate': {" + mongoOp + ": " +  "ISODate("+preparedValue + ")}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.freight ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "Freight': {" + mongoOp + ": " + preparedValue + "}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.shipName ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "ShipName': {" + mongoOp + ": " + preparedValue + "}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.shipAddress ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "ShipAddress': {" + mongoOp + ": " + preparedValue + "}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.shipCity ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "ShipCity': {" + mongoOp + ": " + preparedValue + "}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.shipRegion ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "ShipRegion': {" + mongoOp + ": " + preparedValue + "}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.shipPostalCode ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "ShipPostalCode': {" + mongoOp + ": " + preparedValue + "}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(attr == OrderAttribute.shipCountry ) {
						isConditionAttrEncountered = true;
					
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						res = "ShipCountry': {" + mongoOp + ": " + preparedValue + "}";
	
						res = "orders." + res;
					res = "'" + res;
					}
					if(!isConditionAttrEncountered) {
						refilterFlag.setValue(true);
						res = "$expr: {$eq:[1,1]}";
					}
					
				}
			}
	
			if(condition instanceof AndCondition) {
				String bsonLeft = getBSONMatchQueryInCustomersFromMyMongoDB(((AndCondition)condition).getLeftCondition(), refilterFlag);
				String bsonRight = getBSONMatchQueryInCustomersFromMyMongoDB(((AndCondition)condition).getRightCondition(), refilterFlag);			
				if(bsonLeft == null && bsonRight == null)
					return null;
				if(bsonLeft == null)
					return bsonRight;
				if(bsonRight == null)
					return bsonLeft;
				res = " $and: [ {" + bsonLeft + "}, {" + bsonRight + "}] ";
			}
	
			if(condition instanceof OrCondition) {
				String bsonLeft = getBSONMatchQueryInCustomersFromMyMongoDB(((OrCondition)condition).getLeftCondition(), refilterFlag);
				String bsonRight = getBSONMatchQueryInCustomersFromMyMongoDB(((OrCondition)condition).getRightCondition(), refilterFlag);			
				if(bsonLeft == null && bsonRight == null)
					return null;
				if(bsonLeft == null)
					return bsonRight;
				if(bsonRight == null)
					return bsonLeft;
				res = " $or: [ {" + bsonLeft + "}, {" + bsonRight + "}] ";	
			}
	
			
	
			
		}
	
		return res;
	}
	
	public static Pair<String, List<String>> getBSONQueryAndArrayFilterForUpdateQueryInCustomersFromMyMongoDB(Condition<OrderAttribute> condition, final List<String> arrayVariableNames, Set<String> arrayVariablesUsed, MutableBoolean refilterFlag) {	
		String query = null;
		List<String> arrayFilters = new ArrayList<String>();
		if(condition != null) {
			if(condition instanceof SimpleCondition) {
				String bson = null;
				OrderAttribute attr = ((SimpleCondition<OrderAttribute>) condition).getAttribute();
				Operator op = ((SimpleCondition<OrderAttribute>) condition).getOperator();
				Object value = ((SimpleCondition<OrderAttribute>) condition).getValue();
				if(value != null) {
					String valueString = Util.transformBSONValue(value);
					boolean isConditionAttrEncountered = false;
	
					if(attr == OrderAttribute.orderID ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "OrderID': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.orderDate ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "OrderDate': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.requiredDate ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "RequiredDate': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.shippedDate ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "ShippedDate': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.freight ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "Freight': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.shipName ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "ShipName': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.shipAddress ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "ShipAddress': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.shipCity ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "ShipCity': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.shipRegion ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "ShipRegion': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.shipPostalCode ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "ShipPostalCode': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(attr == OrderAttribute.shipCountry ) {
						isConditionAttrEncountered = true;
						String mongoOp = op.getMongoDBOperator();
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "'.*" + Util.escapeReservedRegexMongo(valueString)  + ".*'";
						} else {
							preparedValue = Util.getDelimitedMongoValue(value.getClass(), preparedValue);
						}
						bson = "ShipCountry': {" + mongoOp + ": " + preparedValue + "}";
					
						boolean arrayVar = false;
						if(!arrayVar) {
							if(arrayVariableNames.contains("orders0")) {
								arrayVar = true;
								arrayVariablesUsed.add("orders0");
								bson = "orders0." + bson; 
							} else {
								bson = "orders." + bson;
							}
						}
	
						bson = "'" + bson;
						if(arrayVar)
							arrayFilters.add(bson);
						else
							query = bson;
					}
					if(!isConditionAttrEncountered) {
						refilterFlag.setValue(true);
					}
					
				}
	
			}
	
			if(condition instanceof AndCondition) {
				Pair<String, List<String>> bsonLeft = getBSONQueryAndArrayFilterForUpdateQueryInCustomersFromMyMongoDB(((AndCondition)condition).getLeftCondition(), arrayVariableNames, arrayVariablesUsed, refilterFlag);
				Pair<String, List<String>> bsonRight = getBSONQueryAndArrayFilterForUpdateQueryInCustomersFromMyMongoDB(((AndCondition)condition).getRightCondition(), arrayVariableNames, arrayVariablesUsed, refilterFlag);			
				
				String queryLeft = bsonLeft.getLeft();
				String queryRight = bsonRight.getLeft();
				List<String> arrayFilterLeft = bsonLeft.getRight();
				List<String> arrayFilterRight = bsonRight.getRight();
	
				if(queryLeft == null && queryRight != null)
					query = queryRight;
				if(queryLeft != null && queryRight == null)
					query = queryLeft;
				if(queryLeft != null && queryRight != null)
					query = " $and: [ {" + queryLeft + "}, {" + queryRight + "}] ";
	
				arrayFilters.addAll(arrayFilterLeft);
				arrayFilters.addAll(arrayFilterRight);
			}
	
			if(condition instanceof OrCondition) {
				Pair<String, List<String>> bsonLeft = getBSONQueryAndArrayFilterForUpdateQueryInCustomersFromMyMongoDB(((AndCondition)condition).getLeftCondition(), arrayVariableNames, arrayVariablesUsed, refilterFlag);
				Pair<String, List<String>> bsonRight = getBSONQueryAndArrayFilterForUpdateQueryInCustomersFromMyMongoDB(((AndCondition)condition).getRightCondition(), arrayVariableNames, arrayVariablesUsed, refilterFlag);			
				
				String queryLeft = bsonLeft.getLeft();
				String queryRight = bsonRight.getLeft();
				List<String> arrayFilterLeft = bsonLeft.getRight();
				List<String> arrayFilterRight = bsonRight.getRight();
	
				if(queryLeft == null && queryRight != null)
					query = queryRight;
				if(queryLeft != null && queryRight == null)
					query = queryLeft;
				if(queryLeft != null && queryRight != null)
					query = " $or: [ {" + queryLeft + "}, {" + queryRight + "}] ";
	
				arrayFilters.addAll(arrayFilterLeft);
				arrayFilters.addAll(arrayFilterRight); // can be a problem
			}
		}
	
		return new ImmutablePair<String, List<String>>(query, arrayFilters);
	}
	
	
	
	public Dataset<Order> getOrderListInCustomersFromMyMongoDB(conditions.Condition<conditions.OrderAttribute> condition, MutableBoolean refilterFlag){
		String bsonQuery = OrderServiceImpl.getBSONMatchQueryInCustomersFromMyMongoDB(condition, refilterFlag);
		if(bsonQuery != null) {
			bsonQuery = "{$match: {" + bsonQuery + "}}";	
		} 
		
		Dataset<Row> dataset = dbconnection.SparkConnectionMgr.getDatasetFromMongoDB("myMongoDB", "Customers", bsonQuery);
	
		Dataset<Order> res = dataset.flatMap((FlatMapFunction<Row, Order>) r -> {
				Set<Order> list_res = new HashSet<Order>();
				Integer groupIndex = null;
				String regex = null;
				String value = null;
				Pattern p = null;
				Matcher m = null;
				boolean matches = false;
				Row nestedRow = null;
	
				boolean addedInList = false;
				Row r1 = r;
				Order order1 = new Order();
					boolean toAdd1  = false;
					WrappedArray array1  = null;
					array1 = r1.getAs("orders");
					if(array1!= null) {
						for (int i2 = 0; i2 < array1.size(); i2++){
							Row r2 = (Row) array1.apply(i2);
							Order order2 = (Order) order1.clone();
							boolean toAdd2  = false;
							WrappedArray array2  = null;
							// 	attribute Order.orderID for field OrderID			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderID")) {
								if(nestedRow.getAs("OrderID")==null)
									order2.setOrderID(null);
								else{
									order2.setOrderID(Util.getIntegerValue(nestedRow.getAs("OrderID")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.orderDate for field OrderDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderDate")) {
								if(nestedRow.getAs("OrderDate")==null)
									order2.setOrderDate(null);
								else{
									order2.setOrderDate(Util.getLocalDateValue(nestedRow.getAs("OrderDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.requiredDate for field RequiredDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("RequiredDate")) {
								if(nestedRow.getAs("RequiredDate")==null)
									order2.setRequiredDate(null);
								else{
									order2.setRequiredDate(Util.getLocalDateValue(nestedRow.getAs("RequiredDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shippedDate for field ShippedDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShippedDate")) {
								if(nestedRow.getAs("ShippedDate")==null)
									order2.setShippedDate(null);
								else{
									order2.setShippedDate(Util.getLocalDateValue(nestedRow.getAs("ShippedDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.freight for field Freight			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Freight")) {
								if(nestedRow.getAs("Freight")==null)
									order2.setFreight(null);
								else{
									order2.setFreight(Util.getDoubleValue(nestedRow.getAs("Freight")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipName for field ShipName			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipName")) {
								if(nestedRow.getAs("ShipName")==null)
									order2.setShipName(null);
								else{
									order2.setShipName(Util.getStringValue(nestedRow.getAs("ShipName")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipAddress for field ShipAddress			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipAddress")) {
								if(nestedRow.getAs("ShipAddress")==null)
									order2.setShipAddress(null);
								else{
									order2.setShipAddress(Util.getStringValue(nestedRow.getAs("ShipAddress")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCity for field ShipCity			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCity")) {
								if(nestedRow.getAs("ShipCity")==null)
									order2.setShipCity(null);
								else{
									order2.setShipCity(Util.getStringValue(nestedRow.getAs("ShipCity")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipRegion for field ShipRegion			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipRegion")) {
								if(nestedRow.getAs("ShipRegion")==null)
									order2.setShipRegion(null);
								else{
									order2.setShipRegion(Util.getStringValue(nestedRow.getAs("ShipRegion")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipPostalCode for field ShipPostalCode			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipPostalCode")) {
								if(nestedRow.getAs("ShipPostalCode")==null)
									order2.setShipPostalCode(null);
								else{
									order2.setShipPostalCode(Util.getStringValue(nestedRow.getAs("ShipPostalCode")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCountry for field ShipCountry			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCountry")) {
								if(nestedRow.getAs("ShipCountry")==null)
									order2.setShipCountry(null);
								else{
									order2.setShipCountry(Util.getStringValue(nestedRow.getAs("ShipCountry")));
									toAdd2 = true;					
									}
							}
							if(toAdd2&& (condition ==null || refilterFlag.booleanValue() || condition.evaluate(order2))) {
								list_res.add(order2);
								addedInList = true;
							} 
							if(addedInList)
								toAdd1 = false;
						}
					}
					
					if(toAdd1) {
						list_res.add(order1);
						addedInList = true;
					} 
					
					
				
				return list_res.iterator();
	
		}, Encoders.bean(Order.class));
		res= res.dropDuplicates(new String[]{"orderID"});
		return res;
		
	}
	
	
	
	
	
	
	public Dataset<Order> getBoughtOrderListInBought(conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,conditions.Condition<conditions.CustomerAttribute> customer_condition)		{
		MutableBoolean boughtOrder_refilter = new MutableBoolean(false);
		List<Dataset<Order>> datasetsPOJO = new ArrayList<Dataset<Order>>();
		Dataset<Customer> all = null;
		boolean all_already_persisted = false;
		MutableBoolean customer_refilter;
		org.apache.spark.sql.Column joinCondition = null;
		// For role 'boughtOrder' in reference 'customerRef'. A->B Scenario
		customer_refilter = new MutableBoolean(false);
		Dataset<OrderTDO> orderTDOcustomerRefboughtOrder = boughtService.getOrderTDOListBoughtOrderInCustomerRefInOrderShippingFromMyRedis(boughtOrder_condition, boughtOrder_refilter);
		Dataset<CustomerTDO> customerTDOcustomerRefcustomer = boughtService.getCustomerTDOListCustomerInCustomerRefInOrderShippingFromMyRedis(customer_condition, customer_refilter);
		if(customer_refilter.booleanValue()) {
			if(all == null)
				all = new CustomerServiceImpl().getCustomerList(customer_condition);
			joinCondition = null;
			joinCondition = customerTDOcustomerRefcustomer.col("customerID").equalTo(all.col("customerID"));
			if(joinCondition == null)
				customerTDOcustomerRefcustomer = customerTDOcustomerRefcustomer.as("A").join(all).select("A.*").as(Encoders.bean(CustomerTDO.class));
			else
				customerTDOcustomerRefcustomer = customerTDOcustomerRefcustomer.as("A").join(all, joinCondition).select("A.*").as(Encoders.bean(CustomerTDO.class));
		}
	
		
		Dataset<Row> res_customerRef = orderTDOcustomerRefboughtOrder.join(customerTDOcustomerRefcustomer
				.withColumnRenamed("address", "Customer_address")
				.withColumnRenamed("city", "Customer_city")
				.withColumnRenamed("companyName", "Customer_companyName")
				.withColumnRenamed("contactName", "Customer_contactName")
				.withColumnRenamed("contactTitle", "Customer_contactTitle")
				.withColumnRenamed("country", "Customer_country")
				.withColumnRenamed("customerID", "Customer_customerID")
				.withColumnRenamed("fax", "Customer_fax")
				.withColumnRenamed("phone", "Customer_phone")
				.withColumnRenamed("postalCode", "Customer_postalCode")
				.withColumnRenamed("region", "Customer_region")
				.withColumnRenamed("logEvents", "Customer_logEvents"),
				orderTDOcustomerRefboughtOrder.col("myRedis_orderShipping_customerRef_source_CustomerRef").equalTo(customerTDOcustomerRefcustomer.col("myRedis_orderShipping_customerRef_target_CustomerID")));
		Dataset<Order> res_Order_customerRef = res_customerRef.select( "orderID", "orderDate", "requiredDate", "shippedDate", "freight", "shipName", "shipAddress", "shipCity", "shipRegion", "shipPostalCode", "shipCountry", "logEvents").as(Encoders.bean(Order.class));
		
		res_Order_customerRef = res_Order_customerRef.dropDuplicates(new String[] {"orderID"});
		datasetsPOJO.add(res_Order_customerRef);
		
		
		Dataset<Bought> res_bought_boughtOrder;
		Dataset<Order> res_Order;
		// Role 'customer' mapped to EmbeddedObject 'orders' 'Order' containing 'Customer' 
		customer_refilter = new MutableBoolean(false);
		res_bought_boughtOrder = boughtService.getBoughtListInmyMongoDBCustomersorders(customer_condition, boughtOrder_condition, customer_refilter, boughtOrder_refilter);
		if(customer_refilter.booleanValue()) {
			if(all == null)
				all = new CustomerServiceImpl().getCustomerList(customer_condition);
			joinCondition = null;
			joinCondition = res_bought_boughtOrder.col("customer.customerID").equalTo(all.col("customerID"));
			if(joinCondition == null)
				res_Order = res_bought_boughtOrder.join(all).select("boughtOrder.*").as(Encoders.bean(Order.class));
			else
				res_Order = res_bought_boughtOrder.join(all, joinCondition).select("boughtOrder.*").as(Encoders.bean(Order.class));
		
		} else
			res_Order = res_bought_boughtOrder.map((MapFunction<Bought,Order>) r -> r.getBoughtOrder(), Encoders.bean(Order.class));
		res_Order = res_Order.dropDuplicates(new String[] {"orderID"});
		datasetsPOJO.add(res_Order);
		
		
		//Join datasets or return 
		Dataset<Order> res = fullOuterJoinsOrder(datasetsPOJO);
		if(res == null)
			return null;
	
		if(boughtOrder_refilter.booleanValue())
			res = res.filter((FilterFunction<Order>) r -> boughtOrder_condition == null || boughtOrder_condition.evaluate(r));
		
	
		return res;
		}
	public Dataset<Order> getOrderListInSold(conditions.Condition<conditions.OrderAttribute> order_condition,conditions.Condition<conditions.EmployeeAttribute> employee_condition)		{
		MutableBoolean order_refilter = new MutableBoolean(false);
		List<Dataset<Order>> datasetsPOJO = new ArrayList<Dataset<Order>>();
		Dataset<Employee> all = null;
		boolean all_already_persisted = false;
		MutableBoolean employee_refilter;
		org.apache.spark.sql.Column joinCondition = null;
		// For role 'order' in reference 'employeeRef'. A->B Scenario
		employee_refilter = new MutableBoolean(false);
		Dataset<OrderTDO> orderTDOemployeeReforder = soldService.getOrderTDOListOrderInEmployeeRefInOrderShippingFromMyRedis(order_condition, order_refilter);
		Dataset<EmployeeTDO> employeeTDOemployeeRefemployee = soldService.getEmployeeTDOListEmployeeInEmployeeRefInOrderShippingFromMyRedis(employee_condition, employee_refilter);
		if(employee_refilter.booleanValue()) {
			if(all == null)
				all = new EmployeeServiceImpl().getEmployeeList(employee_condition);
			joinCondition = null;
			joinCondition = employeeTDOemployeeRefemployee.col("employeeID").equalTo(all.col("employeeID"));
			if(joinCondition == null)
				employeeTDOemployeeRefemployee = employeeTDOemployeeRefemployee.as("A").join(all).select("A.*").as(Encoders.bean(EmployeeTDO.class));
			else
				employeeTDOemployeeRefemployee = employeeTDOemployeeRefemployee.as("A").join(all, joinCondition).select("A.*").as(Encoders.bean(EmployeeTDO.class));
		}
	
		
		Dataset<Row> res_employeeRef = orderTDOemployeeReforder.join(employeeTDOemployeeRefemployee
				.withColumnRenamed("employeeID", "Employee_employeeID")
				.withColumnRenamed("lastName", "Employee_lastName")
				.withColumnRenamed("firstName", "Employee_firstName")
				.withColumnRenamed("title", "Employee_title")
				.withColumnRenamed("titleOfCourtesy", "Employee_titleOfCourtesy")
				.withColumnRenamed("birthDate", "Employee_birthDate")
				.withColumnRenamed("hireDate", "Employee_hireDate")
				.withColumnRenamed("address", "Employee_address")
				.withColumnRenamed("city", "Employee_city")
				.withColumnRenamed("region", "Employee_region")
				.withColumnRenamed("postalCode", "Employee_postalCode")
				.withColumnRenamed("country", "Employee_country")
				.withColumnRenamed("homePhone", "Employee_homePhone")
				.withColumnRenamed("extension", "Employee_extension")
				.withColumnRenamed("photo", "Employee_photo")
				.withColumnRenamed("notes", "Employee_notes")
				.withColumnRenamed("photoPath", "Employee_photoPath")
				.withColumnRenamed("salary", "Employee_salary")
				.withColumnRenamed("logEvents", "Employee_logEvents"),
				orderTDOemployeeReforder.col("myRedis_orderShipping_employeeRef_source_EmployeeRef").equalTo(employeeTDOemployeeRefemployee.col("myRedis_orderShipping_employeeRef_target_EmployeeID")));
		Dataset<Order> res_Order_employeeRef = res_employeeRef.select( "orderID", "orderDate", "requiredDate", "shippedDate", "freight", "shipName", "shipAddress", "shipCity", "shipRegion", "shipPostalCode", "shipCountry", "logEvents").as(Encoders.bean(Order.class));
		
		res_Order_employeeRef = res_Order_employeeRef.dropDuplicates(new String[] {"orderID"});
		datasetsPOJO.add(res_Order_employeeRef);
		
		
		Dataset<Sold> res_sold_order;
		Dataset<Order> res_Order;
		
		
		//Join datasets or return 
		Dataset<Order> res = fullOuterJoinsOrder(datasetsPOJO);
		if(res == null)
			return null;
	
		List<Dataset<Order>> lonelyOrderList = new ArrayList<Dataset<Order>>();
		lonelyOrderList.add(getOrderListInCustomersFromMyMongoDB(order_condition, new MutableBoolean(false)));
		Dataset<Order> lonelyOrder = fullOuterJoinsOrder(lonelyOrderList);
		if(lonelyOrder != null) {
			res = fullLeftOuterJoinsOrder(Arrays.asList(res, lonelyOrder));
		}
		if(order_refilter.booleanValue())
			res = res.filter((FilterFunction<Order>) r -> order_condition == null || order_condition.evaluate(r));
		
	
		return res;
		}
	public Dataset<Order> getOrderListInComposedOf(conditions.Condition<conditions.OrderAttribute> order_condition,conditions.Condition<conditions.ProductAttribute> product_condition, conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition)		{
		MutableBoolean order_refilter = new MutableBoolean(false);
		List<Dataset<Order>> datasetsPOJO = new ArrayList<Dataset<Order>>();
		Dataset<Product> all = null;
		boolean all_already_persisted = false;
		MutableBoolean product_refilter;
		org.apache.spark.sql.Column joinCondition = null;
		// join physical structure A<-AB->B
		
		//join between 2 SQL tables and a non-relational structure
		// (A) (AB - B)
		product_refilter = new MutableBoolean(false);
		MutableBoolean composedOf_refilter = new MutableBoolean(false);
		Dataset<ComposedOfTDO> res_composedOf_productRef_orderRef = composedOfService.getComposedOfTDOListInProductsAndOrder_DetailsFromreldata(product_condition, composedOf_condition, product_refilter, composedOf_refilter);
		Dataset<OrderTDO> res_orderRef_productRef = composedOfService.getOrderTDOListOrderInOrderRefInOrderShippingFromMyRedis(order_condition, order_refilter);
		if(product_refilter.booleanValue()) {
			if(all == null)
					all = new ProductServiceImpl().getProductList(product_condition);
			joinCondition = null;
				joinCondition = res_composedOf_productRef_orderRef.col("product.productID").equalTo(all.col("productID"));
				res_composedOf_productRef_orderRef = res_composedOf_productRef_orderRef.as("A").join(all, joinCondition).select("A.*").as(Encoders.bean(ComposedOfTDO.class));
		} 
		Dataset<Row> res_row_productRef_orderRef = res_composedOf_productRef_orderRef.join(res_orderRef_productRef.withColumnRenamed("logEvents", "composedOf_logEvents"),
			res_composedOf_productRef_orderRef.col("reldata_Order_Details_orderRef_source_OrderRef").equalTo(res_orderRef_productRef.col("reldata_Order_Details_orderRef_target_orderid")));
		Dataset<Order> res_Order_orderRef = res_row_productRef_orderRef.as(Encoders.bean(Order.class));
		datasetsPOJO.add(res_Order_orderRef.dropDuplicates(new String[] {"orderID"}));	
		
		
		
		Dataset<ComposedOf> res_composedOf_order;
		Dataset<Order> res_Order;
		
		
		//Join datasets or return 
		Dataset<Order> res = fullOuterJoinsOrder(datasetsPOJO);
		if(res == null)
			return null;
	
		List<Dataset<Order>> lonelyOrderList = new ArrayList<Dataset<Order>>();
		lonelyOrderList.add(getOrderListInCustomersFromMyMongoDB(order_condition, new MutableBoolean(false)));
		Dataset<Order> lonelyOrder = fullOuterJoinsOrder(lonelyOrderList);
		if(lonelyOrder != null) {
			res = fullLeftOuterJoinsOrder(Arrays.asList(res, lonelyOrder));
		}
		if(order_refilter.booleanValue())
			res = res.filter((FilterFunction<Order>) r -> order_condition == null || order_condition.evaluate(r));
		
	
		return res;
		}
	
	public boolean insertOrder(
		Order order,
		Customer	customerBought,
		Employee	employeeSold,
		 List<Product> productComposedOf,
		Double composedOf_unitPrice
	,
		Integer composedOf_quantity
	,
		Double composedOf_discount
	){
		boolean inserted = false;
		// Insert in standalone structures
		// Insert in structures containing double embedded role
		// Insert in descending structures
		// Insert in ascending structures 
		inserted = insertOrderInCustomersFromMyMongoDB(order,customerBought,employeeSold,productComposedOf, composedOf_unitPrice, composedOf_quantity, composedOf_discount)|| inserted ;
		// Insert in ref structures 
		inserted = insertOrderInOrderShippingFromMyRedis(order,customerBought,employeeSold,productComposedOf, composedOf_unitPrice, composedOf_quantity, composedOf_discount)|| inserted ;
		inserted = insertOrderInOrder_DetailsFromReldata(order,customerBought,employeeSold,productComposedOf, composedOf_unitPrice, composedOf_quantity, composedOf_discount)|| inserted ;
		// Insert in ref structures mapped to opposite role of mandatory role  
		return inserted;
	}
	
	public boolean insertOrderInCustomersFromMyMongoDB(Order order,
		Customer	customerBought,
		Employee	employeeSold,
		 List<Product> productComposedOf,
		Double composedOf_unitPrice
	,
		Integer composedOf_quantity
	,
		Double composedOf_discount
	)	{
			 // Implement Insert in ascending complex struct
		
			Bson filter= new Document();
			Bson updateOp;
			String addToSet;
			List<String> fieldName= new ArrayList();
			List<Bson> arrayFilterCond = new ArrayList();
			Document docorders_1 = new Document();
			docorders_1.append("OrderID",order.getOrderID());
			docorders_1.append("OrderDate",order.getOrderDate());
			docorders_1.append("RequiredDate",order.getRequiredDate());
			docorders_1.append("ShippedDate",order.getShippedDate());
			docorders_1.append("Freight",order.getFreight());
			docorders_1.append("ShipName",order.getShipName());
			docorders_1.append("ShipAddress",order.getShipAddress());
			docorders_1.append("ShipCity",order.getShipCity());
			docorders_1.append("ShipRegion",order.getShipRegion());
			docorders_1.append("ShipPostalCode",order.getShipPostalCode());
			docorders_1.append("ShipCountry",order.getShipCountry());
			
			// level 1 ascending
			Customer customer = customerBought;
				filter = eq("CustomerID",customer.getCustomerID());
				updateOp = addToSet("orders", docorders_1);
				DBConnectionMgr.upsertMany(filter, updateOp, "Customers", "myMongoDB");					
		
			return true;
		}
	public boolean insertOrderInOrderShippingFromMyRedis(Order order,
		Customer	customerBought,
		Employee	employeeSold,
		 List<Product> productComposedOf,
		Double composedOf_unitPrice
	,
		Integer composedOf_quantity
	,
		Double composedOf_discount
	)	{
			 // Implement Insert in structures with mandatory references
			// In insertRefStruct in KeyValuePair
			// In Key value database
			Customer customer = customerBought;
			// Build key
				String keyBoughtOrder="";
				String valueBoughtOrder="";
				keyBoughtOrder += "ORDER:";
				keyBoughtOrder += order.getOrderID();
				// Value part
				// Generate for hash value
				boolean toAddBoughtOrder = false;
				List<Tuple2<String,String>> hashBoughtOrder = new ArrayList<>();
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_OrderDate="OrderDate";
				String BoughtOrder_value_OrderDate="";
				if(order.getOrderDate()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_OrderDate += order.getOrderDate();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_OrderDate,BoughtOrder_value_OrderDate));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_RequiredDate="RequiredDate";
				String BoughtOrder_value_RequiredDate="";
				if(order.getRequiredDate()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_RequiredDate += order.getRequiredDate();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_RequiredDate,BoughtOrder_value_RequiredDate));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_ShippedDate="ShippedDate";
				String BoughtOrder_value_ShippedDate="";
				if(order.getShippedDate()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_ShippedDate += order.getShippedDate();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_ShippedDate,BoughtOrder_value_ShippedDate));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_Freight="Freight";
				String BoughtOrder_value_Freight="";
				if(order.getFreight()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_Freight += order.getFreight();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_Freight,BoughtOrder_value_Freight));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_ShipName="ShipName";
				String BoughtOrder_value_ShipName="";
				if(order.getShipName()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_ShipName += order.getShipName();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_ShipName,BoughtOrder_value_ShipName));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_ShipAddress="ShipAddress";
				String BoughtOrder_value_ShipAddress="";
				if(order.getShipAddress()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_ShipAddress += order.getShipAddress();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_ShipAddress,BoughtOrder_value_ShipAddress));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_ShipCity="ShipCity";
				String BoughtOrder_value_ShipCity="";
				if(order.getShipCity()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_ShipCity += order.getShipCity();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_ShipCity,BoughtOrder_value_ShipCity));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_ShipRegion="ShipRegion";
				String BoughtOrder_value_ShipRegion="";
				if(order.getShipRegion()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_ShipRegion += order.getShipRegion();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_ShipRegion,BoughtOrder_value_ShipRegion));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_ShipPostalCode="ShipPostalCode";
				String BoughtOrder_value_ShipPostalCode="";
				if(order.getShipPostalCode()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_ShipPostalCode += order.getShipPostalCode();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_ShipPostalCode,BoughtOrder_value_ShipPostalCode));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_ShipCountry="ShipCountry";
				String BoughtOrder_value_ShipCountry="";
				if(order.getShipCountry()!=null){
					toAddBoughtOrder = true;
					BoughtOrder_value_ShipCountry += order.getShipCountry();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_ShipCountry,BoughtOrder_value_ShipCountry));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_CustomerRef="CustomerRef";
				String BoughtOrder_value_CustomerRef="";
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_CustomerRef,BoughtOrder_value_CustomerRef));
				toAddBoughtOrder = false;
				String BoughtOrder_fieldname_EmployeeRef="EmployeeRef";
				String BoughtOrder_value_EmployeeRef="";
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddBoughtOrder)
					hashBoughtOrder.add(new Tuple2<String,String>(BoughtOrder_fieldname_EmployeeRef,BoughtOrder_value_EmployeeRef));
				
				// Ref 'customerRef' mapped to role 'boughtOrder'
				hashBoughtOrder.add(new Tuple2<String,String>("CustomerRef",Util.getStringValue(customerBought.getCustomerID())));
				// Ref 'employeeRef' mapped to role 'order'
				hashBoughtOrder.add(new Tuple2<String,String>("EmployeeRef",Util.getStringValue(employeeSold.getEmployeeID())));
				
				
				SparkConnectionMgr.writeKeyValueHash(keyBoughtOrder,hashBoughtOrder, "myRedis");
			Employee employee = employeeSold;
			// Build key
				String keyOrder="";
				String valueOrder="";
				keyOrder += "ORDER:";
				keyOrder += order.getOrderID();
				// Value part
				// Generate for hash value
				boolean toAddOrder = false;
				List<Tuple2<String,String>> hashOrder = new ArrayList<>();
				toAddOrder = false;
				String Order_fieldname_OrderDate="OrderDate";
				String Order_value_OrderDate="";
				if(order.getOrderDate()!=null){
					toAddOrder = true;
					Order_value_OrderDate += order.getOrderDate();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_OrderDate,Order_value_OrderDate));
				toAddOrder = false;
				String Order_fieldname_RequiredDate="RequiredDate";
				String Order_value_RequiredDate="";
				if(order.getRequiredDate()!=null){
					toAddOrder = true;
					Order_value_RequiredDate += order.getRequiredDate();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_RequiredDate,Order_value_RequiredDate));
				toAddOrder = false;
				String Order_fieldname_ShippedDate="ShippedDate";
				String Order_value_ShippedDate="";
				if(order.getShippedDate()!=null){
					toAddOrder = true;
					Order_value_ShippedDate += order.getShippedDate();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_ShippedDate,Order_value_ShippedDate));
				toAddOrder = false;
				String Order_fieldname_Freight="Freight";
				String Order_value_Freight="";
				if(order.getFreight()!=null){
					toAddOrder = true;
					Order_value_Freight += order.getFreight();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_Freight,Order_value_Freight));
				toAddOrder = false;
				String Order_fieldname_ShipName="ShipName";
				String Order_value_ShipName="";
				if(order.getShipName()!=null){
					toAddOrder = true;
					Order_value_ShipName += order.getShipName();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_ShipName,Order_value_ShipName));
				toAddOrder = false;
				String Order_fieldname_ShipAddress="ShipAddress";
				String Order_value_ShipAddress="";
				if(order.getShipAddress()!=null){
					toAddOrder = true;
					Order_value_ShipAddress += order.getShipAddress();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_ShipAddress,Order_value_ShipAddress));
				toAddOrder = false;
				String Order_fieldname_ShipCity="ShipCity";
				String Order_value_ShipCity="";
				if(order.getShipCity()!=null){
					toAddOrder = true;
					Order_value_ShipCity += order.getShipCity();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_ShipCity,Order_value_ShipCity));
				toAddOrder = false;
				String Order_fieldname_ShipRegion="ShipRegion";
				String Order_value_ShipRegion="";
				if(order.getShipRegion()!=null){
					toAddOrder = true;
					Order_value_ShipRegion += order.getShipRegion();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_ShipRegion,Order_value_ShipRegion));
				toAddOrder = false;
				String Order_fieldname_ShipPostalCode="ShipPostalCode";
				String Order_value_ShipPostalCode="";
				if(order.getShipPostalCode()!=null){
					toAddOrder = true;
					Order_value_ShipPostalCode += order.getShipPostalCode();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_ShipPostalCode,Order_value_ShipPostalCode));
				toAddOrder = false;
				String Order_fieldname_ShipCountry="ShipCountry";
				String Order_value_ShipCountry="";
				if(order.getShipCountry()!=null){
					toAddOrder = true;
					Order_value_ShipCountry += order.getShipCountry();
				}
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_ShipCountry,Order_value_ShipCountry));
				toAddOrder = false;
				String Order_fieldname_CustomerRef="CustomerRef";
				String Order_value_CustomerRef="";
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_CustomerRef,Order_value_CustomerRef));
				toAddOrder = false;
				String Order_fieldname_EmployeeRef="EmployeeRef";
				String Order_value_EmployeeRef="";
				// When value is null for a field in the hash we dont add it to the hash.
				if(toAddOrder)
					hashOrder.add(new Tuple2<String,String>(Order_fieldname_EmployeeRef,Order_value_EmployeeRef));
				
				// Ref 'customerRef' mapped to role 'boughtOrder'
				hashOrder.add(new Tuple2<String,String>("CustomerRef",Util.getStringValue(customerBought.getCustomerID())));
				// Ref 'employeeRef' mapped to role 'order'
				hashOrder.add(new Tuple2<String,String>("EmployeeRef",Util.getStringValue(employeeSold.getEmployeeID())));
				
				
				SparkConnectionMgr.writeKeyValueHash(keyOrder,hashOrder, "myRedis");
			return false;
		
		}
	public boolean insertOrderInOrder_DetailsFromReldata(Order order,
		Customer	customerBought,
		Employee	employeeSold,
		 List<Product> productComposedOf,
		Double composedOf_unitPrice
	,
		Integer composedOf_quantity
	,
		Double composedOf_discount
	)	{
			 // Implement Insert in structures with mandatory references
			List<String> columns = new ArrayList<>();
			List<Object> values = new ArrayList<>();
			List<List<Object>> rows = new ArrayList<>();
			Object orderId;
			// Role in join structure 
			columns.add("OrderRef");
			orderId = order.getOrderID();
			columns.add("ProductRef");
			for(Product product : productComposedOf){
				values = new ArrayList<>();
				values.add(orderId);
				values.add(product.getProductID());
				rows.add(values);
			}	
			DBConnectionMgr.insertInTable(columns, rows, "Order_Details", "reldata");
			return true;
		
		}
	private boolean inUpdateMethod = false;
	private List<Row> allOrderIdList = null;
	public void updateOrderList(conditions.Condition<conditions.OrderAttribute> condition, conditions.SetClause<conditions.OrderAttribute> set){
		inUpdateMethod = true;
		try {
	
	
		} finally {
			inUpdateMethod = false;
		}
	}
	
	
	
	
	
	public void updateOrder(pojo.Order order) {
		//TODO using the id
		return;
	}
	public void updateBoughtOrderListInBought(
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,
		conditions.Condition<conditions.CustomerAttribute> customer_condition,
		
		conditions.SetClause<conditions.OrderAttribute> set
	){
		//TODO
	}
	
	public void updateBoughtOrderListInBoughtByBoughtOrderCondition(
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateBoughtOrderListInBought(boughtOrder_condition, null, set);
	}
	public void updateBoughtOrderListInBoughtByCustomerCondition(
		conditions.Condition<conditions.CustomerAttribute> customer_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateBoughtOrderListInBought(null, customer_condition, set);
	}
	
	public void updateBoughtOrderListInBoughtByCustomer(
		pojo.Customer customer,
		conditions.SetClause<conditions.OrderAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	public void updateOrderListInSold(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.EmployeeAttribute> employee_condition,
		
		conditions.SetClause<conditions.OrderAttribute> set
	){
		//TODO
	}
	
	public void updateOrderListInSoldByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInSold(order_condition, null, set);
	}
	public void updateOrderListInSoldByEmployeeCondition(
		conditions.Condition<conditions.EmployeeAttribute> employee_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInSold(null, employee_condition, set);
	}
	
	public void updateOrderListInSoldByEmployee(
		pojo.Employee employee,
		conditions.SetClause<conditions.OrderAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	public void updateOrderListInComposedOf(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		//TODO
	}
	
	public void updateOrderListInComposedOfByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInComposedOf(order_condition, null, null, set);
	}
	public void updateOrderListInComposedOfByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInComposedOf(null, product_condition, null, set);
	}
	
	public void updateOrderListInComposedOfByProduct(
		pojo.Product product,
		conditions.SetClause<conditions.OrderAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	public void updateOrderListInComposedOfByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInComposedOf(null, null, composedOf_condition, set);
	}
	
	
	public void deleteOrderList(conditions.Condition<conditions.OrderAttribute> condition){
		//TODO
	}
	
	public void deleteOrder(pojo.Order order) {
		//TODO using the id
		return;
	}
	public void deleteBoughtOrderListInBought(	
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,	
		conditions.Condition<conditions.CustomerAttribute> customer_condition){
			//TODO
		}
	
	public void deleteBoughtOrderListInBoughtByBoughtOrderCondition(
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition
	){
		deleteBoughtOrderListInBought(boughtOrder_condition, null);
	}
	public void deleteBoughtOrderListInBoughtByCustomerCondition(
		conditions.Condition<conditions.CustomerAttribute> customer_condition
	){
		deleteBoughtOrderListInBought(null, customer_condition);
	}
	
	public void deleteBoughtOrderListInBoughtByCustomer(
		pojo.Customer customer 
	){
		//TODO get id in condition
		return;	
	}
	
	public void deleteOrderListInSold(	
		conditions.Condition<conditions.OrderAttribute> order_condition,	
		conditions.Condition<conditions.EmployeeAttribute> employee_condition){
			//TODO
		}
	
	public void deleteOrderListInSoldByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteOrderListInSold(order_condition, null);
	}
	public void deleteOrderListInSoldByEmployeeCondition(
		conditions.Condition<conditions.EmployeeAttribute> employee_condition
	){
		deleteOrderListInSold(null, employee_condition);
	}
	
	public void deleteOrderListInSoldByEmployee(
		pojo.Employee employee 
	){
		//TODO get id in condition
		return;	
	}
	
	public void deleteOrderListInComposedOf(	
		conditions.Condition<conditions.OrderAttribute> order_condition,	
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf){
			//TODO
		}
	
	public void deleteOrderListInComposedOfByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteOrderListInComposedOf(order_condition, null, null);
	}
	public void deleteOrderListInComposedOfByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition
	){
		deleteOrderListInComposedOf(null, product_condition, null);
	}
	
	public void deleteOrderListInComposedOfByProduct(
		pojo.Product product 
	){
		//TODO get id in condition
		return;	
	}
	
	public void deleteOrderListInComposedOfByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition
	){
		deleteOrderListInComposedOf(null, null, composedOf_condition);
	}
	
}
