package dao.impl;

import exceptions.PhysicalStructureException;
import java.util.Arrays;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.apache.commons.lang3.StringUtils;
import util.Dataset;
import conditions.Condition;
import java.util.HashSet;
import java.util.Set;
import conditions.AndCondition;
import conditions.OrCondition;
import conditions.SimpleCondition;
import conditions.SoldAttribute;
import conditions.Operator;
import tdo.*;
import pojo.*;
import tdo.OrderTDO;
import tdo.SoldTDO;
import conditions.OrderAttribute;
import dao.services.OrderService;
import tdo.EmployeeTDO;
import tdo.SoldTDO;
import conditions.EmployeeAttribute;
import dao.services.EmployeeService;
import java.util.List;
import java.util.ArrayList;
import util.ScalaUtil;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.commons.lang3.mutable.MutableBoolean;
import util.Util;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import util.Row;
import org.apache.spark.sql.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import util.WrappedArray;
import org.apache.spark.api.java.function.FlatMapFunction;
import dbconnection.SparkConnectionMgr;
import dbconnection.DBConnectionMgr;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.ArrayType;
import static com.mongodb.client.model.Updates.addToSet;
import org.bson.Document;
import org.bson.conversions.Bson;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

public class SoldServiceImpl extends dao.services.SoldService {
	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SoldServiceImpl.class);
	
	
	// Left side 'EmployeeRef' of reference [employeeRef ]
	public Dataset<OrderTDO> getOrderTDOListOrderInEmployeeRefInOrderShippingFromMyRedis(Condition<OrderAttribute> condition, MutableBoolean refilterFlag){	
		// Build the key pattern
		//  - If the condition attribute is in the key pattern, replace by the value. Only if operator is EQUALS.
		//  - Replace all other fields of key pattern by a '*' 
		String keypattern= "", keypatternAllVariables="";
		String valueCond=null;
		String finalKeypattern;
		List<String> fieldsListInKey = new ArrayList<>();
		Set<OrderAttribute> keyAttributes = new HashSet<>();
		keypattern=keypattern.concat("ORDER:");
		keypatternAllVariables=keypatternAllVariables.concat("ORDER:");
		if(!Util.containsOrCondition(condition)){
			valueCond=Util.getStringValue(Util.getValueOfAttributeInEqualCondition(condition,OrderAttribute.orderID));
			keyAttributes.add(OrderAttribute.orderID);
		}
		else{
			valueCond=null;
			refilterFlag.setValue(true);
		}
		if(valueCond==null)
			keypattern=keypattern.concat("*");
		else
			keypattern=keypattern.concat(valueCond);
		fieldsListInKey.add("orderid");
		keypatternAllVariables=keypatternAllVariables.concat("*");
		if(!refilterFlag.booleanValue()){
			Set<OrderAttribute> conditionAttributes = Util.getConditionAttributes(condition);
			for (OrderAttribute a : conditionAttributes) {
				if (!keyAttributes.contains(a)) {
					refilterFlag.setValue(true);
					break;
				}
			}
		}
	
			
		// Find the type of query to perform in order to retrieve a Dataset<Row>
		// Based on the type of the value. Is a it a simple string or a hash or a list... 
		Dataset<Row> rows;
		StructType structType = new StructType(new StructField[] {
			DataTypes.createStructField("_id", DataTypes.StringType, true), //technical field to store the key.
			DataTypes.createStructField("OrderDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("RequiredDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShippedDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("Freight", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipName", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipAddress", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipCity", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipRegion", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipPostalCode", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipCountry", DataTypes.StringType, true)
	,		DataTypes.createStructField("CustomerRef", DataTypes.StringType, true)
	,		DataTypes.createStructField("EmployeeRef", DataTypes.StringType, true)
		});
		rows = SparkConnectionMgr.getRowsFromKeyValueHashes("myRedis",keypattern, structType);
		if(rows == null || rows.isEmpty())
				return null;
		boolean isStriped = false;
		String prefix=isStriped?keypattern.substring(0, keypattern.length() - 1):"";
		finalKeypattern = keypatternAllVariables;
		Dataset<OrderTDO> res = rows.map((MapFunction<Row, OrderTDO>) r -> {
					OrderTDO order_res = new OrderTDO();
					Integer groupindex = null;
					String regex = null;
					String value = null;
					Pattern p, pattern = null;
					Matcher m, match = null;
					boolean matches = false;
					String key = isStriped ? prefix + r.getAs("_id") : r.getAs("_id");
					// Spark Redis automatically strips leading character if the pattern provided contains a single '*' at the end.				
					pattern = Pattern.compile("\\*");
			        match = pattern.matcher(finalKeypattern);
					regex = finalKeypattern.replaceAll("\\*","(.*)");
					p = Pattern.compile(regex);
					m = p.matcher(key);
					matches = m.find();
					// attribute [Order.OrderID]
					// Attribute mapped in a key.
					groupindex = fieldsListInKey.indexOf("orderid")+1;
					if(groupindex==null) {
						logger.warn("Attribute 'Order' mapped physical field 'orderid' found in key but can't get index in build keypattern '{}'.", finalKeypattern);
					}
					String orderID = null;
					if(matches) {
						orderID = m.group(groupindex.intValue());
					} else {
						logger.warn("Cannot retrieve value for OrderorderID attribute stored in db myRedis. Regex [{}] Value [{}]",regex,value);
						order_res.addLogEvent("Cannot retrieve value for Order.orderID attribute stored in db myRedis. Probably due to an ambiguous regex.");
					}
					order_res.setOrderID(orderID == null ? null : Integer.parseInt(orderID));
					// attribute [Order.OrderDate]
					LocalDate orderDate = r.getAs("OrderDate") == null ? null : LocalDate.parse(r.getAs("OrderDate"));
					order_res.setOrderDate(orderDate);
					// attribute [Order.RequiredDate]
					LocalDate requiredDate = r.getAs("RequiredDate") == null ? null : LocalDate.parse(r.getAs("RequiredDate"));
					order_res.setRequiredDate(requiredDate);
					// attribute [Order.ShippedDate]
					LocalDate shippedDate = r.getAs("ShippedDate") == null ? null : LocalDate.parse(r.getAs("ShippedDate"));
					order_res.setShippedDate(shippedDate);
					// attribute [Order.Freight]
					Double freight = r.getAs("Freight") == null ? null : Double.parseDouble(r.getAs("Freight"));
					order_res.setFreight(freight);
					// attribute [Order.ShipName]
					String shipName = r.getAs("ShipName") == null ? null : r.getAs("ShipName");
					order_res.setShipName(shipName);
					// attribute [Order.ShipAddress]
					String shipAddress = r.getAs("ShipAddress") == null ? null : r.getAs("ShipAddress");
					order_res.setShipAddress(shipAddress);
					// attribute [Order.ShipCity]
					String shipCity = r.getAs("ShipCity") == null ? null : r.getAs("ShipCity");
					order_res.setShipCity(shipCity);
					// attribute [Order.ShipRegion]
					String shipRegion = r.getAs("ShipRegion") == null ? null : r.getAs("ShipRegion");
					order_res.setShipRegion(shipRegion);
					// attribute [Order.ShipPostalCode]
					String shipPostalCode = r.getAs("ShipPostalCode") == null ? null : r.getAs("ShipPostalCode");
					order_res.setShipPostalCode(shipPostalCode);
					// attribute [Order.ShipCountry]
					String shipCountry = r.getAs("ShipCountry") == null ? null : r.getAs("ShipCountry");
					order_res.setShipCountry(shipCountry);
					// Get reference column in value [EmployeeRef ] for reference [employeeRef]
					order_res.setMyRedis_orderShipping_employeeRef_source_EmployeeRef(r.getAs("EmployeeRef"));
	
						return order_res;
				}, Encoders.bean(OrderTDO.class));
		res=res.dropDuplicates(new String[] {"orderID"});
		return res;
	}
	
	// Right side 'EmployeeID' of reference [employeeRef ]
	public Dataset<EmployeeTDO> getEmployeeTDOListEmployeeInEmployeeRefInOrderShippingFromMyRedis(Condition<EmployeeAttribute> condition, MutableBoolean refilterFlag){
	
		Pair<String, List<String>> whereClause = EmployeeServiceImpl.getSQLWhereClauseInEmployeesFromReldata(condition, refilterFlag);
		String where = whereClause.getKey();
		List<String> preparedValues = whereClause.getValue();
		for(String preparedValue : preparedValues) {
			where = where.replaceFirst("\\?", preparedValue);
		}
		
		Dataset<Row> d = dbconnection.SparkConnectionMgr.getDataset("reldata", "Employees", where);
		
	
		Dataset<EmployeeTDO> res = d.map((MapFunction<Row, EmployeeTDO>) r -> {
					EmployeeTDO employee_res = new EmployeeTDO();
					Integer groupIndex = null;
					String regex = null;
					String value = null;
					Pattern p = null;
					Matcher m = null;
					boolean matches = false;
					
					// attribute [Employee.EmployeeID]
					Integer employeeID = Util.getIntegerValue(r.getAs("EmployeeID"));
					employee_res.setEmployeeID(employeeID);
					
					// attribute [Employee.LastName]
					String lastName = Util.getStringValue(r.getAs("LastName"));
					employee_res.setLastName(lastName);
					
					// attribute [Employee.FirstName]
					String firstName = Util.getStringValue(r.getAs("FirstName"));
					employee_res.setFirstName(firstName);
					
					// attribute [Employee.Title]
					String title = Util.getStringValue(r.getAs("Title"));
					employee_res.setTitle(title);
					
					// attribute [Employee.TitleOfCourtesy]
					String titleOfCourtesy = Util.getStringValue(r.getAs("TitleOfCourtesy"));
					employee_res.setTitleOfCourtesy(titleOfCourtesy);
					
					// attribute [Employee.BirthDate]
					LocalDate birthDate = Util.getLocalDateValue(r.getAs("BirthDate"));
					employee_res.setBirthDate(birthDate);
					
					// attribute [Employee.HireDate]
					LocalDate hireDate = Util.getLocalDateValue(r.getAs("HireDate"));
					employee_res.setHireDate(hireDate);
					
					// attribute [Employee.Address]
					String address = Util.getStringValue(r.getAs("Address"));
					employee_res.setAddress(address);
					
					// attribute [Employee.City]
					String city = Util.getStringValue(r.getAs("City"));
					employee_res.setCity(city);
					
					// attribute [Employee.Region]
					String region = Util.getStringValue(r.getAs("Region"));
					employee_res.setRegion(region);
					
					// attribute [Employee.PostalCode]
					String postalCode = Util.getStringValue(r.getAs("PostalCode"));
					employee_res.setPostalCode(postalCode);
					
					// attribute [Employee.Country]
					String country = Util.getStringValue(r.getAs("Country"));
					employee_res.setCountry(country);
					
					// attribute [Employee.HomePhone]
					String homePhone = Util.getStringValue(r.getAs("HomePhone"));
					employee_res.setHomePhone(homePhone);
					
					// attribute [Employee.Extension]
					String extension = Util.getStringValue(r.getAs("Extension"));
					employee_res.setExtension(extension);
					
					// attribute [Employee.Photo]
					byte[] photo = Util.getByteArrayValue(r.getAs("Photo"));
					employee_res.setPhoto(photo);
					
					// attribute [Employee.Notes]
					String notes = Util.getStringValue(r.getAs("Notes"));
					employee_res.setNotes(notes);
					
					// attribute [Employee.PhotoPath]
					String photoPath = Util.getStringValue(r.getAs("PhotoPath"));
					employee_res.setPhotoPath(photoPath);
					
					// attribute [Employee.Salary]
					Double salary = Util.getDoubleValue(r.getAs("Salary"));
					employee_res.setSalary(salary);
	
					// Get reference column [EmployeeID ] for reference [employeeRef]
					String myRedis_orderShipping_employeeRef_target_EmployeeID = r.getAs("EmployeeID") == null ? null : r.getAs("EmployeeID").toString();
					employee_res.setMyRedis_orderShipping_employeeRef_target_EmployeeID(myRedis_orderShipping_employeeRef_target_EmployeeID);
	
	
					return employee_res;
				}, Encoders.bean(EmployeeTDO.class));
	
	
		return res;}
	
	
	
	
	public Dataset<Sold> getSoldList(
		Condition<OrderAttribute> order_condition,
		Condition<EmployeeAttribute> employee_condition){
			SoldServiceImpl soldService = this;
			OrderService orderService = new OrderServiceImpl();  
			EmployeeService employeeService = new EmployeeServiceImpl();
			MutableBoolean order_refilter = new MutableBoolean(false);
			List<Dataset<Sold>> datasetsPOJO = new ArrayList<Dataset<Sold>>();
			boolean all_already_persisted = false;
			MutableBoolean employee_refilter = new MutableBoolean(false);
			
			org.apache.spark.sql.Column joinCondition = null;
			// For role 'order' in reference 'employeeRef'. A->B Scenario
			employee_refilter = new MutableBoolean(false);
			Dataset<OrderTDO> orderTDOemployeeReforder = soldService.getOrderTDOListOrderInEmployeeRefInOrderShippingFromMyRedis(order_condition, order_refilter);
			Dataset<EmployeeTDO> employeeTDOemployeeRefemployee = soldService.getEmployeeTDOListEmployeeInEmployeeRefInOrderShippingFromMyRedis(employee_condition, employee_refilter);
			
			Dataset<Row> res_employeeRef_temp = orderTDOemployeeReforder.join(employeeTDOemployeeRefemployee
					.withColumnRenamed("employeeID", "Employee_employeeID")
					.withColumnRenamed("lastName", "Employee_lastName")
					.withColumnRenamed("firstName", "Employee_firstName")
					.withColumnRenamed("title", "Employee_title")
					.withColumnRenamed("titleOfCourtesy", "Employee_titleOfCourtesy")
					.withColumnRenamed("birthDate", "Employee_birthDate")
					.withColumnRenamed("hireDate", "Employee_hireDate")
					.withColumnRenamed("address", "Employee_address")
					.withColumnRenamed("city", "Employee_city")
					.withColumnRenamed("region", "Employee_region")
					.withColumnRenamed("postalCode", "Employee_postalCode")
					.withColumnRenamed("country", "Employee_country")
					.withColumnRenamed("homePhone", "Employee_homePhone")
					.withColumnRenamed("extension", "Employee_extension")
					.withColumnRenamed("photo", "Employee_photo")
					.withColumnRenamed("notes", "Employee_notes")
					.withColumnRenamed("photoPath", "Employee_photoPath")
					.withColumnRenamed("salary", "Employee_salary")
					.withColumnRenamed("logEvents", "Employee_logEvents"),
					orderTDOemployeeReforder.col("myRedis_orderShipping_employeeRef_source_EmployeeRef").equalTo(employeeTDOemployeeRefemployee.col("myRedis_orderShipping_employeeRef_target_EmployeeID")));
		
			Dataset<Sold> res_employeeRef = res_employeeRef_temp.map(
				(MapFunction<Row, Sold>) r -> {
					Sold res = new Sold();
					Order A = new Order();
					Employee B = new Employee();
					A.setOrderID(Util.getIntegerValue(r.getAs("orderID")));
					A.setOrderDate(Util.getLocalDateValue(r.getAs("orderDate")));
					A.setRequiredDate(Util.getLocalDateValue(r.getAs("requiredDate")));
					A.setShippedDate(Util.getLocalDateValue(r.getAs("shippedDate")));
					A.setFreight(Util.getDoubleValue(r.getAs("freight")));
					A.setShipName(Util.getStringValue(r.getAs("shipName")));
					A.setShipAddress(Util.getStringValue(r.getAs("shipAddress")));
					A.setShipCity(Util.getStringValue(r.getAs("shipCity")));
					A.setShipRegion(Util.getStringValue(r.getAs("shipRegion")));
					A.setShipPostalCode(Util.getStringValue(r.getAs("shipPostalCode")));
					A.setShipCountry(Util.getStringValue(r.getAs("shipCountry")));
					A.setLogEvents((ArrayList<String>) ScalaUtil.javaList(r.getAs("logEvents")));
		
					B.setEmployeeID(Util.getIntegerValue(r.getAs("Employee_employeeID")));
					B.setLastName(Util.getStringValue(r.getAs("Employee_lastName")));
					B.setFirstName(Util.getStringValue(r.getAs("Employee_firstName")));
					B.setTitle(Util.getStringValue(r.getAs("Employee_title")));
					B.setTitleOfCourtesy(Util.getStringValue(r.getAs("Employee_titleOfCourtesy")));
					B.setBirthDate(Util.getLocalDateValue(r.getAs("Employee_birthDate")));
					B.setHireDate(Util.getLocalDateValue(r.getAs("Employee_hireDate")));
					B.setAddress(Util.getStringValue(r.getAs("Employee_address")));
					B.setCity(Util.getStringValue(r.getAs("Employee_city")));
					B.setRegion(Util.getStringValue(r.getAs("Employee_region")));
					B.setPostalCode(Util.getStringValue(r.getAs("Employee_postalCode")));
					B.setCountry(Util.getStringValue(r.getAs("Employee_country")));
					B.setHomePhone(Util.getStringValue(r.getAs("Employee_homePhone")));
					B.setExtension(Util.getStringValue(r.getAs("Employee_extension")));
					B.setPhoto(Util.getByteArrayValue(r.getAs("Employee_photo")));
					B.setNotes(Util.getStringValue(r.getAs("Employee_notes")));
					B.setPhotoPath(Util.getStringValue(r.getAs("Employee_photoPath")));
					B.setSalary(Util.getDoubleValue(r.getAs("Employee_salary")));
					B.setLogEvents((ArrayList<String>) ScalaUtil.javaList(r.getAs("Employee_logEvents")));
						
					res.setOrder(A);
					res.setEmployee(B);
					return res;
				},Encoders.bean(Sold.class)
			);
		
			datasetsPOJO.add(res_employeeRef);
		
			
			Dataset<Sold> res_sold_order;
			Dataset<Order> res_Order;
			
			
			//Join datasets or return 
			Dataset<Sold> res = fullOuterJoinsSold(datasetsPOJO);
			if(res == null)
				return null;
		
			Dataset<Order> lonelyOrder = null;
			Dataset<Employee> lonelyEmployee = null;
			
			List<Dataset<Order>> lonelyorderList = new ArrayList<Dataset<Order>>();
			lonelyorderList.add(orderService.getOrderListInCustomersFromMyMongoDB(order_condition, new MutableBoolean(false)));
			lonelyOrder = OrderService.fullOuterJoinsOrder(lonelyorderList);
			if(lonelyOrder != null) {
				res = fullLeftOuterJoinBetweenSoldAndOrder(res, lonelyOrder);
			}	
		
		
			
			if(order_refilter.booleanValue() || employee_refilter.booleanValue())
				res = res.filter((FilterFunction<Sold>) r -> (order_condition == null || order_condition.evaluate(r.getOrder())) && (employee_condition == null || employee_condition.evaluate(r.getEmployee())));
			
		
			return res;
		
		}
	
	public Dataset<Sold> getSoldListByOrderCondition(
		Condition<OrderAttribute> order_condition
	){
		return getSoldList(order_condition, null);
	}
	
	public Sold getSoldByOrder(Order order) {
		Condition<OrderAttribute> cond = null;
		cond = Condition.simple(OrderAttribute.orderID, Operator.EQUALS, order.getOrderID());
		Dataset<Sold> res = getSoldListByOrderCondition(cond);
		List<Sold> list = res.collectAsList();
		if(list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	public Dataset<Sold> getSoldListByEmployeeCondition(
		Condition<EmployeeAttribute> employee_condition
	){
		return getSoldList(null, employee_condition);
	}
	
	public Dataset<Sold> getSoldListByEmployee(Employee employee) {
		Condition<EmployeeAttribute> cond = null;
		cond = Condition.simple(EmployeeAttribute.employeeID, Operator.EQUALS, employee.getEmployeeID());
		Dataset<Sold> res = getSoldListByEmployeeCondition(cond);
	return res;
	}
	
	
	
	public void deleteSoldList(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.EmployeeAttribute> employee_condition){
			//TODO
		}
	
	public void deleteSoldListByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteSoldList(order_condition, null);
	}
	
	public void deleteSoldByOrder(pojo.Order order) {
		// TODO using id for selecting
		return;
	}
	public void deleteSoldListByEmployeeCondition(
		conditions.Condition<conditions.EmployeeAttribute> employee_condition
	){
		deleteSoldList(null, employee_condition);
	}
	
	public void deleteSoldListByEmployee(pojo.Employee employee) {
		// TODO using id for selecting
		return;
	}
		
}
