package dao.impl;

import exceptions.PhysicalStructureException;
import java.util.Arrays;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.apache.commons.lang3.StringUtils;
import util.Dataset;
import conditions.Condition;
import java.util.HashSet;
import java.util.Set;
import conditions.AndCondition;
import conditions.OrCondition;
import conditions.SimpleCondition;
import conditions.ComposedOfAttribute;
import conditions.Operator;
import tdo.*;
import pojo.*;
import tdo.OrderTDO;
import tdo.ComposedOfTDO;
import conditions.OrderAttribute;
import dao.services.OrderService;
import tdo.ProductTDO;
import tdo.ComposedOfTDO;
import conditions.ProductAttribute;
import dao.services.ProductService;
import java.util.List;
import java.util.ArrayList;
import util.ScalaUtil;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.commons.lang3.mutable.MutableBoolean;
import util.Util;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import util.Row;
import org.apache.spark.sql.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import util.WrappedArray;
import org.apache.spark.api.java.function.FlatMapFunction;
import dbconnection.SparkConnectionMgr;
import dbconnection.DBConnectionMgr;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.ArrayType;
import static com.mongodb.client.model.Updates.addToSet;
import org.bson.Document;
import org.bson.conversions.Bson;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

public class ComposedOfServiceImpl extends dao.services.ComposedOfService {
	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ComposedOfServiceImpl.class);
	// A<-AB->B . getAListInREL
	//join structure
	// Left side 'OrderRef' of reference [orderRef ]
	public Dataset<OrderTDO> getOrderTDOListOrderInOrderRefInOrderShippingFromMyRedis(Condition<OrderAttribute> condition, MutableBoolean refilterFlag){	
		// Build the key pattern
		//  - If the condition attribute is in the key pattern, replace by the value. Only if operator is EQUALS.
		//  - Replace all other fields of key pattern by a '*' 
		String keypattern= "", keypatternAllVariables="";
		String valueCond=null;
		String finalKeypattern;
		List<String> fieldsListInKey = new ArrayList<>();
		Set<OrderAttribute> keyAttributes = new HashSet<>();
		keypattern=keypattern.concat("ORDER:");
		keypatternAllVariables=keypatternAllVariables.concat("ORDER:");
		if(!Util.containsOrCondition(condition)){
			valueCond=Util.getStringValue(Util.getValueOfAttributeInEqualCondition(condition,OrderAttribute.orderID));
			keyAttributes.add(OrderAttribute.orderID);
		}
		else{
			valueCond=null;
			refilterFlag.setValue(true);
		}
		if(valueCond==null)
			keypattern=keypattern.concat("*");
		else
			keypattern=keypattern.concat(valueCond);
		fieldsListInKey.add("orderid");
		keypatternAllVariables=keypatternAllVariables.concat("*");
		if(!refilterFlag.booleanValue()){
			Set<OrderAttribute> conditionAttributes = Util.getConditionAttributes(condition);
			for (OrderAttribute a : conditionAttributes) {
				if (!keyAttributes.contains(a)) {
					refilterFlag.setValue(true);
					break;
				}
			}
		}
	
			
		// Find the type of query to perform in order to retrieve a Dataset<Row>
		// Based on the type of the value. Is a it a simple string or a hash or a list... 
		Dataset<Row> rows;
		StructType structType = new StructType(new StructField[] {
			DataTypes.createStructField("_id", DataTypes.StringType, true), //technical field to store the key.
			DataTypes.createStructField("OrderDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("RequiredDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShippedDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("Freight", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipName", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipAddress", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipCity", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipRegion", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipPostalCode", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipCountry", DataTypes.StringType, true)
	,		DataTypes.createStructField("CustomerRef", DataTypes.StringType, true)
	,		DataTypes.createStructField("EmployeeRef", DataTypes.StringType, true)
		});
		rows = SparkConnectionMgr.getRowsFromKeyValueHashes("myRedis",keypattern, structType);
		if(rows == null || rows.isEmpty())
				return null;
		boolean isStriped = false;
		String prefix=isStriped?keypattern.substring(0, keypattern.length() - 1):"";
		finalKeypattern = keypatternAllVariables;
		Dataset<OrderTDO> res = rows.map((MapFunction<Row, OrderTDO>) r -> {
					OrderTDO order_res = new OrderTDO();
					Integer groupindex = null;
					String regex = null;
					String value = null;
					Pattern p, pattern = null;
					Matcher m, match = null;
					boolean matches = false;
					String key = isStriped ? prefix + r.getAs("_id") : r.getAs("_id");
					// Spark Redis automatically strips leading character if the pattern provided contains a single '*' at the end.				
					pattern = Pattern.compile("\\*");
			        match = pattern.matcher(finalKeypattern);
					regex = finalKeypattern.replaceAll("\\*","(.*)");
					p = Pattern.compile(regex);
					m = p.matcher(key);
					matches = m.find();
					// attribute [Order.OrderID]
					// Attribute mapped in a key.
					groupindex = fieldsListInKey.indexOf("orderid")+1;
					if(groupindex==null) {
						logger.warn("Attribute 'Order' mapped physical field 'orderid' found in key but can't get index in build keypattern '{}'.", finalKeypattern);
					}
					String orderID = null;
					if(matches) {
						orderID = m.group(groupindex.intValue());
					} else {
						logger.warn("Cannot retrieve value for OrderorderID attribute stored in db myRedis. Regex [{}] Value [{}]",regex,value);
						order_res.addLogEvent("Cannot retrieve value for Order.orderID attribute stored in db myRedis. Probably due to an ambiguous regex.");
					}
					order_res.setOrderID(orderID == null ? null : Integer.parseInt(orderID));
					// attribute [Order.OrderDate]
					LocalDate orderDate = r.getAs("OrderDate") == null ? null : LocalDate.parse(r.getAs("OrderDate"));
					order_res.setOrderDate(orderDate);
					// attribute [Order.RequiredDate]
					LocalDate requiredDate = r.getAs("RequiredDate") == null ? null : LocalDate.parse(r.getAs("RequiredDate"));
					order_res.setRequiredDate(requiredDate);
					// attribute [Order.ShippedDate]
					LocalDate shippedDate = r.getAs("ShippedDate") == null ? null : LocalDate.parse(r.getAs("ShippedDate"));
					order_res.setShippedDate(shippedDate);
					// attribute [Order.Freight]
					Double freight = r.getAs("Freight") == null ? null : Double.parseDouble(r.getAs("Freight"));
					order_res.setFreight(freight);
					// attribute [Order.ShipName]
					String shipName = r.getAs("ShipName") == null ? null : r.getAs("ShipName");
					order_res.setShipName(shipName);
					// attribute [Order.ShipAddress]
					String shipAddress = r.getAs("ShipAddress") == null ? null : r.getAs("ShipAddress");
					order_res.setShipAddress(shipAddress);
					// attribute [Order.ShipCity]
					String shipCity = r.getAs("ShipCity") == null ? null : r.getAs("ShipCity");
					order_res.setShipCity(shipCity);
					// attribute [Order.ShipRegion]
					String shipRegion = r.getAs("ShipRegion") == null ? null : r.getAs("ShipRegion");
					order_res.setShipRegion(shipRegion);
					// attribute [Order.ShipPostalCode]
					String shipPostalCode = r.getAs("ShipPostalCode") == null ? null : r.getAs("ShipPostalCode");
					order_res.setShipPostalCode(shipPostalCode);
					// attribute [Order.ShipCountry]
					String shipCountry = r.getAs("ShipCountry") == null ? null : r.getAs("ShipCountry");
					order_res.setShipCountry(shipCountry);
					//Checking that reference field 'orderid' is mapped in Key
					if(fieldsListInKey.contains("orderid")){
						//Retrieving reference field 'orderid' in Key
						Pattern pattern_orderid = Pattern.compile("\\*");
				        Matcher match_orderid = pattern_orderid.matcher(finalKeypattern);
						regex = finalKeypattern.replaceAll("\\*","(.*)");
						groupindex = fieldsListInKey.indexOf("orderid")+1;
						if(groupindex==null) {
							logger.warn("Attribute 'Order' mapped physical field 'orderid' found in key but can't get index in build keypattern '{}'.", finalKeypattern);
						}
						p = Pattern.compile(regex);
						m = p.matcher(key);
						matches = m.find();
						String orderRef_orderid = null;
						if(matches) {
						orderRef_orderid = m.group(groupindex.intValue());
						} else {
						logger.warn("Cannot retrieve value 'orderid'. Regex [{}] Value [{}]",regex,value);
						order_res.addLogEvent("Cannot retrieve value for 'orderid' attribute stored in db myRedis. Probably due to an ambiguous regex.");
						}
						order_res.setReldata_Order_Details_orderRef_target_orderid(orderRef_orderid);
					}
	
						return order_res;
				}, Encoders.bean(OrderTDO.class));
		res=res.dropDuplicates(new String[] {"orderID"});
		return res;
	}
	
	public static Pair<String, List<String>> getSQLWhereClauseInOrder_DetailsFromReldata(Condition<ComposedOfAttribute> condition, MutableBoolean refilterFlag) {
		return getSQLWhereClauseInOrder_DetailsFromReldataWithTableAlias(condition, refilterFlag, "");
	}
	
	public static List<String> getSQLSetClauseInOrder_DetailsFromReldata(conditions.SetClause<ComposedOfAttribute> set) {
		List<String> res = new ArrayList<String>();
		if(set != null) {
			java.util.Map<String, java.util.Map<String, String>> longFieldValues = new java.util.HashMap<String, java.util.Map<String, String>>();
			java.util.Map<ComposedOfAttribute, Object> clause = set.getClause();
			for(java.util.Map.Entry<ComposedOfAttribute, Object> e : clause.entrySet()) {
				ComposedOfAttribute attr = e.getKey();
				Object value = e.getValue();
				if(attr == ComposedOfAttribute.unitPrice ) {
					res.add("UnitPrice = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ComposedOfAttribute.quantity ) {
					res.add("Quantity = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == ComposedOfAttribute.discount ) {
					res.add("Discount = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
			}
	
			for(java.util.Map.Entry<String, java.util.Map<String, String>> entry : longFieldValues.entrySet()) {
				String longField = entry.getKey();
				java.util.Map<String, String> values = entry.getValue();
			}
	
		}
		return res;
	}
	
	public static Pair<String, List<String>> getSQLWhereClauseInOrder_DetailsFromReldataWithTableAlias(Condition<ComposedOfAttribute> condition, MutableBoolean refilterFlag, String tableAlias) {
		String where = null;	
		List<String> preparedValues = new java.util.ArrayList<String>();
		if(condition != null) {
			
			if(condition instanceof SimpleCondition) {
				ComposedOfAttribute attr = ((SimpleCondition<ComposedOfAttribute>) condition).getAttribute();
				Operator op = ((SimpleCondition<ComposedOfAttribute>) condition).getOperator();
				Object value = ((SimpleCondition<ComposedOfAttribute>) condition).getValue();
				if(value != null) {
					boolean isConditionAttrEncountered = false;
					if(attr == ComposedOfAttribute.unitPrice ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "UnitPrice " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ComposedOfAttribute.quantity ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Quantity " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == ComposedOfAttribute.discount ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Discount " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(!isConditionAttrEncountered) {
						refilterFlag.setValue(true);
						where = "1 = 1";
					}
				} else {
					if(attr == ComposedOfAttribute.unitPrice ) {
						if(op == Operator.EQUALS)
							where =  "UnitPrice IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "UnitPrice IS NOT NULL";
					}
					if(attr == ComposedOfAttribute.quantity ) {
						if(op == Operator.EQUALS)
							where =  "Quantity IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Quantity IS NOT NULL";
					}
					if(attr == ComposedOfAttribute.discount ) {
						if(op == Operator.EQUALS)
							where =  "Discount IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Discount IS NOT NULL";
					}
				}
			}
	
			if(condition instanceof AndCondition) {
				Pair<String, List<String>> pairLeft = getSQLWhereClauseInOrder_DetailsFromReldata(((AndCondition) condition).getLeftCondition(), refilterFlag);
				Pair<String, List<String>> pairRight = getSQLWhereClauseInOrder_DetailsFromReldata(((AndCondition) condition).getRightCondition(), refilterFlag);
				String whereLeft = pairLeft.getKey();
				String whereRight = pairRight.getKey();
				List<String> leftValues = pairLeft.getValue();
				List<String> rightValues = pairRight.getValue();
				if(whereLeft != null || whereRight != null) {
					if(whereLeft == null)
						where = whereRight;
					else
						if(whereRight == null)
							where = whereLeft;
						else
							where = "(" + whereLeft + " AND " + whereRight + ")";
					preparedValues.addAll(leftValues);
					preparedValues.addAll(rightValues);
				}
			}
	
			if(condition instanceof OrCondition) {
				Pair<String, List<String>> pairLeft = getSQLWhereClauseInOrder_DetailsFromReldata(((OrCondition) condition).getLeftCondition(), refilterFlag);
				Pair<String, List<String>> pairRight = getSQLWhereClauseInOrder_DetailsFromReldata(((OrCondition) condition).getRightCondition(), refilterFlag);
				String whereLeft = pairLeft.getKey();
				String whereRight = pairRight.getKey();
				List<String> leftValues = pairLeft.getValue();
				List<String> rightValues = pairRight.getValue();
				if(whereLeft != null || whereRight != null) {
					if(whereLeft == null)
						where = whereRight;
					else
						if(whereRight == null)
							where = whereLeft;
						else
							where = "(" + whereLeft + " OR " + whereRight + ")";
					preparedValues.addAll(leftValues);
					preparedValues.addAll(rightValues);
				}
			}
	
		}
	
		return new ImmutablePair<String, List<String>>(where, preparedValues);
	}
	
	
	
	
	
	// A<-AB->B . getBListInREL
	
	
	
	public Dataset<ComposedOfTDO> getComposedOfTDOListInProductsAndOrder_DetailsFromreldata(Condition<ProductAttribute> product_cond, Condition<ComposedOfAttribute> composedOf_cond, MutableBoolean refilterFlag, MutableBoolean composedOf_refilter) {
		Pair<String, List<String>> whereClause = ProductServiceImpl.getSQLWhereClauseInProductsFromReldataWithTableAlias(product_cond, refilterFlag, "Products.");
		Pair<String, List<String>> whereClause2 = ComposedOfServiceImpl.getSQLWhereClauseInOrder_DetailsFromReldataWithTableAlias(composedOf_cond, composedOf_refilter, "Order_Details.");
		
		String where1 = whereClause.getKey();
		List<String> preparedValues = whereClause.getValue();
		for(String preparedValue : preparedValues) {
			where1 = where1.replaceFirst("\\?", preparedValue);
		}
	
		String where2 = whereClause2.getKey();
		preparedValues = whereClause2.getValue();
		for(String preparedValue : preparedValues) {
			where2 = where2.replaceFirst("\\?", preparedValue);
		}
		
		String where = "";
		if(where1 != null)
			where = " AND " + where1;
		if(where2 != null)
			where = " AND " + where2;
	
		String aliasedColumns = "Products.ProductID as Products_ProductID,Products.ProductName as Products_ProductName,Products.SupplierRef as Products_SupplierRef,Products.QuantityPerUnit as Products_QuantityPerUnit,Products.UnitPrice as Products_UnitPrice,Products.UnitsInStock as Products_UnitsInStock,Products.UnitsOnOrder as Products_UnitsOnOrder,Products.ReorderLevel as Products_ReorderLevel,Products.Discontinued as Products_Discontinued, Order_Details.OrderRef as Order_Details_OrderRef,Order_Details.ProductRef as Order_Details_ProductRef,Order_Details.UnitPrice as Order_Details_UnitPrice,Order_Details.Quantity as Order_Details_Quantity,Order_Details.Discount as Order_Details_Discount";
		Dataset<Row> d = dbconnection.SparkConnectionMgr.getDataset("reldata", "(SELECT " + aliasedColumns + " FROM Products, Order_Details WHERE Order_Details.ProductRef = Products.ProductID" + where + ") AS JOIN_TABLE", null);
		Dataset<ComposedOfTDO> res = d.map((MapFunction<Row, ComposedOfTDO>) r -> {
					ComposedOfTDO composedOf_res = new ComposedOfTDO();
					composedOf_res.setProduct(new Product());
					
					Integer groupIndex = null;
					String regex = null;
					String value = null;
					Pattern p = null;
					Matcher m = null;
					boolean matches = false;
					
					// attribute [ComposedOf.UnitPrice]
					Double composedOf_unitPrice = Util.getDoubleValue(r.getAs("Order_Details_UnitPrice"));
					composedOf_res.setUnitPrice(composedOf_unitPrice);
					
					// attribute [ComposedOf.Quantity]
					Integer composedOf_quantity = Util.getIntegerValue(r.getAs("Order_Details_Quantity"));
					composedOf_res.setQuantity(composedOf_quantity);
					
					// attribute [ComposedOf.Discount]
					Double composedOf_discount = Util.getDoubleValue(r.getAs("Order_Details_Discount"));
					composedOf_res.setDiscount(composedOf_discount);
		
		
					
					// attribute [Product.ProductID]
					Integer product_productID = Util.getIntegerValue(r.getAs("Products_ProductID"));
					composedOf_res.getProduct().setProductID(product_productID);
					
					// attribute [Product.ProductName]
					String product_productName = Util.getStringValue(r.getAs("Products_ProductName"));
					composedOf_res.getProduct().setProductName(product_productName);
					
					// attribute [Product.SupplierRef]
					Integer product_supplierRef = Util.getIntegerValue(r.getAs("Products_SupplierRef"));
					composedOf_res.getProduct().setSupplierRef(product_supplierRef);
					
					// attribute [Product.QuantityPerUnit]
					String product_quantityPerUnit = Util.getStringValue(r.getAs("Products_QuantityPerUnit"));
					composedOf_res.getProduct().setQuantityPerUnit(product_quantityPerUnit);
					
					// attribute [Product.UnitPrice]
					Double product_unitPrice = Util.getDoubleValue(r.getAs("Products_UnitPrice"));
					composedOf_res.getProduct().setUnitPrice(product_unitPrice);
					
					// attribute [Product.UnitsInStock]
					Integer product_unitsInStock = Util.getIntegerValue(r.getAs("Products_UnitsInStock"));
					composedOf_res.getProduct().setUnitsInStock(product_unitsInStock);
					
					// attribute [Product.UnitsOnOrder]
					Integer product_unitsOnOrder = Util.getIntegerValue(r.getAs("Products_UnitsOnOrder"));
					composedOf_res.getProduct().setUnitsOnOrder(product_unitsOnOrder);
					
					// attribute [Product.ReorderLevel]
					Integer product_reorderLevel = Util.getIntegerValue(r.getAs("Products_ReorderLevel"));
					composedOf_res.getProduct().setReorderLevel(product_reorderLevel);
					
					// attribute [Product.Discontinued]
					Boolean product_discontinued = Util.getBooleanValue(r.getAs("Products_Discontinued"));
					composedOf_res.getProduct().setDiscontinued(product_discontinued);
					
					String orderRef_OrderRef = r.getAs("Order_Details_OrderRef") == null ? null : r.getAs("Order_Details_OrderRef").toString();
					composedOf_res.setReldata_Order_Details_orderRef_source_OrderRef(orderRef_OrderRef);
					
		
					return composedOf_res;
				}, Encoders.bean(ComposedOfTDO.class));
		
		return res;
	}
	
	
	
	
	
	
	public Dataset<ComposedOf> getComposedOfList(
		Condition<OrderAttribute> order_condition,
		Condition<ProductAttribute> product_condition,
		Condition<ComposedOfAttribute> composedOf_condition
	){
		ComposedOfServiceImpl composedOfService = this;
		OrderService orderService = new OrderServiceImpl();  
		ProductService productService = new ProductServiceImpl();
		MutableBoolean order_refilter = new MutableBoolean(false);
		List<Dataset<ComposedOf>> datasetsPOJO = new ArrayList<Dataset<ComposedOf>>();
		boolean all_already_persisted = false;
		MutableBoolean product_refilter = new MutableBoolean(false);
		MutableBoolean composedOf_refilter = new MutableBoolean(false);
		org.apache.spark.sql.Column joinCondition = null;
		// join physical structure A<-AB->B
		//join between 2 SQL tables and a non-relational structure
		// (A) (AB - B)
		product_refilter = new MutableBoolean(false);
		Dataset<ComposedOfTDO> res_composedOf_productRef_orderRef = composedOfService.getComposedOfTDOListInProductsAndOrder_DetailsFromreldata(product_condition, composedOf_condition, product_refilter, composedOf_refilter);
		Dataset<OrderTDO> res_orderRef_productRef = composedOfService.getOrderTDOListOrderInOrderRefInOrderShippingFromMyRedis(order_condition, order_refilter);
		Dataset<Row> B_res_orderRef_productRef = res_orderRef_productRef
			.withColumnRenamed("orderID", "B_orderID")
			.withColumnRenamed("orderDate", "B_orderDate")
			.withColumnRenamed("requiredDate", "B_requiredDate")
			.withColumnRenamed("shippedDate", "B_shippedDate")
			.withColumnRenamed("freight", "B_freight")
			.withColumnRenamed("shipName", "B_shipName")
			.withColumnRenamed("shipAddress", "B_shipAddress")
			.withColumnRenamed("shipCity", "B_shipCity")
			.withColumnRenamed("shipRegion", "B_shipRegion")
			.withColumnRenamed("shipPostalCode", "B_shipPostalCode")
			.withColumnRenamed("shipCountry", "B_shipCountry")
			.withColumnRenamed("logEvents", "B_logEvents");
		
		Dataset<Row> res_row_productRef_orderRef = res_composedOf_productRef_orderRef.join(B_res_orderRef_productRef,
			res_composedOf_productRef_orderRef.col("reldata_Order_Details_orderRef_source_OrderRef").equalTo(B_res_orderRef_productRef.col("reldata_Order_Details_orderRef_target_orderid")));
		Dataset<ComposedOf> res_Order_orderRef = res_row_productRef_orderRef.map((MapFunction<Row, ComposedOf>) r -> {
					ComposedOf res = new ComposedOf();
					
					Order B = new Order();
					Product A = new Product();
						
					Object o = r.getAs("product");
					if(o != null) {
						if(o instanceof Row) {
							Row r2 = (Row) o;
							A.setProductID(Util.getIntegerValue(r2.getAs("productID")));
							A.setProductName(Util.getStringValue(r2.getAs("productName")));
							A.setSupplierRef(Util.getIntegerValue(r2.getAs("supplierRef")));
							A.setQuantityPerUnit(Util.getStringValue(r2.getAs("quantityPerUnit")));
							A.setUnitPrice(Util.getDoubleValue(r2.getAs("unitPrice")));
							A.setUnitsInStock(Util.getIntegerValue(r2.getAs("unitsInStock")));
							A.setUnitsOnOrder(Util.getIntegerValue(r2.getAs("unitsOnOrder")));
							A.setReorderLevel(Util.getIntegerValue(r2.getAs("reorderLevel")));
							A.setDiscontinued(Util.getBooleanValue(r2.getAs("discontinued")));
							A.setLogEvents((ArrayList<String>) ScalaUtil.javaList(r2.getAs("logEvents")));
						}
						if(o instanceof Product)
							A = (Product) o;
					}
		
		
					res.setUnitPrice(Util.getDoubleValue(r.getAs("unitPrice")));
					res.setQuantity(Util.getIntegerValue(r.getAs("quantity")));
					res.setDiscount(Util.getDoubleValue(r.getAs("discount")));
					res.setLogEvents((ArrayList<String>) ScalaUtil.javaList(r.getAs("logEvents")));
		
					B.setOrderID(Util.getIntegerValue(r.getAs("B_orderID")));
					B.setOrderDate(Util.getLocalDateValue(r.getAs("B_orderDate")));
					B.setRequiredDate(Util.getLocalDateValue(r.getAs("B_requiredDate")));
					B.setShippedDate(Util.getLocalDateValue(r.getAs("B_shippedDate")));
					B.setFreight(Util.getDoubleValue(r.getAs("B_freight")));
					B.setShipName(Util.getStringValue(r.getAs("B_shipName")));
					B.setShipAddress(Util.getStringValue(r.getAs("B_shipAddress")));
					B.setShipCity(Util.getStringValue(r.getAs("B_shipCity")));
					B.setShipRegion(Util.getStringValue(r.getAs("B_shipRegion")));
					B.setShipPostalCode(Util.getStringValue(r.getAs("B_shipPostalCode")));
					B.setShipCountry(Util.getStringValue(r.getAs("B_shipCountry")));
					B.setLogEvents((ArrayList<String>) ScalaUtil.javaList(r.getAs("B_logEvents")));
						
					res.setProduct(A);
					res.setOrder(B);
					return res;
				}, Encoders.bean(ComposedOf.class));
		
		datasetsPOJO.add(res_Order_orderRef);
		
	
		
		Dataset<ComposedOf> res_composedOf_order;
		Dataset<Order> res_Order;
		
		
		//Join datasets or return 
		Dataset<ComposedOf> res = fullOuterJoinsComposedOf(datasetsPOJO);
		if(res == null)
			return null;
	
		Dataset<Order> lonelyOrder = null;
		Dataset<Product> lonelyProduct = null;
		
		List<Dataset<Order>> lonelyorderList = new ArrayList<Dataset<Order>>();
		lonelyorderList.add(orderService.getOrderListInCustomersFromMyMongoDB(order_condition, new MutableBoolean(false)));
		lonelyOrder = OrderService.fullOuterJoinsOrder(lonelyorderList);
		if(lonelyOrder != null) {
			res = fullLeftOuterJoinBetweenComposedOfAndOrder(res, lonelyOrder);
		}	
	
	
		
		if(order_refilter.booleanValue() || product_refilter.booleanValue() || composedOf_refilter.booleanValue())
			res = res.filter((FilterFunction<ComposedOf>) r -> (order_condition == null || order_condition.evaluate(r.getOrder())) && (product_condition == null || product_condition.evaluate(r.getProduct())) && (composedOf_condition == null || composedOf_condition.evaluate(r)));
		
	
		return res;
	
	}
	
	public Dataset<ComposedOf> getComposedOfListByOrderCondition(
		Condition<OrderAttribute> order_condition
	){
		return getComposedOfList(order_condition, null, null);
	}
	
	public Dataset<ComposedOf> getComposedOfListByOrder(Order order) {
		Condition<OrderAttribute> cond = null;
		cond = Condition.simple(OrderAttribute.orderID, Operator.EQUALS, order.getOrderID());
		Dataset<ComposedOf> res = getComposedOfListByOrderCondition(cond);
	return res;
	}
	public Dataset<ComposedOf> getComposedOfListByProductCondition(
		Condition<ProductAttribute> product_condition
	){
		return getComposedOfList(null, product_condition, null);
	}
	
	public Dataset<ComposedOf> getComposedOfListByProduct(Product product) {
		Condition<ProductAttribute> cond = null;
		cond = Condition.simple(ProductAttribute.productID, Operator.EQUALS, product.getProductID());
		Dataset<ComposedOf> res = getComposedOfListByProductCondition(cond);
	return res;
	}
	
	public Dataset<ComposedOf> getComposedOfListByComposedOfCondition(
		Condition<ComposedOfAttribute> composedOf_condition
	){
		return getComposedOfList(null, null, composedOf_condition);
	}
	
	public void insertComposedOf(ComposedOf composedOf){
		//Link entities in join structures.
		insertComposedOfInJoinStructOrder_DetailsInReldata(composedOf);
		// Update embedded structures mapped to non mandatory roles.
		// Update ref fields mapped to non mandatory roles. 
	}
	
	public 	boolean insertComposedOfInJoinStructOrder_DetailsInReldata(ComposedOf composedOf){
	 	// Rel 'composedOf' Insert in join structure 'Order_Details'
		
		Order order_order = composedOf.getOrder();
		Product product_product = composedOf.getProduct();
		List<String> columns = new ArrayList<>();
		List<Object> values = new ArrayList<>();
		List<List<Object>> rows = new ArrayList<>();
		columns.add("UnitPrice");
		values.add(composedOf.getUnitPrice());
		columns.add("Quantity");
		values.add(composedOf.getQuantity());
		columns.add("Discount");
		values.add(composedOf.getDiscount());
		// Role in join structure 
		columns.add("OrderRef");
		Object orderId = order_order.getOrderID();
		values.add(orderId);
		// Role in join structure 
		columns.add("ProductRef");
		Object productId = product_product.getProductID();
		values.add(productId);
		rows.add(values);
		DBConnectionMgr.insertInTable(columns, rows, "Order_Details", "reldata"); 					
		return true;
	
	}
	
	
	
	
	
	public void updateComposedOfList(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition,
		conditions.SetClause<conditions.ComposedOfAttribute> set
	){
		//TODO
	}
	
	public void updateComposedOfListByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.SetClause<conditions.ComposedOfAttribute> set
	){
		updateComposedOfList(order_condition, null, null, set);
	}
	
	public void updateComposedOfListByOrder(pojo.Order order, conditions.SetClause<conditions.ComposedOfAttribute> set) {
		// TODO using id for selecting
		return;
	}
	public void updateComposedOfListByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.SetClause<conditions.ComposedOfAttribute> set
	){
		updateComposedOfList(null, product_condition, null, set);
	}
	
	public void updateComposedOfListByProduct(pojo.Product product, conditions.SetClause<conditions.ComposedOfAttribute> set) {
		// TODO using id for selecting
		return;
	}
	
	public void updateComposedOfListByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition,
		conditions.SetClause<conditions.ComposedOfAttribute> set
	){
		updateComposedOfList(null, null, composedOf_condition, set);
	}
	
	public void deleteComposedOfList(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition){
			//TODO
		}
	
	public void deleteComposedOfListByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteComposedOfList(order_condition, null, null);
	}
	
	public void deleteComposedOfListByOrder(pojo.Order order) {
		// TODO using id for selecting
		return;
	}
	public void deleteComposedOfListByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition
	){
		deleteComposedOfList(null, product_condition, null);
	}
	
	public void deleteComposedOfListByProduct(pojo.Product product) {
		// TODO using id for selecting
		return;
	}
	
	public void deleteComposedOfListByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition
	){
		deleteComposedOfList(null, null, composedOf_condition);
	}
		
}
