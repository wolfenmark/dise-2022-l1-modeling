package dao.services;

import util.Dataset;
import util.Row;
import util.WrappedArray;
import pojo.Order;
import conditions.OrderAttribute;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.commons.lang3.mutable.MutableBoolean;
import conditions.Condition;
import conditions.Operator;
import util.*;
import conditions.OrderAttribute;
import pojo.Bought;
import conditions.CustomerAttribute;
import pojo.Customer;
import conditions.OrderAttribute;
import pojo.Sold;
import conditions.EmployeeAttribute;
import pojo.Employee;
import conditions.OrderAttribute;
import pojo.ComposedOf;
import conditions.ProductAttribute;
import pojo.Product;

public abstract class OrderService {
	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OrderService.class);
	protected BoughtService boughtService = new dao.impl.BoughtServiceImpl();
	protected SoldService soldService = new dao.impl.SoldServiceImpl();
	protected ComposedOfService composedOfService = new dao.impl.ComposedOfServiceImpl();
	


	public static enum ROLE_NAME {
		BOUGHT_BOUGHTORDER, SOLD_ORDER, COMPOSEDOF_ORDER
	}
	private static java.util.Map<ROLE_NAME, loading.Loading> defaultLoadingParameters = new java.util.HashMap<ROLE_NAME, loading.Loading>();
	static {
		defaultLoadingParameters.put(ROLE_NAME.BOUGHT_BOUGHTORDER, loading.Loading.EAGER);
		defaultLoadingParameters.put(ROLE_NAME.SOLD_ORDER, loading.Loading.EAGER);
		defaultLoadingParameters.put(ROLE_NAME.COMPOSEDOF_ORDER, loading.Loading.LAZY);
	}
	
	private java.util.Map<ROLE_NAME, loading.Loading> loadingParameters = new java.util.HashMap<ROLE_NAME, loading.Loading>();
	
	public OrderService() {
		for(java.util.Map.Entry<ROLE_NAME, loading.Loading> entry: defaultLoadingParameters.entrySet())
			loadingParameters.put(entry.getKey(), entry.getValue());
	}
	
	public OrderService(java.util.Map<ROLE_NAME, loading.Loading> loadingParams) {
		this();
		if(loadingParams != null)
			for(java.util.Map.Entry<ROLE_NAME, loading.Loading> entry: loadingParams.entrySet())
				loadingParameters.put(entry.getKey(), entry.getValue());
	}
	
	public static java.util.Map<ROLE_NAME, loading.Loading> getDefaultLoadingParameters() {
		java.util.Map<ROLE_NAME, loading.Loading> res = new java.util.HashMap<ROLE_NAME, loading.Loading>();
		for(java.util.Map.Entry<ROLE_NAME, loading.Loading> entry: defaultLoadingParameters.entrySet())
				res.put(entry.getKey(), entry.getValue());
		return res;
	}
	
	public static void setAllDefaultLoadingParameters(loading.Loading loading) {
		java.util.Map<ROLE_NAME, loading.Loading> newParams = new java.util.HashMap<ROLE_NAME, loading.Loading>();
		for(java.util.Map.Entry<ROLE_NAME, loading.Loading> entry: defaultLoadingParameters.entrySet())
				newParams.put(entry.getKey(), entry.getValue());
		defaultLoadingParameters = newParams;
	}
	
	public java.util.Map<ROLE_NAME, loading.Loading> getLoadingParameters() {
		return this.loadingParameters;
	}
	
	public void setLoadingParameters(java.util.Map<ROLE_NAME, loading.Loading> newParams) {
		this.loadingParameters = newParams;
	}
	
	public void updateLoadingParameter(ROLE_NAME role, loading.Loading l) {
		this.loadingParameters.put(role, l);
	}
	
	
	public Dataset<Order> getOrderList(){
		return getOrderList(null);
	}
	
	public Dataset<Order> getOrderList(conditions.Condition<conditions.OrderAttribute> condition){
		StopWatch stopwatch = new StopWatch();
		MutableBoolean refilterFlag = new MutableBoolean(false);
		List<Dataset<Order>> datasets = new ArrayList<Dataset<Order>>();
		Dataset<Order> d = null;
		d = getOrderListInOrderShippingFromMyRedis(condition, refilterFlag);
		if(d != null)
			datasets.add(d);
		d = getOrderListInCustomersFromMyMongoDB(condition, refilterFlag);
		if(d != null)
			datasets.add(d);
		
		if(datasets.size() == 0)
			return null;
	
		d = datasets.get(0);
		if(datasets.size() > 1) {
			d=fullOuterJoinsOrder(datasets);
		}
		if(refilterFlag.booleanValue())
			d = d.filter((FilterFunction<Order>) r -> condition == null || condition.evaluate(r));
		d = d.dropDuplicates(new String[] {"orderID"});
		logger.info("Execution time in seconds : ", stopwatch.getElapsedTimeInSeconds());
		return d;
	}
	
	
	
	
	
	public abstract Dataset<Order> getOrderListInOrderShippingFromMyRedis(conditions.Condition<conditions.OrderAttribute> condition, MutableBoolean refilterFlag);
	
	
	
	
	
	public abstract Dataset<Order> getOrderListInCustomersFromMyMongoDB(conditions.Condition<conditions.OrderAttribute> condition, MutableBoolean refilterFlag);
	
	
	public Order getOrderById(Integer orderID){
		Condition cond;
		cond = Condition.simple(OrderAttribute.orderID, conditions.Operator.EQUALS, orderID);
		Dataset<Order> res = getOrderList(cond);
		if(res!=null && !res.isEmpty())
			return res.first();
		return null;
	}
	
	public Dataset<Order> getOrderListByOrderID(Integer orderID) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.orderID, conditions.Operator.EQUALS, orderID));
	}
	
	public Dataset<Order> getOrderListByOrderDate(LocalDate orderDate) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.orderDate, conditions.Operator.EQUALS, orderDate));
	}
	
	public Dataset<Order> getOrderListByRequiredDate(LocalDate requiredDate) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.requiredDate, conditions.Operator.EQUALS, requiredDate));
	}
	
	public Dataset<Order> getOrderListByShippedDate(LocalDate shippedDate) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.shippedDate, conditions.Operator.EQUALS, shippedDate));
	}
	
	public Dataset<Order> getOrderListByFreight(Double freight) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.freight, conditions.Operator.EQUALS, freight));
	}
	
	public Dataset<Order> getOrderListByShipName(String shipName) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.shipName, conditions.Operator.EQUALS, shipName));
	}
	
	public Dataset<Order> getOrderListByShipAddress(String shipAddress) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.shipAddress, conditions.Operator.EQUALS, shipAddress));
	}
	
	public Dataset<Order> getOrderListByShipCity(String shipCity) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.shipCity, conditions.Operator.EQUALS, shipCity));
	}
	
	public Dataset<Order> getOrderListByShipRegion(String shipRegion) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.shipRegion, conditions.Operator.EQUALS, shipRegion));
	}
	
	public Dataset<Order> getOrderListByShipPostalCode(String shipPostalCode) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.shipPostalCode, conditions.Operator.EQUALS, shipPostalCode));
	}
	
	public Dataset<Order> getOrderListByShipCountry(String shipCountry) {
		return getOrderList(conditions.Condition.simple(conditions.OrderAttribute.shipCountry, conditions.Operator.EQUALS, shipCountry));
	}
	
	
	
	public static Dataset<Order> fullOuterJoinsOrder(List<Dataset<Order>> datasetsPOJO) {
		return fullOuterJoinsOrder(datasetsPOJO, "fullouter");
	}
	
	protected static Dataset<Order> fullLeftOuterJoinsOrder(List<Dataset<Order>> datasetsPOJO) {
		return fullOuterJoinsOrder(datasetsPOJO, "leftouter");
	}
	
	private static Dataset<Order> fullOuterJoinsOrder(List<Dataset<Order>> datasetsPOJO, String joinMode) {
		if(datasetsPOJO.size() == 0)
				return null;
		if(datasetsPOJO.size() == 1)
			return datasetsPOJO.get(0);
		Dataset<Order> d = datasetsPOJO.get(0);
			List<String> idFields = new ArrayList<String>();
			idFields.add("orderID");
			logger.debug("Start {} of [{}] datasets of [Order] objects",joinMode,datasetsPOJO.size());
			scala.collection.Seq<String> seq = scala.collection.JavaConverters.asScalaIteratorConverter(idFields.iterator()).asScala().toSeq();
			Dataset<Row> res = d.join(datasetsPOJO.get(1)
								.withColumnRenamed("orderDate", "orderDate_1")
								.withColumnRenamed("requiredDate", "requiredDate_1")
								.withColumnRenamed("shippedDate", "shippedDate_1")
								.withColumnRenamed("freight", "freight_1")
								.withColumnRenamed("shipName", "shipName_1")
								.withColumnRenamed("shipAddress", "shipAddress_1")
								.withColumnRenamed("shipCity", "shipCity_1")
								.withColumnRenamed("shipRegion", "shipRegion_1")
								.withColumnRenamed("shipPostalCode", "shipPostalCode_1")
								.withColumnRenamed("shipCountry", "shipCountry_1")
								.withColumnRenamed("logEvents", "logEvents_1")
							, seq, joinMode);
			for(int i = 2; i < datasetsPOJO.size(); i++) {
				res = res.join(datasetsPOJO.get(i)
								.withColumnRenamed("orderDate", "orderDate_" + i)
								.withColumnRenamed("requiredDate", "requiredDate_" + i)
								.withColumnRenamed("shippedDate", "shippedDate_" + i)
								.withColumnRenamed("freight", "freight_" + i)
								.withColumnRenamed("shipName", "shipName_" + i)
								.withColumnRenamed("shipAddress", "shipAddress_" + i)
								.withColumnRenamed("shipCity", "shipCity_" + i)
								.withColumnRenamed("shipRegion", "shipRegion_" + i)
								.withColumnRenamed("shipPostalCode", "shipPostalCode_" + i)
								.withColumnRenamed("shipCountry", "shipCountry_" + i)
								.withColumnRenamed("logEvents", "logEvents_" + i)
						, seq, joinMode);
			}
			logger.debug("End join. Start");
			logger.debug("Start transforming Row objects to [Order] objects"); 
			d = res.map((MapFunction<Row, Order>) r -> {
					Order order_res = new Order();
					
					// attribute 'Order.orderID'
					Integer firstNotNull_orderID = Util.getIntegerValue(r.getAs("orderID"));
					order_res.setOrderID(firstNotNull_orderID);
					
					// attribute 'Order.orderDate'
					LocalDate firstNotNull_orderDate = Util.getLocalDateValue(r.getAs("orderDate"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						LocalDate orderDate2 = Util.getLocalDateValue(r.getAs("orderDate_" + i));
						if (firstNotNull_orderDate != null && orderDate2 != null && !firstNotNull_orderDate.equals(orderDate2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.orderDate': " + firstNotNull_orderDate + " and " + orderDate2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.orderDate': " + firstNotNull_orderDate + " and " + orderDate2 + "." );
						}
						if (firstNotNull_orderDate == null && orderDate2 != null) {
							firstNotNull_orderDate = orderDate2;
						}
					}
					order_res.setOrderDate(firstNotNull_orderDate);
					
					// attribute 'Order.requiredDate'
					LocalDate firstNotNull_requiredDate = Util.getLocalDateValue(r.getAs("requiredDate"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						LocalDate requiredDate2 = Util.getLocalDateValue(r.getAs("requiredDate_" + i));
						if (firstNotNull_requiredDate != null && requiredDate2 != null && !firstNotNull_requiredDate.equals(requiredDate2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.requiredDate': " + firstNotNull_requiredDate + " and " + requiredDate2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.requiredDate': " + firstNotNull_requiredDate + " and " + requiredDate2 + "." );
						}
						if (firstNotNull_requiredDate == null && requiredDate2 != null) {
							firstNotNull_requiredDate = requiredDate2;
						}
					}
					order_res.setRequiredDate(firstNotNull_requiredDate);
					
					// attribute 'Order.shippedDate'
					LocalDate firstNotNull_shippedDate = Util.getLocalDateValue(r.getAs("shippedDate"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						LocalDate shippedDate2 = Util.getLocalDateValue(r.getAs("shippedDate_" + i));
						if (firstNotNull_shippedDate != null && shippedDate2 != null && !firstNotNull_shippedDate.equals(shippedDate2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shippedDate': " + firstNotNull_shippedDate + " and " + shippedDate2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shippedDate': " + firstNotNull_shippedDate + " and " + shippedDate2 + "." );
						}
						if (firstNotNull_shippedDate == null && shippedDate2 != null) {
							firstNotNull_shippedDate = shippedDate2;
						}
					}
					order_res.setShippedDate(firstNotNull_shippedDate);
					
					// attribute 'Order.freight'
					Double firstNotNull_freight = Util.getDoubleValue(r.getAs("freight"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						Double freight2 = Util.getDoubleValue(r.getAs("freight_" + i));
						if (firstNotNull_freight != null && freight2 != null && !firstNotNull_freight.equals(freight2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.freight': " + firstNotNull_freight + " and " + freight2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.freight': " + firstNotNull_freight + " and " + freight2 + "." );
						}
						if (firstNotNull_freight == null && freight2 != null) {
							firstNotNull_freight = freight2;
						}
					}
					order_res.setFreight(firstNotNull_freight);
					
					// attribute 'Order.shipName'
					String firstNotNull_shipName = Util.getStringValue(r.getAs("shipName"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						String shipName2 = Util.getStringValue(r.getAs("shipName_" + i));
						if (firstNotNull_shipName != null && shipName2 != null && !firstNotNull_shipName.equals(shipName2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipName': " + firstNotNull_shipName + " and " + shipName2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipName': " + firstNotNull_shipName + " and " + shipName2 + "." );
						}
						if (firstNotNull_shipName == null && shipName2 != null) {
							firstNotNull_shipName = shipName2;
						}
					}
					order_res.setShipName(firstNotNull_shipName);
					
					// attribute 'Order.shipAddress'
					String firstNotNull_shipAddress = Util.getStringValue(r.getAs("shipAddress"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						String shipAddress2 = Util.getStringValue(r.getAs("shipAddress_" + i));
						if (firstNotNull_shipAddress != null && shipAddress2 != null && !firstNotNull_shipAddress.equals(shipAddress2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipAddress': " + firstNotNull_shipAddress + " and " + shipAddress2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipAddress': " + firstNotNull_shipAddress + " and " + shipAddress2 + "." );
						}
						if (firstNotNull_shipAddress == null && shipAddress2 != null) {
							firstNotNull_shipAddress = shipAddress2;
						}
					}
					order_res.setShipAddress(firstNotNull_shipAddress);
					
					// attribute 'Order.shipCity'
					String firstNotNull_shipCity = Util.getStringValue(r.getAs("shipCity"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						String shipCity2 = Util.getStringValue(r.getAs("shipCity_" + i));
						if (firstNotNull_shipCity != null && shipCity2 != null && !firstNotNull_shipCity.equals(shipCity2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipCity': " + firstNotNull_shipCity + " and " + shipCity2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipCity': " + firstNotNull_shipCity + " and " + shipCity2 + "." );
						}
						if (firstNotNull_shipCity == null && shipCity2 != null) {
							firstNotNull_shipCity = shipCity2;
						}
					}
					order_res.setShipCity(firstNotNull_shipCity);
					
					// attribute 'Order.shipRegion'
					String firstNotNull_shipRegion = Util.getStringValue(r.getAs("shipRegion"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						String shipRegion2 = Util.getStringValue(r.getAs("shipRegion_" + i));
						if (firstNotNull_shipRegion != null && shipRegion2 != null && !firstNotNull_shipRegion.equals(shipRegion2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipRegion': " + firstNotNull_shipRegion + " and " + shipRegion2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipRegion': " + firstNotNull_shipRegion + " and " + shipRegion2 + "." );
						}
						if (firstNotNull_shipRegion == null && shipRegion2 != null) {
							firstNotNull_shipRegion = shipRegion2;
						}
					}
					order_res.setShipRegion(firstNotNull_shipRegion);
					
					// attribute 'Order.shipPostalCode'
					String firstNotNull_shipPostalCode = Util.getStringValue(r.getAs("shipPostalCode"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						String shipPostalCode2 = Util.getStringValue(r.getAs("shipPostalCode_" + i));
						if (firstNotNull_shipPostalCode != null && shipPostalCode2 != null && !firstNotNull_shipPostalCode.equals(shipPostalCode2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipPostalCode': " + firstNotNull_shipPostalCode + " and " + shipPostalCode2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipPostalCode': " + firstNotNull_shipPostalCode + " and " + shipPostalCode2 + "." );
						}
						if (firstNotNull_shipPostalCode == null && shipPostalCode2 != null) {
							firstNotNull_shipPostalCode = shipPostalCode2;
						}
					}
					order_res.setShipPostalCode(firstNotNull_shipPostalCode);
					
					// attribute 'Order.shipCountry'
					String firstNotNull_shipCountry = Util.getStringValue(r.getAs("shipCountry"));
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						String shipCountry2 = Util.getStringValue(r.getAs("shipCountry_" + i));
						if (firstNotNull_shipCountry != null && shipCountry2 != null && !firstNotNull_shipCountry.equals(shipCountry2)) {
							order_res.addLogEvent("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipCountry': " + firstNotNull_shipCountry + " and " + shipCountry2 + "." );
							logger.warn("Data consistency problem for [Order - id :"+order_res.getOrderID()+"]: different values found for attribute 'Order.shipCountry': " + firstNotNull_shipCountry + " and " + shipCountry2 + "." );
						}
						if (firstNotNull_shipCountry == null && shipCountry2 != null) {
							firstNotNull_shipCountry = shipCountry2;
						}
					}
					order_res.setShipCountry(firstNotNull_shipCountry);
	
					WrappedArray logEvents = r.getAs("logEvents");
					if(logEvents != null)
						for (int i = 0; i < logEvents.size(); i++){
							order_res.addLogEvent((String) logEvents.apply(i));
						}
		
					for (int i = 1; i < datasetsPOJO.size(); i++) {
						logEvents = r.getAs("logEvents_" + i);
						if(logEvents != null)
						for (int j = 0; j < logEvents.size(); j++){
							order_res.addLogEvent((String) logEvents.apply(j));
						}
					}
	
					return order_res;
				}, Encoders.bean(Order.class));
			return d;
	}
	
	
	public Dataset<Order> getOrderList(Order.bought role, Customer customer) {
		if(role != null) {
			if(role.equals(Order.bought.boughtOrder))
				return getBoughtOrderListInBoughtByCustomer(customer);
		}
		return null;
	}
	
	public Dataset<Order> getOrderList(Order.bought role, Condition<CustomerAttribute> condition) {
		if(role != null) {
			if(role.equals(Order.bought.boughtOrder))
				return getBoughtOrderListInBoughtByCustomerCondition(condition);
		}
		return null;
	}
	
	public Dataset<Order> getOrderList(Order.bought role, Condition<OrderAttribute> condition1, Condition<CustomerAttribute> condition2) {
		if(role != null) {
			if(role.equals(Order.bought.boughtOrder))
				return getBoughtOrderListInBought(condition1, condition2);
		}
		return null;
	}
	
	
	
	public Dataset<Order> getOrderList(Order.sold role, Employee employee) {
		if(role != null) {
			if(role.equals(Order.sold.order))
				return getOrderListInSoldByEmployee(employee);
		}
		return null;
	}
	
	public Dataset<Order> getOrderList(Order.sold role, Condition<EmployeeAttribute> condition) {
		if(role != null) {
			if(role.equals(Order.sold.order))
				return getOrderListInSoldByEmployeeCondition(condition);
		}
		return null;
	}
	
	public Dataset<Order> getOrderList(Order.sold role, Condition<OrderAttribute> condition1, Condition<EmployeeAttribute> condition2) {
		if(role != null) {
			if(role.equals(Order.sold.order))
				return getOrderListInSold(condition1, condition2);
		}
		return null;
	}
	
	
	
	
	
	
	public abstract Dataset<Order> getBoughtOrderListInBought(conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,conditions.Condition<conditions.CustomerAttribute> customer_condition);
	
	public Dataset<Order> getBoughtOrderListInBoughtByBoughtOrderCondition(conditions.Condition<conditions.OrderAttribute> boughtOrder_condition){
		return getBoughtOrderListInBought(boughtOrder_condition, null);
	}
	public Dataset<Order> getBoughtOrderListInBoughtByCustomerCondition(conditions.Condition<conditions.CustomerAttribute> customer_condition){
		return getBoughtOrderListInBought(null, customer_condition);
	}
	
	public Dataset<Order> getBoughtOrderListInBoughtByCustomer(pojo.Customer customer){
		if(customer == null)
			return null;
	
		Condition c;
		c=Condition.simple(CustomerAttribute.customerID,Operator.EQUALS, customer.getCustomerID());
		Dataset<Order> res = getBoughtOrderListInBoughtByCustomerCondition(c);
		return res;
	}
	
	public abstract Dataset<Order> getOrderListInSold(conditions.Condition<conditions.OrderAttribute> order_condition,conditions.Condition<conditions.EmployeeAttribute> employee_condition);
	
	public Dataset<Order> getOrderListInSoldByOrderCondition(conditions.Condition<conditions.OrderAttribute> order_condition){
		return getOrderListInSold(order_condition, null);
	}
	public Dataset<Order> getOrderListInSoldByEmployeeCondition(conditions.Condition<conditions.EmployeeAttribute> employee_condition){
		return getOrderListInSold(null, employee_condition);
	}
	
	public Dataset<Order> getOrderListInSoldByEmployee(pojo.Employee employee){
		if(employee == null)
			return null;
	
		Condition c;
		c=Condition.simple(EmployeeAttribute.employeeID,Operator.EQUALS, employee.getEmployeeID());
		Dataset<Order> res = getOrderListInSoldByEmployeeCondition(c);
		return res;
	}
	
	public abstract Dataset<Order> getOrderListInComposedOf(conditions.Condition<conditions.OrderAttribute> order_condition,conditions.Condition<conditions.ProductAttribute> product_condition, conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition);
	
	public Dataset<Order> getOrderListInComposedOfByOrderCondition(conditions.Condition<conditions.OrderAttribute> order_condition){
		return getOrderListInComposedOf(order_condition, null, null);
	}
	public Dataset<Order> getOrderListInComposedOfByProductCondition(conditions.Condition<conditions.ProductAttribute> product_condition){
		return getOrderListInComposedOf(null, product_condition, null);
	}
	
	public Dataset<Order> getOrderListInComposedOfByProduct(pojo.Product product){
		if(product == null)
			return null;
	
		Condition c;
		c=Condition.simple(ProductAttribute.productID,Operator.EQUALS, product.getProductID());
		Dataset<Order> res = getOrderListInComposedOfByProductCondition(c);
		return res;
	}
	
	public Dataset<Order> getOrderListInComposedOfByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition
	){
		return getOrderListInComposedOf(null, null, composedOf_condition);
	}
	
	public abstract boolean insertOrder(
		Order order,
		Customer	customerBought,
		Employee	employeeSold,
		 List<Product> productComposedOf,
		Double composedOf_unitPrice
	,
		Integer composedOf_quantity
	,
		Double composedOf_discount
	);
	
	public abstract boolean insertOrderInCustomersFromMyMongoDB(Order order,
		Customer	customerBought,
		Employee	employeeSold,
		 List<Product> productComposedOf,
		Double composedOf_unitPrice
	,
		Integer composedOf_quantity
	,
		Double composedOf_discount
	);
	public abstract boolean insertOrderInOrderShippingFromMyRedis(Order order,
		Customer	customerBought,
		Employee	employeeSold,
		 List<Product> productComposedOf,
		Double composedOf_unitPrice
	,
		Integer composedOf_quantity
	,
		Double composedOf_discount
	);
	public abstract boolean insertOrderInOrder_DetailsFromReldata(Order order,
		Customer	customerBought,
		Employee	employeeSold,
		 List<Product> productComposedOf,
		Double composedOf_unitPrice
	,
		Integer composedOf_quantity
	,
		Double composedOf_discount
	);
	private boolean inUpdateMethod = false;
	private List<Row> allOrderIdList = null;
	public abstract void updateOrderList(conditions.Condition<conditions.OrderAttribute> condition, conditions.SetClause<conditions.OrderAttribute> set);
	
	public void updateOrder(pojo.Order order) {
		//TODO using the id
		return;
	}
	public abstract void updateBoughtOrderListInBought(
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,
		conditions.Condition<conditions.CustomerAttribute> customer_condition,
		
		conditions.SetClause<conditions.OrderAttribute> set
	);
	
	public void updateBoughtOrderListInBoughtByBoughtOrderCondition(
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateBoughtOrderListInBought(boughtOrder_condition, null, set);
	}
	public void updateBoughtOrderListInBoughtByCustomerCondition(
		conditions.Condition<conditions.CustomerAttribute> customer_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateBoughtOrderListInBought(null, customer_condition, set);
	}
	
	public void updateBoughtOrderListInBoughtByCustomer(
		pojo.Customer customer,
		conditions.SetClause<conditions.OrderAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	public abstract void updateOrderListInSold(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.EmployeeAttribute> employee_condition,
		
		conditions.SetClause<conditions.OrderAttribute> set
	);
	
	public void updateOrderListInSoldByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInSold(order_condition, null, set);
	}
	public void updateOrderListInSoldByEmployeeCondition(
		conditions.Condition<conditions.EmployeeAttribute> employee_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInSold(null, employee_condition, set);
	}
	
	public void updateOrderListInSoldByEmployee(
		pojo.Employee employee,
		conditions.SetClause<conditions.OrderAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	public abstract void updateOrderListInComposedOf(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf,
		conditions.SetClause<conditions.OrderAttribute> set
	);
	
	public void updateOrderListInComposedOfByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInComposedOf(order_condition, null, null, set);
	}
	public void updateOrderListInComposedOfByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInComposedOf(null, product_condition, null, set);
	}
	
	public void updateOrderListInComposedOfByProduct(
		pojo.Product product,
		conditions.SetClause<conditions.OrderAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	public void updateOrderListInComposedOfByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition,
		conditions.SetClause<conditions.OrderAttribute> set
	){
		updateOrderListInComposedOf(null, null, composedOf_condition, set);
	}
	
	
	public abstract void deleteOrderList(conditions.Condition<conditions.OrderAttribute> condition);
	
	public void deleteOrder(pojo.Order order) {
		//TODO using the id
		return;
	}
	public abstract void deleteBoughtOrderListInBought(	
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,	
		conditions.Condition<conditions.CustomerAttribute> customer_condition);
	
	public void deleteBoughtOrderListInBoughtByBoughtOrderCondition(
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition
	){
		deleteBoughtOrderListInBought(boughtOrder_condition, null);
	}
	public void deleteBoughtOrderListInBoughtByCustomerCondition(
		conditions.Condition<conditions.CustomerAttribute> customer_condition
	){
		deleteBoughtOrderListInBought(null, customer_condition);
	}
	
	public void deleteBoughtOrderListInBoughtByCustomer(
		pojo.Customer customer 
	){
		//TODO get id in condition
		return;	
	}
	
	public abstract void deleteOrderListInSold(	
		conditions.Condition<conditions.OrderAttribute> order_condition,	
		conditions.Condition<conditions.EmployeeAttribute> employee_condition);
	
	public void deleteOrderListInSoldByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteOrderListInSold(order_condition, null);
	}
	public void deleteOrderListInSoldByEmployeeCondition(
		conditions.Condition<conditions.EmployeeAttribute> employee_condition
	){
		deleteOrderListInSold(null, employee_condition);
	}
	
	public void deleteOrderListInSoldByEmployee(
		pojo.Employee employee 
	){
		//TODO get id in condition
		return;	
	}
	
	public abstract void deleteOrderListInComposedOf(	
		conditions.Condition<conditions.OrderAttribute> order_condition,	
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf);
	
	public void deleteOrderListInComposedOfByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteOrderListInComposedOf(order_condition, null, null);
	}
	public void deleteOrderListInComposedOfByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition
	){
		deleteOrderListInComposedOf(null, product_condition, null);
	}
	
	public void deleteOrderListInComposedOfByProduct(
		pojo.Product product 
	){
		//TODO get id in condition
		return;	
	}
	
	public void deleteOrderListInComposedOfByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition
	){
		deleteOrderListInComposedOf(null, null, composedOf_condition);
	}
	
}
