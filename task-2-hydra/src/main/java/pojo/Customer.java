package pojo;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Customer extends LoggingPojo {

	private String address;
	private String city;
	private String companyName;
	private String contactName;
	private String contactTitle;
	private String country;
	private String customerID;
	private String fax;
	private String phone;
	private String postalCode;
	private String region;

	public enum bought {
		customer
	}
	private List<Order> boughtOrderList;

	// Empty constructor
	public Customer() {}

	// Constructor on Identifier
	public Customer(String customerID){
		this.customerID = customerID;
	}
	/*
	* Constructor on simple attribute 
	*/
	public Customer(String address,String city,String companyName,String contactName,String contactTitle,String country,String customerID,String fax,String phone,String postalCode,String region) {
		this.address = address;
		this.city = city;
		this.companyName = companyName;
		this.contactName = contactName;
		this.contactTitle = contactTitle;
		this.country = country;
		this.customerID = customerID;
		this.fax = fax;
		this.phone = phone;
		this.postalCode = postalCode;
		this.region = region;
	}
	@Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	@Override
	public boolean equals(Object o){
		if(this==o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Customer Customer = (Customer) o;
		boolean eqSimpleAttr = Objects.equals(address,Customer.address) && Objects.equals(city,Customer.city) && Objects.equals(companyName,Customer.companyName) && Objects.equals(contactName,Customer.contactName) && Objects.equals(contactTitle,Customer.contactTitle) && Objects.equals(country,Customer.country) && Objects.equals(customerID,Customer.customerID) && Objects.equals(fax,Customer.fax) && Objects.equals(phone,Customer.phone) && Objects.equals(postalCode,Customer.postalCode) && Objects.equals(region,Customer.region);
		boolean eqComplexAttr = false;
		eqComplexAttr = true && 
	Objects.equals(boughtOrderList, Customer.boughtOrderList) &&
 true;
		return eqSimpleAttr && eqComplexAttr;
	}
	
	@Override
	public String toString(){
		return "Customer { " + "address="+address +", "+
					"city="+city +", "+
					"companyName="+companyName +", "+
					"contactName="+contactName +", "+
					"contactTitle="+contactTitle +", "+
					"country="+country +", "+
					"customerID="+customerID +", "+
					"fax="+fax +", "+
					"phone="+phone +", "+
					"postalCode="+postalCode +", "+
					"region="+region +"}"; 
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTitle() {
		return contactTitle;
	}

	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	

	public List<Order> _getBoughtOrderList() {
		return boughtOrderList;
	}

	public void _setBoughtOrderList(List<Order> boughtOrderList) {
		this.boughtOrderList = boughtOrderList;
	}
}
