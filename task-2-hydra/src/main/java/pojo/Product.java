package pojo;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Product extends LoggingPojo {

	private Integer productID;
	private String productName;
	private Integer supplierRef;
	private String quantityPerUnit;
	private Double unitPrice;
	private Integer unitsInStock;
	private Integer unitsOnOrder;
	private Integer reorderLevel;
	private Boolean discontinued;

	private List<ComposedOf> composedOfListAsProduct;
	public enum supplies {
		product
	}
	private Supplier supplier;

	// Empty constructor
	public Product() {}

	// Constructor on Identifier
	public Product(Integer productID){
		this.productID = productID;
	}
	/*
	* Constructor on simple attribute 
	*/
	public Product(Integer productID,String productName,Integer supplierRef,String quantityPerUnit,Double unitPrice,Integer unitsInStock,Integer unitsOnOrder,Integer reorderLevel,Boolean discontinued) {
		this.productID = productID;
		this.productName = productName;
		this.supplierRef = supplierRef;
		this.quantityPerUnit = quantityPerUnit;
		this.unitPrice = unitPrice;
		this.unitsInStock = unitsInStock;
		this.unitsOnOrder = unitsOnOrder;
		this.reorderLevel = reorderLevel;
		this.discontinued = discontinued;
	}
	@Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	@Override
	public boolean equals(Object o){
		if(this==o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Product Product = (Product) o;
		boolean eqSimpleAttr = Objects.equals(productID,Product.productID) && Objects.equals(productName,Product.productName) && Objects.equals(supplierRef,Product.supplierRef) && Objects.equals(quantityPerUnit,Product.quantityPerUnit) && Objects.equals(unitPrice,Product.unitPrice) && Objects.equals(unitsInStock,Product.unitsInStock) && Objects.equals(unitsOnOrder,Product.unitsOnOrder) && Objects.equals(reorderLevel,Product.reorderLevel) && Objects.equals(discontinued,Product.discontinued);
		boolean eqComplexAttr = false;
		eqComplexAttr = true && 
	Objects.equals(composedOfListAsProduct,Product.composedOfListAsProduct) &&
	Objects.equals(supplier, Product.supplier) &&
 true;
		return eqSimpleAttr && eqComplexAttr;
	}
	
	@Override
	public String toString(){
		return "Product { " + "productID="+productID +", "+
					"productName="+productName +", "+
					"supplierRef="+supplierRef +", "+
					"quantityPerUnit="+quantityPerUnit +", "+
					"unitPrice="+unitPrice +", "+
					"unitsInStock="+unitsInStock +", "+
					"unitsOnOrder="+unitsOnOrder +", "+
					"reorderLevel="+reorderLevel +", "+
					"discontinued="+discontinued +"}"; 
	}
	
	public Integer getProductID() {
		return productID;
	}

	public void setProductID(Integer productID) {
		this.productID = productID;
	}
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getSupplierRef() {
		return supplierRef;
	}

	public void setSupplierRef(Integer supplierRef) {
		this.supplierRef = supplierRef;
	}
	public String getQuantityPerUnit() {
		return quantityPerUnit;
	}

	public void setQuantityPerUnit(String quantityPerUnit) {
		this.quantityPerUnit = quantityPerUnit;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Integer getUnitsInStock() {
		return unitsInStock;
	}

	public void setUnitsInStock(Integer unitsInStock) {
		this.unitsInStock = unitsInStock;
	}
	public Integer getUnitsOnOrder() {
		return unitsOnOrder;
	}

	public void setUnitsOnOrder(Integer unitsOnOrder) {
		this.unitsOnOrder = unitsOnOrder;
	}
	public Integer getReorderLevel() {
		return reorderLevel;
	}

	public void setReorderLevel(Integer reorderLevel) {
		this.reorderLevel = reorderLevel;
	}
	public Boolean getDiscontinued() {
		return discontinued;
	}

	public void setDiscontinued(Boolean discontinued) {
		this.discontinued = discontinued;
	}

	

	public java.util.List<ComposedOf> _getComposedOfListAsProduct() {
		return composedOfListAsProduct;
	}

	public void _setComposedOfListAsProduct(java.util.List<ComposedOf> composedOfListAsProduct) {
		this.composedOfListAsProduct = composedOfListAsProduct;
	}
	public Supplier _getSupplier() {
		return supplier;
	}

	public void _setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
}
