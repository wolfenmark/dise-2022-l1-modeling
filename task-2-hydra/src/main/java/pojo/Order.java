package pojo;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Order extends LoggingPojo {

	private Integer orderID;
	private LocalDate orderDate;
	private LocalDate requiredDate;
	private LocalDate shippedDate;
	private Double freight;
	private String shipName;
	private String shipAddress;
	private String shipCity;
	private String shipRegion;
	private String shipPostalCode;
	private String shipCountry;

	public enum bought {
		boughtOrder
	}
	private Customer customer;
	public enum sold {
		order
	}
	private Employee employee;
	private List<ComposedOf> composedOfListAsOrder;

	// Empty constructor
	public Order() {}

	// Constructor on Identifier
	public Order(Integer orderID){
		this.orderID = orderID;
	}
	/*
	* Constructor on simple attribute 
	*/
	public Order(Integer orderID,LocalDate orderDate,LocalDate requiredDate,LocalDate shippedDate,Double freight,String shipName,String shipAddress,String shipCity,String shipRegion,String shipPostalCode,String shipCountry) {
		this.orderID = orderID;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.freight = freight;
		this.shipName = shipName;
		this.shipAddress = shipAddress;
		this.shipCity = shipCity;
		this.shipRegion = shipRegion;
		this.shipPostalCode = shipPostalCode;
		this.shipCountry = shipCountry;
	}
	@Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	@Override
	public boolean equals(Object o){
		if(this==o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Order Order = (Order) o;
		boolean eqSimpleAttr = Objects.equals(orderID,Order.orderID) && Objects.equals(orderDate,Order.orderDate) && Objects.equals(requiredDate,Order.requiredDate) && Objects.equals(shippedDate,Order.shippedDate) && Objects.equals(freight,Order.freight) && Objects.equals(shipName,Order.shipName) && Objects.equals(shipAddress,Order.shipAddress) && Objects.equals(shipCity,Order.shipCity) && Objects.equals(shipRegion,Order.shipRegion) && Objects.equals(shipPostalCode,Order.shipPostalCode) && Objects.equals(shipCountry,Order.shipCountry);
		boolean eqComplexAttr = false;
		eqComplexAttr = true && 
	Objects.equals(customer, Order.customer) &&
	Objects.equals(employee, Order.employee) &&
	Objects.equals(composedOfListAsOrder,Order.composedOfListAsOrder) &&
 true;
		return eqSimpleAttr && eqComplexAttr;
	}
	
	@Override
	public String toString(){
		return "Order { " + "orderID="+orderID +", "+
					"orderDate="+orderDate +", "+
					"requiredDate="+requiredDate +", "+
					"shippedDate="+shippedDate +", "+
					"freight="+freight +", "+
					"shipName="+shipName +", "+
					"shipAddress="+shipAddress +", "+
					"shipCity="+shipCity +", "+
					"shipRegion="+shipRegion +", "+
					"shipPostalCode="+shipPostalCode +", "+
					"shipCountry="+shipCountry +"}"; 
	}
	
	public Integer getOrderID() {
		return orderID;
	}

	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public LocalDate getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(LocalDate requiredDate) {
		this.requiredDate = requiredDate;
	}
	public LocalDate getShippedDate() {
		return shippedDate;
	}

	public void setShippedDate(LocalDate shippedDate) {
		this.shippedDate = shippedDate;
	}
	public Double getFreight() {
		return freight;
	}

	public void setFreight(Double freight) {
		this.freight = freight;
	}
	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}
	public String getShipAddress() {
		return shipAddress;
	}

	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}
	public String getShipCity() {
		return shipCity;
	}

	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}
	public String getShipRegion() {
		return shipRegion;
	}

	public void setShipRegion(String shipRegion) {
		this.shipRegion = shipRegion;
	}
	public String getShipPostalCode() {
		return shipPostalCode;
	}

	public void setShipPostalCode(String shipPostalCode) {
		this.shipPostalCode = shipPostalCode;
	}
	public String getShipCountry() {
		return shipCountry;
	}

	public void setShipCountry(String shipCountry) {
		this.shipCountry = shipCountry;
	}

	

	public Customer _getCustomer() {
		return customer;
	}

	public void _setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Employee _getEmployee() {
		return employee;
	}

	public void _setEmployee(Employee employee) {
		this.employee = employee;
	}
	public java.util.List<ComposedOf> _getComposedOfListAsOrder() {
		return composedOfListAsOrder;
	}

	public void _setComposedOfListAsOrder(java.util.List<ComposedOf> composedOfListAsOrder) {
		this.composedOfListAsOrder = composedOfListAsOrder;
	}
}
