package pojo;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Bought extends LoggingPojo {

	private Order boughtOrder;	
	private Customer customer;	

	//Empty constructor
	public Bought() {}
	
	//Role constructor
	public Bought(Order boughtOrder,Customer customer){
		this.boughtOrder=boughtOrder;
		this.customer=customer;
	}
	

	@Override
    public Object clone() throws CloneNotSupportedException {
        Bought cloned = (Bought) super.clone();
		cloned.setBoughtOrder((Order)cloned.getBoughtOrder().clone());	
		cloned.setCustomer((Customer)cloned.getCustomer().clone());	
		return cloned;
    }

@Override
	public String toString(){
		return "bought : "+
				"roles : {" +  "boughtOrder:Order={"+boughtOrder+"},"+ 
					 "customer:Customer={"+customer+"}"+
				 "}"+
				"";
	}
	public Order getBoughtOrder() {
		return boughtOrder;
	}	

	public void setBoughtOrder(Order boughtOrder) {
		this.boughtOrder = boughtOrder;
	}
	
	public Customer getCustomer() {
		return customer;
	}	

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	

}
