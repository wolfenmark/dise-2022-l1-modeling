import dao.services.SupplierService;
import dao.services.ComposedOfService;
import dao.services.CustomerService;
import dao.services.EmployeeService;
import dao.services.OrderService;

import conditions.ProductAttribute;
import conditions.EmployeeAttribute;
import pojo.Product;
import pojo.Supplier;
import pojo.Employee;
import pojo.Order;
import pojo.ComposedOf;
import pojo.Customer;
import util.Dataset;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import conditions.Condition;
import conditions.Operator;
import dao.impl.SupplierServiceImpl;
import dao.impl.ComposedOfServiceImpl;
import dao.impl.CustomerServiceImpl;
import dao.impl.EmployeeServiceImpl;
import dao.impl.OrderServiceImpl;

/**
 * HyDRA supported implementation of the required searches.
 */

/**
 * @author wolfenmark and tabholt
 *
 */
public class Main {

	/**
	 * Returns the supplier of a product given its product name
	 * @param productName the name of the product whose supplier should be returned
	 * @return the supplier of product 'productName'
	 */
	public static Supplier getSupplierByProductName(String productName) {
		Condition<ProductAttribute> productNameCondition = Condition.simple(ProductAttribute.productName, Operator.EQUALS, productName);
		SupplierService supplierService = new SupplierServiceImpl();
		Dataset<Supplier> suppliers = supplierService.getSupplierListInSuppliesByProductCondition(productNameCondition);
		assert(suppliers.count() == 1);
		return suppliers.first();
	}

	/**
	 * Return a set of customers whose orders have been encoded by an employee whose first name is 'employeeFirstName'.
	 * @param employeeFirstName the name of the employee who encoded the order
	 * @return a set of customers
	 * @warning Due to current limitations in HyDRA generated implementation, the set is artificially obtained and manipulating it adding customer objects does not guarantee their uniqueness anymore
	 */
	public static Set<Customer> getCustomersByEncodingEmployeeFirstName(String employeeFirstName) {
		OrderService orderService = new OrderServiceImpl();
		EmployeeService employeeService = new EmployeeServiceImpl();
		CustomerService customerService = new CustomerServiceImpl();
		
		Condition<EmployeeAttribute> nameMargaret = Condition.simple(EmployeeAttribute.firstName, Operator.EQUALS, employeeFirstName);
		Dataset<Employee> margarets = employeeService.getEmployeeList(nameMargaret);
		// FIXME Should use Sets when HyDRA generated code supports hash for hashed collections.
		// Temporary workaround, remove duplicates, since equals is already correctly implemented
//		Set<Customer> customers = new HashSet<Customer>();
		Map<String, Customer> customers = new HashMap<String, Customer>();
		margarets.collectAsList().forEach(m -> {
			Dataset<Order> ordersByMargaret = orderService.getOrderListInSoldByEmployee(m);
			ordersByMargaret.collectAsList().forEach(order -> {
				// For documentation purposes, we tried to access this directly for each order by getting the customer but it was uninitialized (null)
//				Customer customer = order._getCustomer();
//				customers.add(customer);
				
				// FIXME maybe its more efficient to retrieve all order ids and get all customers from the bought order with that id all at once
				Customer c = customerService.getCustomerInBoughtByBoughtOrder(order);
				String id = c.getCustomerID();
				if (!customers.containsKey(id)) {
					customers.put(id, c);
				}
				else {
					assert(customers.get(id).equals(c));
				}
				});
			}
		);
		return new HashSet<Customer>(customers.values());
	}
	
	/**
	 * Return a map of products and their additional details in the order with id 'orderID'.
	 * @param orderID the id of the order of interest
	 * @return a map of products and their additional details
	 */
	public static Map<Product, Map<String, Object>> getDetailedProductsByOrderID(Integer orderID) {
		OrderService orderService = new OrderServiceImpl();
		Order orderWanted = orderService.getOrderById(orderID);
		ComposedOfService composedOfService = new ComposedOfServiceImpl();
		Dataset<ComposedOf> composedOfDataset = composedOfService.getComposedOfListByOrder(orderWanted);
		Map<Product, Map<String, Object>> detailedProducts = new HashMap<Product, Map<String, Object>>();
		composedOfDataset.collectAsList().forEach(composedOf -> {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put("Quantity", composedOf.getQuantity());
			details.put("Discount", composedOf.getDiscount());
			details.put("UnitPrice", composedOf.getUnitPrice());
			detailedProducts.put(composedOf.getProduct(), details);
		});

		return detailedProducts;
	}

	
	/** HyDRA assisted extraction of information from the polystore
	 * @param args
	 */
	public static void main(String[] args) {
		// Returns a Supplier object containing all information about the supplier of the
		// product with the name “Escargots de Bourgogne”
		Supplier supplier = getSupplierByProductName("Escargots de Bourgogne");
		System.out.println("\nThe supplier of Escargots de Bourgogne is:");
		System.out.println(supplier.toString() + "\n");
		
		
		// Returns a List of Customers objects containing all Customers which have
		// made an Order encoded by Employee with firstname “Margaret”
		Set<Customer> customers = getCustomersByEncodingEmployeeFirstName("Margaret");
		System.out.println("\nThe customers for whom at least one order has been encoded by Margaret are:");
		customers.forEach(c -> {
			System.out.println(c.toString());
		});
		System.out.println("");
		
		
		// Get all detailed Products of Order with ID 10266
		Map<Product, Map<String, Object>> productDetails = getDetailedProductsByOrderID(10266);
		System.out.println("\nThe products (with details) of the order with ID 10266 are:");
		productDetails.forEach((product, productAttributes) -> {
			System.out.println(product.toString());
			System.out.println("Attributes: { Quantity: " + productAttributes.get("Quantity") + ", " +
					"Unit Price: " + productAttributes.get("UnitPrice") + ", " +
					"Discount: " + productAttributes.get("Discount") + "}");
			}
		);
		System.out.println("");
		
	}

}
