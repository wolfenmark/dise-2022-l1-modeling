	package tdo;

	import pojo.ComposedOf;
	
	public class ComposedOfTDO extends ComposedOf {
	
	private String reldata_Order_Details_orderRef_source_OrderRef;
	public String getReldata_Order_Details_orderRef_source_OrderRef() {
		return this.reldata_Order_Details_orderRef_source_OrderRef;
	}

	public void setReldata_Order_Details_orderRef_source_OrderRef( String reldata_Order_Details_orderRef_source_OrderRef) {
		this.reldata_Order_Details_orderRef_source_OrderRef = reldata_Order_Details_orderRef_source_OrderRef;
	}

	private String reldata_Order_Details_orderRef_target_orderid;
	public String getReldata_Order_Details_orderRef_target_orderid() {
		return this.reldata_Order_Details_orderRef_target_orderid;
	}

	public void setReldata_Order_Details_orderRef_target_orderid( String reldata_Order_Details_orderRef_target_orderid) {
		this.reldata_Order_Details_orderRef_target_orderid = reldata_Order_Details_orderRef_target_orderid;
	}

	private String reldata_Order_Details_productRef_source_ProductRef;
	public String getReldata_Order_Details_productRef_source_ProductRef() {
		return this.reldata_Order_Details_productRef_source_ProductRef;
	}

	public void setReldata_Order_Details_productRef_source_ProductRef( String reldata_Order_Details_productRef_source_ProductRef) {
		this.reldata_Order_Details_productRef_source_ProductRef = reldata_Order_Details_productRef_source_ProductRef;
	}

	private String reldata_Order_Details_productRef_target_ProductID;
	public String getReldata_Order_Details_productRef_target_ProductID() {
		return this.reldata_Order_Details_productRef_target_ProductID;
	}

	public void setReldata_Order_Details_productRef_target_ProductID( String reldata_Order_Details_productRef_target_ProductID) {
		this.reldata_Order_Details_productRef_target_ProductID = reldata_Order_Details_productRef_target_ProductID;
	}

	
	}
