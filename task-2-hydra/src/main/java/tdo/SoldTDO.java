	package tdo;

	import pojo.Sold;
	
	public class SoldTDO extends Sold {
	
	private String myRedis_orderShipping_employeeRef_source_EmployeeRef;
	public String getMyRedis_orderShipping_employeeRef_source_EmployeeRef() {
		return this.myRedis_orderShipping_employeeRef_source_EmployeeRef;
	}

	public void setMyRedis_orderShipping_employeeRef_source_EmployeeRef( String myRedis_orderShipping_employeeRef_source_EmployeeRef) {
		this.myRedis_orderShipping_employeeRef_source_EmployeeRef = myRedis_orderShipping_employeeRef_source_EmployeeRef;
	}

	private String myRedis_orderShipping_employeeRef_target_EmployeeID;
	public String getMyRedis_orderShipping_employeeRef_target_EmployeeID() {
		return this.myRedis_orderShipping_employeeRef_target_EmployeeID;
	}

	public void setMyRedis_orderShipping_employeeRef_target_EmployeeID( String myRedis_orderShipping_employeeRef_target_EmployeeID) {
		this.myRedis_orderShipping_employeeRef_target_EmployeeID = myRedis_orderShipping_employeeRef_target_EmployeeID;
	}

	
	}
