package tdo;

import pojo.Supplier;
import java.util.List;
import java.util.ArrayList;

public class SupplierTDO extends Supplier {
	private  String reldata_Products_supplierRef_target_SupplierID;
	public  String getReldata_Products_supplierRef_target_SupplierID() {
		return this.reldata_Products_supplierRef_target_SupplierID;
	}

	public void setReldata_Products_supplierRef_target_SupplierID(  String reldata_Products_supplierRef_target_SupplierID) {
		this.reldata_Products_supplierRef_target_SupplierID = reldata_Products_supplierRef_target_SupplierID;
	}

}
