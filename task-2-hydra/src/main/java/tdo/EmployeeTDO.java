package tdo;

import pojo.Employee;
import java.util.List;
import java.util.ArrayList;

public class EmployeeTDO extends Employee {
	private  String myRedis_orderShipping_employeeRef_target_EmployeeID;
	public  String getMyRedis_orderShipping_employeeRef_target_EmployeeID() {
		return this.myRedis_orderShipping_employeeRef_target_EmployeeID;
	}

	public void setMyRedis_orderShipping_employeeRef_target_EmployeeID(  String myRedis_orderShipping_employeeRef_target_EmployeeID) {
		this.myRedis_orderShipping_employeeRef_target_EmployeeID = myRedis_orderShipping_employeeRef_target_EmployeeID;
	}

}
