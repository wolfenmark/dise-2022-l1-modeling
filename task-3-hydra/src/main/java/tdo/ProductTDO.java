package tdo;

import pojo.Product;
import java.util.List;
import java.util.ArrayList;

public class ProductTDO extends Product {
	private  String reldata_Products_supplierRef_source_SupplierRef; 
	public  String getReldata_Products_supplierRef_source_SupplierRef() {
		return this.reldata_Products_supplierRef_source_SupplierRef;
	}

	public void setReldata_Products_supplierRef_source_SupplierRef(  String reldata_Products_supplierRef_source_SupplierRef) {
		this.reldata_Products_supplierRef_source_SupplierRef = reldata_Products_supplierRef_source_SupplierRef;
	}

}
