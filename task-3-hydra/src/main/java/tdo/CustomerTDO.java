package tdo;

import pojo.Customer;
import java.util.List;
import java.util.ArrayList;

public class CustomerTDO extends Customer {
	private  String myRedis_orderShipping_customerRef_target_CustomerID;
	public  String getMyRedis_orderShipping_customerRef_target_CustomerID() {
		return this.myRedis_orderShipping_customerRef_target_CustomerID;
	}

	public void setMyRedis_orderShipping_customerRef_target_CustomerID(  String myRedis_orderShipping_customerRef_target_CustomerID) {
		this.myRedis_orderShipping_customerRef_target_CustomerID = myRedis_orderShipping_customerRef_target_CustomerID;
	}

}
