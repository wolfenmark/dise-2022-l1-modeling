	package tdo;

	import pojo.Bought;
	
	public class BoughtTDO extends Bought {
	
	private String myRedis_orderShipping_customerRef_source_CustomerRef;
	public String getMyRedis_orderShipping_customerRef_source_CustomerRef() {
		return this.myRedis_orderShipping_customerRef_source_CustomerRef;
	}

	public void setMyRedis_orderShipping_customerRef_source_CustomerRef( String myRedis_orderShipping_customerRef_source_CustomerRef) {
		this.myRedis_orderShipping_customerRef_source_CustomerRef = myRedis_orderShipping_customerRef_source_CustomerRef;
	}

	private String myRedis_orderShipping_customerRef_target_CustomerID;
	public String getMyRedis_orderShipping_customerRef_target_CustomerID() {
		return this.myRedis_orderShipping_customerRef_target_CustomerID;
	}

	public void setMyRedis_orderShipping_customerRef_target_CustomerID( String myRedis_orderShipping_customerRef_target_CustomerID) {
		this.myRedis_orderShipping_customerRef_target_CustomerID = myRedis_orderShipping_customerRef_target_CustomerID;
	}

	
	}
