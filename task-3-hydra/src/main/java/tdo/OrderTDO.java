package tdo;

import pojo.Order;
import java.util.List;
import java.util.ArrayList;

public class OrderTDO extends Order {
	private  String myRedis_orderShipping_customerRef_source_CustomerRef; 
	public  String getMyRedis_orderShipping_customerRef_source_CustomerRef() {
		return this.myRedis_orderShipping_customerRef_source_CustomerRef;
	}

	public void setMyRedis_orderShipping_customerRef_source_CustomerRef(  String myRedis_orderShipping_customerRef_source_CustomerRef) {
		this.myRedis_orderShipping_customerRef_source_CustomerRef = myRedis_orderShipping_customerRef_source_CustomerRef;
	}

	private  String myRedis_orderShipping_employeeRef_source_EmployeeRef; 
	public  String getMyRedis_orderShipping_employeeRef_source_EmployeeRef() {
		return this.myRedis_orderShipping_employeeRef_source_EmployeeRef;
	}

	public void setMyRedis_orderShipping_employeeRef_source_EmployeeRef(  String myRedis_orderShipping_employeeRef_source_EmployeeRef) {
		this.myRedis_orderShipping_employeeRef_source_EmployeeRef = myRedis_orderShipping_employeeRef_source_EmployeeRef;
	}

}
