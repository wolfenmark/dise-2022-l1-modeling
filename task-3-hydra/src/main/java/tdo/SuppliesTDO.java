	package tdo;

	import pojo.Supplies;
	
	public class SuppliesTDO extends Supplies {
	
	private String reldata_Products_supplierRef_source_SupplierRef;
	public String getReldata_Products_supplierRef_source_SupplierRef() {
		return this.reldata_Products_supplierRef_source_SupplierRef;
	}

	public void setReldata_Products_supplierRef_source_SupplierRef( String reldata_Products_supplierRef_source_SupplierRef) {
		this.reldata_Products_supplierRef_source_SupplierRef = reldata_Products_supplierRef_source_SupplierRef;
	}

	private String reldata_Products_supplierRef_target_SupplierID;
	public String getReldata_Products_supplierRef_target_SupplierID() {
		return this.reldata_Products_supplierRef_target_SupplierID;
	}

	public void setReldata_Products_supplierRef_target_SupplierID( String reldata_Products_supplierRef_target_SupplierID) {
		this.reldata_Products_supplierRef_target_SupplierID = reldata_Products_supplierRef_target_SupplierID;
	}

	
	}
