package pojo;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Sold extends LoggingPojo {

	private Order order;	
	private Employee employee;	

	//Empty constructor
	public Sold() {}
	
	//Role constructor
	public Sold(Order order,Employee employee){
		this.order=order;
		this.employee=employee;
	}
	

	@Override
    public Object clone() throws CloneNotSupportedException {
        Sold cloned = (Sold) super.clone();
		cloned.setOrder((Order)cloned.getOrder().clone());	
		cloned.setEmployee((Employee)cloned.getEmployee().clone());	
		return cloned;
    }

@Override
	public String toString(){
		return "sold : "+
				"roles : {" +  "order:Order={"+order+"},"+ 
					 "employee:Employee={"+employee+"}"+
				 "}"+
				"";
	}
	public Order getOrder() {
		return order;
	}	

	public void setOrder(Order order) {
		this.order = order;
	}
	
	public Employee getEmployee() {
		return employee;
	}	

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	

}
