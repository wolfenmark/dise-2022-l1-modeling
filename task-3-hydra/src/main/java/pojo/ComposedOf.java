package pojo;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ComposedOf extends LoggingPojo {

	private Order order;	
	private Product product;	
	private Double unitPrice;	
	private Integer quantity;	
	private Double discount;	

	//Empty constructor
	public ComposedOf() {}
	
	//Role constructor
	public ComposedOf(Order order,Product product){
		this.order=order;
		this.product=product;
	}
	

	@Override
    public Object clone() throws CloneNotSupportedException {
        ComposedOf cloned = (ComposedOf) super.clone();
		cloned.setOrder((Order)cloned.getOrder().clone());	
		cloned.setProduct((Product)cloned.getProduct().clone());	
		return cloned;
    }

@Override
	public String toString(){
		return "composedOf : "+
				"roles : {" +  "order:Order={"+order+"},"+ 
					 "product:Product={"+product+"}"+
				 "}"+
			" attributes : { " + "unitPrice="+unitPrice +", "+
					"quantity="+quantity +", "+
					"discount="+discount +"}"; 
	}
	public Order getOrder() {
		return order;
	}	

	public void setOrder(Order order) {
		this.order = order;
	}
	
	public Product getProduct() {
		return product;
	}	

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	

}
