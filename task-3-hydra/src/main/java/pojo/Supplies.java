package pojo;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Supplies extends LoggingPojo {

	private Product product;	
	private Supplier supplier;	

	//Empty constructor
	public Supplies() {}
	
	//Role constructor
	public Supplies(Product product,Supplier supplier){
		this.product=product;
		this.supplier=supplier;
	}
	

	@Override
    public Object clone() throws CloneNotSupportedException {
        Supplies cloned = (Supplies) super.clone();
		cloned.setProduct((Product)cloned.getProduct().clone());	
		cloned.setSupplier((Supplier)cloned.getSupplier().clone());	
		return cloned;
    }

@Override
	public String toString(){
		return "supplies : "+
				"roles : {" +  "product:Product={"+product+"},"+ 
					 "supplier:Supplier={"+supplier+"}"+
				 "}"+
				"";
	}
	public Product getProduct() {
		return product;
	}	

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Supplier getSupplier() {
		return supplier;
	}	

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	

}
