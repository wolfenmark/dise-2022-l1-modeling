package conditions;

public enum OrderAttribute implements Attributes{
	orderID, orderDate, requiredDate, shippedDate, freight, shipName, shipAddress, shipCity, shipRegion, shipPostalCode, shipCountry
}
