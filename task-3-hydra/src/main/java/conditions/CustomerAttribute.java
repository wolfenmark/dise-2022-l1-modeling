package conditions;

public enum CustomerAttribute implements Attributes{
	address, city, companyName, contactName, contactTitle, country, customerID, fax, phone, postalCode, region
}
