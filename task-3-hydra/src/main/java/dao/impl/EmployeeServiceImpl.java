package dao.impl;
import exceptions.PhysicalStructureException;
import java.util.Arrays;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import pojo.Employee;
import conditions.*;
import dao.services.EmployeeService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import util.Dataset;
import org.apache.spark.sql.Encoders;
import util.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.*;
import org.apache.spark.api.java.function.MapFunction;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.api.java.JavaSparkContext;
import com.mongodb.spark.MongoSpark;
import org.bson.Document;
import static java.util.Collections.singletonList;
import dbconnection.SparkConnectionMgr;
import dbconnection.DBConnectionMgr;
import util.WrappedArray;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.FilterFunction;
import java.util.ArrayList;
import org.apache.commons.lang3.mutable.MutableBoolean;
import tdo.*;
import pojo.*;
import util.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.ArrayType;
import scala.Tuple2;
import org.bson.Document;
import org.bson.conversions.Bson;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;


public class EmployeeServiceImpl extends EmployeeService {
	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(EmployeeServiceImpl.class);
	
	
	
	
	public static Pair<String, List<String>> getSQLWhereClauseInEmployeesFromReldata(Condition<EmployeeAttribute> condition, MutableBoolean refilterFlag) {
		return getSQLWhereClauseInEmployeesFromReldataWithTableAlias(condition, refilterFlag, "");
	}
	
	public static List<String> getSQLSetClauseInEmployeesFromReldata(conditions.SetClause<EmployeeAttribute> set) {
		List<String> res = new ArrayList<String>();
		if(set != null) {
			java.util.Map<String, java.util.Map<String, String>> longFieldValues = new java.util.HashMap<String, java.util.Map<String, String>>();
			java.util.Map<EmployeeAttribute, Object> clause = set.getClause();
			for(java.util.Map.Entry<EmployeeAttribute, Object> e : clause.entrySet()) {
				EmployeeAttribute attr = e.getKey();
				Object value = e.getValue();
				if(attr == EmployeeAttribute.employeeID ) {
					res.add("EmployeeID = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.lastName ) {
					res.add("LastName = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.firstName ) {
					res.add("FirstName = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.title ) {
					res.add("Title = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.titleOfCourtesy ) {
					res.add("TitleOfCourtesy = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.birthDate ) {
					res.add("BirthDate = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.hireDate ) {
					res.add("HireDate = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.address ) {
					res.add("Address = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.city ) {
					res.add("City = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.region ) {
					res.add("Region = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.postalCode ) {
					res.add("PostalCode = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.country ) {
					res.add("Country = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.homePhone ) {
					res.add("HomePhone = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.extension ) {
					res.add("Extension = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.photo ) {
					res.add("Photo = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.notes ) {
					res.add("Notes = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.photoPath ) {
					res.add("PhotoPath = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
				if(attr == EmployeeAttribute.salary ) {
					res.add("Salary = " + Util.getDelimitedSQLValue((value == null ? null : value.getClass()), (value == null ? null : value.toString())));
				}
			}
	
			for(java.util.Map.Entry<String, java.util.Map<String, String>> entry : longFieldValues.entrySet()) {
				String longField = entry.getKey();
				java.util.Map<String, String> values = entry.getValue();
			}
	
		}
		return res;
	}
	
	public static Pair<String, List<String>> getSQLWhereClauseInEmployeesFromReldataWithTableAlias(Condition<EmployeeAttribute> condition, MutableBoolean refilterFlag, String tableAlias) {
		String where = null;	
		List<String> preparedValues = new java.util.ArrayList<String>();
		if(condition != null) {
			
			if(condition instanceof SimpleCondition) {
				EmployeeAttribute attr = ((SimpleCondition<EmployeeAttribute>) condition).getAttribute();
				Operator op = ((SimpleCondition<EmployeeAttribute>) condition).getOperator();
				Object value = ((SimpleCondition<EmployeeAttribute>) condition).getValue();
				if(value != null) {
					boolean isConditionAttrEncountered = false;
					if(attr == EmployeeAttribute.employeeID ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "EmployeeID " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.lastName ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "LastName " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.firstName ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "FirstName " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.title ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Title " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.titleOfCourtesy ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "TitleOfCourtesy " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.birthDate ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "BirthDate " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.hireDate ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "HireDate " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.address ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Address " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.city ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "City " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.region ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Region " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.postalCode ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "PostalCode " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.country ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Country " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.homePhone ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "HomePhone " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.extension ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Extension " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.photo ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Photo " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.notes ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Notes " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.photoPath ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "PhotoPath " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(attr == EmployeeAttribute.salary ) {
						isConditionAttrEncountered = true;
						String valueString = Util.transformSQLValue(value);
						String sqlOp = op.getSQLOperator();
						Class cl = null;
						String preparedValue = valueString;
						if(op == Operator.CONTAINS && valueString != null) {
							preparedValue = "%" + Util.escapeReservedCharSQL(valueString)  + "%";
							cl = String.class;
						} else
							cl = value.getClass();
						
						where = tableAlias + "Salary " + sqlOp + " ?";
	
						preparedValue = Util.getDelimitedSQLValue(cl, preparedValue);
						preparedValues.add(preparedValue);
					}
					if(!isConditionAttrEncountered) {
						refilterFlag.setValue(true);
						where = "1 = 1";
					}
				} else {
					if(attr == EmployeeAttribute.employeeID ) {
						if(op == Operator.EQUALS)
							where =  "EmployeeID IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "EmployeeID IS NOT NULL";
					}
					if(attr == EmployeeAttribute.lastName ) {
						if(op == Operator.EQUALS)
							where =  "LastName IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "LastName IS NOT NULL";
					}
					if(attr == EmployeeAttribute.firstName ) {
						if(op == Operator.EQUALS)
							where =  "FirstName IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "FirstName IS NOT NULL";
					}
					if(attr == EmployeeAttribute.title ) {
						if(op == Operator.EQUALS)
							where =  "Title IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Title IS NOT NULL";
					}
					if(attr == EmployeeAttribute.titleOfCourtesy ) {
						if(op == Operator.EQUALS)
							where =  "TitleOfCourtesy IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "TitleOfCourtesy IS NOT NULL";
					}
					if(attr == EmployeeAttribute.birthDate ) {
						if(op == Operator.EQUALS)
							where =  "BirthDate IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "BirthDate IS NOT NULL";
					}
					if(attr == EmployeeAttribute.hireDate ) {
						if(op == Operator.EQUALS)
							where =  "HireDate IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "HireDate IS NOT NULL";
					}
					if(attr == EmployeeAttribute.address ) {
						if(op == Operator.EQUALS)
							where =  "Address IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Address IS NOT NULL";
					}
					if(attr == EmployeeAttribute.city ) {
						if(op == Operator.EQUALS)
							where =  "City IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "City IS NOT NULL";
					}
					if(attr == EmployeeAttribute.region ) {
						if(op == Operator.EQUALS)
							where =  "Region IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Region IS NOT NULL";
					}
					if(attr == EmployeeAttribute.postalCode ) {
						if(op == Operator.EQUALS)
							where =  "PostalCode IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "PostalCode IS NOT NULL";
					}
					if(attr == EmployeeAttribute.country ) {
						if(op == Operator.EQUALS)
							where =  "Country IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Country IS NOT NULL";
					}
					if(attr == EmployeeAttribute.homePhone ) {
						if(op == Operator.EQUALS)
							where =  "HomePhone IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "HomePhone IS NOT NULL";
					}
					if(attr == EmployeeAttribute.extension ) {
						if(op == Operator.EQUALS)
							where =  "Extension IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Extension IS NOT NULL";
					}
					if(attr == EmployeeAttribute.photo ) {
						if(op == Operator.EQUALS)
							where =  "Photo IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Photo IS NOT NULL";
					}
					if(attr == EmployeeAttribute.notes ) {
						if(op == Operator.EQUALS)
							where =  "Notes IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Notes IS NOT NULL";
					}
					if(attr == EmployeeAttribute.photoPath ) {
						if(op == Operator.EQUALS)
							where =  "PhotoPath IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "PhotoPath IS NOT NULL";
					}
					if(attr == EmployeeAttribute.salary ) {
						if(op == Operator.EQUALS)
							where =  "Salary IS NULL";
						if(op == Operator.NOT_EQUALS)
							where =  "Salary IS NOT NULL";
					}
				}
			}
	
			if(condition instanceof AndCondition) {
				Pair<String, List<String>> pairLeft = getSQLWhereClauseInEmployeesFromReldata(((AndCondition) condition).getLeftCondition(), refilterFlag);
				Pair<String, List<String>> pairRight = getSQLWhereClauseInEmployeesFromReldata(((AndCondition) condition).getRightCondition(), refilterFlag);
				String whereLeft = pairLeft.getKey();
				String whereRight = pairRight.getKey();
				List<String> leftValues = pairLeft.getValue();
				List<String> rightValues = pairRight.getValue();
				if(whereLeft != null || whereRight != null) {
					if(whereLeft == null)
						where = whereRight;
					else
						if(whereRight == null)
							where = whereLeft;
						else
							where = "(" + whereLeft + " AND " + whereRight + ")";
					preparedValues.addAll(leftValues);
					preparedValues.addAll(rightValues);
				}
			}
	
			if(condition instanceof OrCondition) {
				Pair<String, List<String>> pairLeft = getSQLWhereClauseInEmployeesFromReldata(((OrCondition) condition).getLeftCondition(), refilterFlag);
				Pair<String, List<String>> pairRight = getSQLWhereClauseInEmployeesFromReldata(((OrCondition) condition).getRightCondition(), refilterFlag);
				String whereLeft = pairLeft.getKey();
				String whereRight = pairRight.getKey();
				List<String> leftValues = pairLeft.getValue();
				List<String> rightValues = pairRight.getValue();
				if(whereLeft != null || whereRight != null) {
					if(whereLeft == null)
						where = whereRight;
					else
						if(whereRight == null)
							where = whereLeft;
						else
							where = "(" + whereLeft + " OR " + whereRight + ")";
					preparedValues.addAll(leftValues);
					preparedValues.addAll(rightValues);
				}
			}
	
		}
	
		return new ImmutablePair<String, List<String>>(where, preparedValues);
	}
	
	
	
	public Dataset<Employee> getEmployeeListInEmployeesFromReldata(conditions.Condition<conditions.EmployeeAttribute> condition, MutableBoolean refilterFlag){
	
		Pair<String, List<String>> whereClause = EmployeeServiceImpl.getSQLWhereClauseInEmployeesFromReldata(condition, refilterFlag);
		String where = whereClause.getKey();
		List<String> preparedValues = whereClause.getValue();
		for(String preparedValue : preparedValues) {
			where = where.replaceFirst("\\?", preparedValue);
		}
		
		Dataset<Row> d = dbconnection.SparkConnectionMgr.getDataset("reldata", "Employees", where);
		
	
		Dataset<Employee> res = d.map((MapFunction<Row, Employee>) r -> {
					Employee employee_res = new Employee();
					Integer groupIndex = null;
					String regex = null;
					String value = null;
					Pattern p = null;
					Matcher m = null;
					boolean matches = false;
					
					// attribute [Employee.EmployeeID]
					Integer employeeID = Util.getIntegerValue(r.getAs("EmployeeID"));
					employee_res.setEmployeeID(employeeID);
					
					// attribute [Employee.LastName]
					String lastName = Util.getStringValue(r.getAs("LastName"));
					employee_res.setLastName(lastName);
					
					// attribute [Employee.FirstName]
					String firstName = Util.getStringValue(r.getAs("FirstName"));
					employee_res.setFirstName(firstName);
					
					// attribute [Employee.Title]
					String title = Util.getStringValue(r.getAs("Title"));
					employee_res.setTitle(title);
					
					// attribute [Employee.TitleOfCourtesy]
					String titleOfCourtesy = Util.getStringValue(r.getAs("TitleOfCourtesy"));
					employee_res.setTitleOfCourtesy(titleOfCourtesy);
					
					// attribute [Employee.BirthDate]
					LocalDate birthDate = Util.getLocalDateValue(r.getAs("BirthDate"));
					employee_res.setBirthDate(birthDate);
					
					// attribute [Employee.HireDate]
					LocalDate hireDate = Util.getLocalDateValue(r.getAs("HireDate"));
					employee_res.setHireDate(hireDate);
					
					// attribute [Employee.Address]
					String address = Util.getStringValue(r.getAs("Address"));
					employee_res.setAddress(address);
					
					// attribute [Employee.City]
					String city = Util.getStringValue(r.getAs("City"));
					employee_res.setCity(city);
					
					// attribute [Employee.Region]
					String region = Util.getStringValue(r.getAs("Region"));
					employee_res.setRegion(region);
					
					// attribute [Employee.PostalCode]
					String postalCode = Util.getStringValue(r.getAs("PostalCode"));
					employee_res.setPostalCode(postalCode);
					
					// attribute [Employee.Country]
					String country = Util.getStringValue(r.getAs("Country"));
					employee_res.setCountry(country);
					
					// attribute [Employee.HomePhone]
					String homePhone = Util.getStringValue(r.getAs("HomePhone"));
					employee_res.setHomePhone(homePhone);
					
					// attribute [Employee.Extension]
					String extension = Util.getStringValue(r.getAs("Extension"));
					employee_res.setExtension(extension);
					
					// attribute [Employee.Photo]
					byte[] photo = Util.getByteArrayValue(r.getAs("Photo"));
					employee_res.setPhoto(photo);
					
					// attribute [Employee.Notes]
					String notes = Util.getStringValue(r.getAs("Notes"));
					employee_res.setNotes(notes);
					
					// attribute [Employee.PhotoPath]
					String photoPath = Util.getStringValue(r.getAs("PhotoPath"));
					employee_res.setPhotoPath(photoPath);
					
					// attribute [Employee.Salary]
					Double salary = Util.getDoubleValue(r.getAs("Salary"));
					employee_res.setSalary(salary);
	
	
	
					return employee_res;
				}, Encoders.bean(Employee.class));
	
	
		return res;
		
	}
	
	
	
	
	
	
	public Dataset<Employee> getEmployeeListInSold(conditions.Condition<conditions.OrderAttribute> order_condition,conditions.Condition<conditions.EmployeeAttribute> employee_condition)		{
		MutableBoolean employee_refilter = new MutableBoolean(false);
		List<Dataset<Employee>> datasetsPOJO = new ArrayList<Dataset<Employee>>();
		Dataset<Order> all = null;
		boolean all_already_persisted = false;
		MutableBoolean order_refilter;
		org.apache.spark.sql.Column joinCondition = null;
		
		order_refilter = new MutableBoolean(false);
		// For role 'order' in reference 'employeeRef'  B->A Scenario
		Dataset<OrderTDO> orderTDOemployeeReforder = soldService.getOrderTDOListOrderInEmployeeRefInOrderShippingFromMyRedis(order_condition, order_refilter);
		Dataset<EmployeeTDO> employeeTDOemployeeRefemployee = soldService.getEmployeeTDOListEmployeeInEmployeeRefInOrderShippingFromMyRedis(employee_condition, employee_refilter);
		if(order_refilter.booleanValue()) {
			if(all == null)
				all = new OrderServiceImpl().getOrderList(order_condition);
			joinCondition = null;
			joinCondition = orderTDOemployeeReforder.col("orderID").equalTo(all.col("orderID"));
			if(joinCondition == null)
				orderTDOemployeeReforder = orderTDOemployeeReforder.as("A").join(all).select("A.*").as(Encoders.bean(OrderTDO.class));
			else
				orderTDOemployeeReforder = orderTDOemployeeReforder.as("A").join(all, joinCondition).select("A.*").as(Encoders.bean(OrderTDO.class));
		}
		Dataset<Row> res_employeeRef = 
			employeeTDOemployeeRefemployee.join(orderTDOemployeeReforder
				.withColumnRenamed("orderID", "Order_orderID")
				.withColumnRenamed("orderDate", "Order_orderDate")
				.withColumnRenamed("requiredDate", "Order_requiredDate")
				.withColumnRenamed("shippedDate", "Order_shippedDate")
				.withColumnRenamed("freight", "Order_freight")
				.withColumnRenamed("shipName", "Order_shipName")
				.withColumnRenamed("shipAddress", "Order_shipAddress")
				.withColumnRenamed("shipCity", "Order_shipCity")
				.withColumnRenamed("shipRegion", "Order_shipRegion")
				.withColumnRenamed("shipPostalCode", "Order_shipPostalCode")
				.withColumnRenamed("shipCountry", "Order_shipCountry")
				.withColumnRenamed("logEvents", "Order_logEvents"),
				employeeTDOemployeeRefemployee.col("myRedis_orderShipping_employeeRef_target_EmployeeID").equalTo(orderTDOemployeeReforder.col("myRedis_orderShipping_employeeRef_source_EmployeeRef")));
		Dataset<Employee> res_Employee_employeeRef = res_employeeRef.select( "employeeID", "lastName", "firstName", "title", "titleOfCourtesy", "birthDate", "hireDate", "address", "city", "region", "postalCode", "country", "homePhone", "extension", "photo", "notes", "photoPath", "salary", "logEvents").as(Encoders.bean(Employee.class));
		res_Employee_employeeRef = res_Employee_employeeRef.dropDuplicates(new String[] {"employeeID"});
		datasetsPOJO.add(res_Employee_employeeRef);
		
		Dataset<Sold> res_sold_employee;
		Dataset<Employee> res_Employee;
		
		
		//Join datasets or return 
		Dataset<Employee> res = fullOuterJoinsEmployee(datasetsPOJO);
		if(res == null)
			return null;
	
		if(employee_refilter.booleanValue())
			res = res.filter((FilterFunction<Employee>) r -> employee_condition == null || employee_condition.evaluate(r));
		
	
		return res;
		}
	
	
	public boolean insertEmployee(Employee employee){
		// Insert into all mapped standalone AbstractPhysicalStructure 
		boolean inserted = false;
			inserted = insertEmployeeInEmployeesFromReldata(employee) || inserted ;
		return inserted;
	}
	
	public boolean insertEmployeeInEmployeesFromReldata(Employee employee)	{
		String idvalue="";
		idvalue+=employee.getEmployeeID();
		boolean entityExists = false; // Modify in acceleo code (in 'main.services.insert.entitytype.generateSimpleInsertMethods.mtl') to generate checking before insert
		if(!entityExists){
		List<String> columns = new ArrayList<>();
		List<Object> values = new ArrayList<>();	
		columns.add("EmployeeID");
		values.add(employee.getEmployeeID());
		columns.add("LastName");
		values.add(employee.getLastName());
		columns.add("FirstName");
		values.add(employee.getFirstName());
		columns.add("Title");
		values.add(employee.getTitle());
		columns.add("TitleOfCourtesy");
		values.add(employee.getTitleOfCourtesy());
		columns.add("BirthDate");
		values.add(employee.getBirthDate());
		columns.add("HireDate");
		values.add(employee.getHireDate());
		columns.add("Address");
		values.add(employee.getAddress());
		columns.add("City");
		values.add(employee.getCity());
		columns.add("Region");
		values.add(employee.getRegion());
		columns.add("PostalCode");
		values.add(employee.getPostalCode());
		columns.add("Country");
		values.add(employee.getCountry());
		columns.add("HomePhone");
		values.add(employee.getHomePhone());
		columns.add("Extension");
		values.add(employee.getExtension());
		columns.add("Photo");
		values.add(employee.getPhoto());
		columns.add("Notes");
		values.add(employee.getNotes());
		columns.add("PhotoPath");
		values.add(employee.getPhotoPath());
		columns.add("Salary");
		values.add(employee.getSalary());
		DBConnectionMgr.insertInTable(columns, Arrays.asList(values), "Employees", "reldata");
			logger.info("Inserted [Employee] entity ID [{}] in [Employees] in database [Reldata]", idvalue);
		}
		else
			logger.warn("[Employee] entity ID [{}] already present in [Employees] in database [Reldata]", idvalue);
		return !entityExists;
	} 
	
	private boolean inUpdateMethod = false;
	private List<Row> allEmployeeIdList = null;
	public void updateEmployeeList(conditions.Condition<conditions.EmployeeAttribute> condition, conditions.SetClause<conditions.EmployeeAttribute> set){
		inUpdateMethod = true;
		try {
			MutableBoolean refilterInEmployeesFromReldata = new MutableBoolean(false);
			getSQLWhereClauseInEmployeesFromReldata(condition, refilterInEmployeesFromReldata);
			// one first updates in the structures necessitating to execute a "SELECT *" query to establish the update condition 
			if(refilterInEmployeesFromReldata.booleanValue())
				updateEmployeeListInEmployeesFromReldata(condition, set);
		
	
			if(!refilterInEmployeesFromReldata.booleanValue())
				updateEmployeeListInEmployeesFromReldata(condition, set);
	
		} finally {
			inUpdateMethod = false;
		}
	}
	
	
	public void updateEmployeeListInEmployeesFromReldata(Condition<EmployeeAttribute> condition, SetClause<EmployeeAttribute> set) {
		List<String> setClause = EmployeeServiceImpl.getSQLSetClauseInEmployeesFromReldata(set);
		String setSQL = null;
		for(int i = 0; i < setClause.size(); i++) {
			if(i == 0)
				setSQL = setClause.get(i);
			else
				setSQL += ", " + setClause.get(i);
		}
		
		if(setSQL == null)
			return;
		
		MutableBoolean refilter = new MutableBoolean(false);
		Pair<String, List<String>> whereClause = EmployeeServiceImpl.getSQLWhereClauseInEmployeesFromReldata(condition, refilter);
		if(!refilter.booleanValue()) {
			String where = whereClause.getKey();
			List<String> preparedValues = whereClause.getValue();
			for(String preparedValue : preparedValues) {
				where = where.replaceFirst("\\?", preparedValue);
			}
			
			String sql = "UPDATE Employees SET " + setSQL;
			if(where != null)
				sql += " WHERE " + where;
			
			DBConnectionMgr.updateInTable(sql, "reldata");
		} else {
			if(!inUpdateMethod || allEmployeeIdList == null)
				allEmployeeIdList = this.getEmployeeList(condition).select("employeeID").collectAsList();
		
			List<String> updateQueries = new ArrayList<String>();
			for(Row row : allEmployeeIdList) {
				Condition<EmployeeAttribute> conditionId = null;
				conditionId = Condition.simple(EmployeeAttribute.employeeID, Operator.EQUALS, row.getAs("employeeID"));
				whereClause = EmployeeServiceImpl.getSQLWhereClauseInEmployeesFromReldata(conditionId, refilter);
				String sql = "UPDATE Employees SET " + setSQL;
				String where = whereClause.getKey();
				List<String> preparedValues = whereClause.getValue();
				for(String preparedValue : preparedValues) {
					where = where.replaceFirst("\\?", preparedValue);
				}
				if(where != null)
					sql += " WHERE " + where;
				updateQueries.add(sql);
			}
		
			DBConnectionMgr.updatesInTable(updateQueries, "reldata");
		}
		
	}
	
	
	
	public void updateEmployee(pojo.Employee employee) {
		//TODO using the id
		return;
	}
	public void updateEmployeeListInSold(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.EmployeeAttribute> employee_condition,
		
		conditions.SetClause<conditions.EmployeeAttribute> set
	){
		//TODO
	}
	
	public void updateEmployeeListInSoldByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.SetClause<conditions.EmployeeAttribute> set
	){
		updateEmployeeListInSold(order_condition, null, set);
	}
	
	public void updateEmployeeInSoldByOrder(
		pojo.Order order,
		conditions.SetClause<conditions.EmployeeAttribute> set 
	){
		//TODO get id in condition
		return;	
	}
	
	public void updateEmployeeListInSoldByEmployeeCondition(
		conditions.Condition<conditions.EmployeeAttribute> employee_condition,
		conditions.SetClause<conditions.EmployeeAttribute> set
	){
		updateEmployeeListInSold(null, employee_condition, set);
	}
	
	
	public void deleteEmployeeList(conditions.Condition<conditions.EmployeeAttribute> condition){
		//TODO
	}
	
	public void deleteEmployee(pojo.Employee employee) {
		//TODO using the id
		return;
	}
	public void deleteEmployeeListInSold(	
		conditions.Condition<conditions.OrderAttribute> order_condition,	
		conditions.Condition<conditions.EmployeeAttribute> employee_condition){
			//TODO
		}
	
	public void deleteEmployeeListInSoldByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteEmployeeListInSold(order_condition, null);
	}
	
	public void deleteEmployeeInSoldByOrder(
		pojo.Order order 
	){
		//TODO get id in condition
		return;	
	}
	
	public void deleteEmployeeListInSoldByEmployeeCondition(
		conditions.Condition<conditions.EmployeeAttribute> employee_condition
	){
		deleteEmployeeListInSold(null, employee_condition);
	}
	
}
