package dao.impl;

import exceptions.PhysicalStructureException;
import java.util.Arrays;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.apache.commons.lang3.StringUtils;
import util.Dataset;
import conditions.Condition;
import java.util.HashSet;
import java.util.Set;
import conditions.AndCondition;
import conditions.OrCondition;
import conditions.SimpleCondition;
import conditions.BoughtAttribute;
import conditions.Operator;
import tdo.*;
import pojo.*;
import tdo.OrderTDO;
import tdo.BoughtTDO;
import conditions.OrderAttribute;
import dao.services.OrderService;
import tdo.CustomerTDO;
import tdo.BoughtTDO;
import conditions.CustomerAttribute;
import dao.services.CustomerService;
import java.util.List;
import java.util.ArrayList;
import util.ScalaUtil;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.commons.lang3.mutable.MutableBoolean;
import util.Util;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import util.Row;
import org.apache.spark.sql.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import util.WrappedArray;
import org.apache.spark.api.java.function.FlatMapFunction;
import dbconnection.SparkConnectionMgr;
import dbconnection.DBConnectionMgr;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.ArrayType;
import static com.mongodb.client.model.Updates.addToSet;
import org.bson.Document;
import org.bson.conversions.Bson;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

public class BoughtServiceImpl extends dao.services.BoughtService {
	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BoughtServiceImpl.class);
	
	
	// Left side 'CustomerRef' of reference [customerRef ]
	public Dataset<OrderTDO> getOrderTDOListBoughtOrderInCustomerRefInOrderShippingFromMyRedis(Condition<OrderAttribute> condition, MutableBoolean refilterFlag){	
		// Build the key pattern
		//  - If the condition attribute is in the key pattern, replace by the value. Only if operator is EQUALS.
		//  - Replace all other fields of key pattern by a '*' 
		String keypattern= "", keypatternAllVariables="";
		String valueCond=null;
		String finalKeypattern;
		List<String> fieldsListInKey = new ArrayList<>();
		Set<OrderAttribute> keyAttributes = new HashSet<>();
		keypattern=keypattern.concat("ORDER:");
		keypatternAllVariables=keypatternAllVariables.concat("ORDER:");
		if(!Util.containsOrCondition(condition)){
			valueCond=Util.getStringValue(Util.getValueOfAttributeInEqualCondition(condition,OrderAttribute.orderID));
			keyAttributes.add(OrderAttribute.orderID);
		}
		else{
			valueCond=null;
			refilterFlag.setValue(true);
		}
		if(valueCond==null)
			keypattern=keypattern.concat("*");
		else
			keypattern=keypattern.concat(valueCond);
		fieldsListInKey.add("orderid");
		keypatternAllVariables=keypatternAllVariables.concat("*");
		if(!refilterFlag.booleanValue()){
			Set<OrderAttribute> conditionAttributes = Util.getConditionAttributes(condition);
			for (OrderAttribute a : conditionAttributes) {
				if (!keyAttributes.contains(a)) {
					refilterFlag.setValue(true);
					break;
				}
			}
		}
	
			
		// Find the type of query to perform in order to retrieve a Dataset<Row>
		// Based on the type of the value. Is a it a simple string or a hash or a list... 
		Dataset<Row> rows;
		StructType structType = new StructType(new StructField[] {
			DataTypes.createStructField("_id", DataTypes.StringType, true), //technical field to store the key.
			DataTypes.createStructField("OrderDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("RequiredDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShippedDate", DataTypes.StringType, true)
	,		DataTypes.createStructField("Freight", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipName", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipAddress", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipCity", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipRegion", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipPostalCode", DataTypes.StringType, true)
	,		DataTypes.createStructField("ShipCountry", DataTypes.StringType, true)
	,		DataTypes.createStructField("CustomerRef", DataTypes.StringType, true)
	,		DataTypes.createStructField("EmployeeRef", DataTypes.StringType, true)
		});
		rows = SparkConnectionMgr.getRowsFromKeyValueHashes("myRedis",keypattern, structType);
		if(rows == null || rows.isEmpty())
				return null;
		boolean isStriped = false;
		String prefix=isStriped?keypattern.substring(0, keypattern.length() - 1):"";
		finalKeypattern = keypatternAllVariables;
		Dataset<OrderTDO> res = rows.map((MapFunction<Row, OrderTDO>) r -> {
					OrderTDO order_res = new OrderTDO();
					Integer groupindex = null;
					String regex = null;
					String value = null;
					Pattern p, pattern = null;
					Matcher m, match = null;
					boolean matches = false;
					String key = isStriped ? prefix + r.getAs("_id") : r.getAs("_id");
					// Spark Redis automatically strips leading character if the pattern provided contains a single '*' at the end.				
					pattern = Pattern.compile("\\*");
			        match = pattern.matcher(finalKeypattern);
					regex = finalKeypattern.replaceAll("\\*","(.*)");
					p = Pattern.compile(regex);
					m = p.matcher(key);
					matches = m.find();
					// attribute [Order.OrderID]
					// Attribute mapped in a key.
					groupindex = fieldsListInKey.indexOf("orderid")+1;
					if(groupindex==null) {
						logger.warn("Attribute 'Order' mapped physical field 'orderid' found in key but can't get index in build keypattern '{}'.", finalKeypattern);
					}
					String orderID = null;
					if(matches) {
						orderID = m.group(groupindex.intValue());
					} else {
						logger.warn("Cannot retrieve value for OrderorderID attribute stored in db myRedis. Regex [{}] Value [{}]",regex,value);
						order_res.addLogEvent("Cannot retrieve value for Order.orderID attribute stored in db myRedis. Probably due to an ambiguous regex.");
					}
					order_res.setOrderID(orderID == null ? null : Integer.parseInt(orderID));
					// attribute [Order.OrderDate]
					LocalDate orderDate = r.getAs("OrderDate") == null ? null : LocalDate.parse(r.getAs("OrderDate"));
					order_res.setOrderDate(orderDate);
					// attribute [Order.RequiredDate]
					LocalDate requiredDate = r.getAs("RequiredDate") == null ? null : LocalDate.parse(r.getAs("RequiredDate"));
					order_res.setRequiredDate(requiredDate);
					// attribute [Order.ShippedDate]
					LocalDate shippedDate = r.getAs("ShippedDate") == null ? null : LocalDate.parse(r.getAs("ShippedDate"));
					order_res.setShippedDate(shippedDate);
					// attribute [Order.Freight]
					Double freight = r.getAs("Freight") == null ? null : Double.parseDouble(r.getAs("Freight"));
					order_res.setFreight(freight);
					// attribute [Order.ShipName]
					String shipName = r.getAs("ShipName") == null ? null : r.getAs("ShipName");
					order_res.setShipName(shipName);
					// attribute [Order.ShipAddress]
					String shipAddress = r.getAs("ShipAddress") == null ? null : r.getAs("ShipAddress");
					order_res.setShipAddress(shipAddress);
					// attribute [Order.ShipCity]
					String shipCity = r.getAs("ShipCity") == null ? null : r.getAs("ShipCity");
					order_res.setShipCity(shipCity);
					// attribute [Order.ShipRegion]
					String shipRegion = r.getAs("ShipRegion") == null ? null : r.getAs("ShipRegion");
					order_res.setShipRegion(shipRegion);
					// attribute [Order.ShipPostalCode]
					String shipPostalCode = r.getAs("ShipPostalCode") == null ? null : r.getAs("ShipPostalCode");
					order_res.setShipPostalCode(shipPostalCode);
					// attribute [Order.ShipCountry]
					String shipCountry = r.getAs("ShipCountry") == null ? null : r.getAs("ShipCountry");
					order_res.setShipCountry(shipCountry);
					// Get reference column in value [CustomerRef ] for reference [customerRef]
					order_res.setMyRedis_orderShipping_customerRef_source_CustomerRef(r.getAs("CustomerRef"));
	
						return order_res;
				}, Encoders.bean(OrderTDO.class));
		res=res.dropDuplicates(new String[] {"orderID"});
		return res;
	}
	
	// Right side 'CustomerID' of reference [customerRef ]
	public Dataset<CustomerTDO> getCustomerTDOListCustomerInCustomerRefInOrderShippingFromMyRedis(Condition<CustomerAttribute> condition, MutableBoolean refilterFlag){
		String bsonQuery = CustomerServiceImpl.getBSONMatchQueryInCustomersFromMyMongoDB(condition, refilterFlag);
		if(bsonQuery != null) {
			bsonQuery = "{$match: {" + bsonQuery + "}}";	
		} 
		
		Dataset<Row> dataset = dbconnection.SparkConnectionMgr.getDatasetFromMongoDB("myMongoDB", "Customers", bsonQuery);
	
		Dataset<CustomerTDO> res = dataset.flatMap((FlatMapFunction<Row, CustomerTDO>) r -> {
				Set<CustomerTDO> list_res = new HashSet<CustomerTDO>();
				Integer groupIndex = null;
				String regex = null;
				String value = null;
				Pattern p = null;
				Matcher m = null;
				boolean matches = false;
				Row nestedRow = null;
	
				boolean addedInList = false;
				Row r1 = r;
				CustomerTDO customer1 = new CustomerTDO();
					boolean toAdd1  = false;
					WrappedArray array1  = null;
					// 	attribute Customer.customerID for field CustomerID			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("CustomerID")) {
						if(nestedRow.getAs("CustomerID") == null){
							customer1.setCustomerID(null);
						}else{
							customer1.setCustomerID(Util.getStringValue(nestedRow.getAs("CustomerID")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.address for field Address			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Address")) {
						if(nestedRow.getAs("Address") == null){
							customer1.setAddress(null);
						}else{
							customer1.setAddress(Util.getStringValue(nestedRow.getAs("Address")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.city for field City			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("City")) {
						if(nestedRow.getAs("City") == null){
							customer1.setCity(null);
						}else{
							customer1.setCity(Util.getStringValue(nestedRow.getAs("City")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.companyName for field CompanyName			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("CompanyName")) {
						if(nestedRow.getAs("CompanyName") == null){
							customer1.setCompanyName(null);
						}else{
							customer1.setCompanyName(Util.getStringValue(nestedRow.getAs("CompanyName")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.contactName for field ContactName			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ContactName")) {
						if(nestedRow.getAs("ContactName") == null){
							customer1.setContactName(null);
						}else{
							customer1.setContactName(Util.getStringValue(nestedRow.getAs("ContactName")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.contactTitle for field ContactTitle			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ContactTitle")) {
						if(nestedRow.getAs("ContactTitle") == null){
							customer1.setContactTitle(null);
						}else{
							customer1.setContactTitle(Util.getStringValue(nestedRow.getAs("ContactTitle")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.country for field Country			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Country")) {
						if(nestedRow.getAs("Country") == null){
							customer1.setCountry(null);
						}else{
							customer1.setCountry(Util.getStringValue(nestedRow.getAs("Country")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.fax for field Fax			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Fax")) {
						if(nestedRow.getAs("Fax") == null){
							customer1.setFax(null);
						}else{
							customer1.setFax(Util.getStringValue(nestedRow.getAs("Fax")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.phone for field Phone			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Phone")) {
						if(nestedRow.getAs("Phone") == null){
							customer1.setPhone(null);
						}else{
							customer1.setPhone(Util.getStringValue(nestedRow.getAs("Phone")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.postalCode for field PostalCode			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("PostalCode")) {
						if(nestedRow.getAs("PostalCode") == null){
							customer1.setPostalCode(null);
						}else{
							customer1.setPostalCode(Util.getStringValue(nestedRow.getAs("PostalCode")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.region for field Region			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Region")) {
						if(nestedRow.getAs("Region") == null){
							customer1.setRegion(null);
						}else{
							customer1.setRegion(Util.getStringValue(nestedRow.getAs("Region")));
							toAdd1 = true;					
							}
					}
					
						// field  CustomerID for reference customerRef . Reference field : CustomerID
					nestedRow =  r1;
					if(nestedRow != null) {
						customer1.setMyRedis_orderShipping_customerRef_target_CustomerID(nestedRow.getAs("CustomerID") == null ? null : nestedRow.getAs("CustomerID").toString());
						toAdd1 = true;					
					}
					
					
					if(toAdd1) {
						list_res.add(customer1);
						addedInList = true;
					} 
					
					
				
				return list_res.iterator();
	
		}, Encoders.bean(CustomerTDO.class));
		res= res.dropDuplicates(new String[]{"customerID"});
		return res;
	}
	
	
	
	// method accessing the embedded object orders mapped to role customer
	public Dataset<Bought> getBoughtListInmyMongoDBCustomersorders(Condition<CustomerAttribute> customer_condition, Condition<OrderAttribute> boughtOrder_condition, MutableBoolean customer_refilter, MutableBoolean boughtOrder_refilter){	
			List<String> bsons = new ArrayList<String>();
			String bson = null;
			bson = OrderServiceImpl.getBSONMatchQueryInCustomersFromMyMongoDB(boughtOrder_condition ,boughtOrder_refilter);
			if(bson != null)
				bsons.add("{" + bson + "}");
			bson = CustomerServiceImpl.getBSONMatchQueryInCustomersFromMyMongoDB(customer_condition ,customer_refilter);
			if(bson != null)
				bsons.add("{" + bson + "}");
		
			String bsonQuery = bsons.size() == 0 ? null : "{$match: { $and: [" + String.join(",", bsons) + "] }}";
		
			Dataset<Row> dataset = dbconnection.SparkConnectionMgr.getDatasetFromMongoDB("myMongoDB", "Customers", bsonQuery);
		
			Dataset<Bought> res = dataset.flatMap((FlatMapFunction<Row, Bought>) r -> {
					List<Bought> list_res = new ArrayList<Bought>();
					Integer groupIndex = null;
					String regex = null;
					String value = null;
					Pattern p = null;
					Matcher m = null;
					boolean matches = false;
					Row nestedRow = null;
		
					boolean addedInList = false;
					Row r1 = r;
					Bought bought1 = new Bought();
					bought1.setBoughtOrder(new Order());
					bought1.setCustomer(new Customer());
					
					boolean toAdd1  = false;
					WrappedArray array1  = null;
					// 	attribute Customer.customerID for field CustomerID			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("CustomerID")) {
						if(nestedRow.getAs("CustomerID")==null)
							bought1.getCustomer().setCustomerID(null);
						else{
							bought1.getCustomer().setCustomerID(Util.getStringValue(nestedRow.getAs("CustomerID")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.address for field Address			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Address")) {
						if(nestedRow.getAs("Address")==null)
							bought1.getCustomer().setAddress(null);
						else{
							bought1.getCustomer().setAddress(Util.getStringValue(nestedRow.getAs("Address")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.city for field City			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("City")) {
						if(nestedRow.getAs("City")==null)
							bought1.getCustomer().setCity(null);
						else{
							bought1.getCustomer().setCity(Util.getStringValue(nestedRow.getAs("City")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.companyName for field CompanyName			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("CompanyName")) {
						if(nestedRow.getAs("CompanyName")==null)
							bought1.getCustomer().setCompanyName(null);
						else{
							bought1.getCustomer().setCompanyName(Util.getStringValue(nestedRow.getAs("CompanyName")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.contactName for field ContactName			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ContactName")) {
						if(nestedRow.getAs("ContactName")==null)
							bought1.getCustomer().setContactName(null);
						else{
							bought1.getCustomer().setContactName(Util.getStringValue(nestedRow.getAs("ContactName")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.contactTitle for field ContactTitle			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ContactTitle")) {
						if(nestedRow.getAs("ContactTitle")==null)
							bought1.getCustomer().setContactTitle(null);
						else{
							bought1.getCustomer().setContactTitle(Util.getStringValue(nestedRow.getAs("ContactTitle")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.country for field Country			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Country")) {
						if(nestedRow.getAs("Country")==null)
							bought1.getCustomer().setCountry(null);
						else{
							bought1.getCustomer().setCountry(Util.getStringValue(nestedRow.getAs("Country")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.fax for field Fax			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Fax")) {
						if(nestedRow.getAs("Fax")==null)
							bought1.getCustomer().setFax(null);
						else{
							bought1.getCustomer().setFax(Util.getStringValue(nestedRow.getAs("Fax")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.phone for field Phone			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Phone")) {
						if(nestedRow.getAs("Phone")==null)
							bought1.getCustomer().setPhone(null);
						else{
							bought1.getCustomer().setPhone(Util.getStringValue(nestedRow.getAs("Phone")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.postalCode for field PostalCode			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("PostalCode")) {
						if(nestedRow.getAs("PostalCode")==null)
							bought1.getCustomer().setPostalCode(null);
						else{
							bought1.getCustomer().setPostalCode(Util.getStringValue(nestedRow.getAs("PostalCode")));
							toAdd1 = true;					
							}
					}
					// 	attribute Customer.region for field Region			
					nestedRow =  r1;
					if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Region")) {
						if(nestedRow.getAs("Region")==null)
							bought1.getCustomer().setRegion(null);
						else{
							bought1.getCustomer().setRegion(Util.getStringValue(nestedRow.getAs("Region")));
							toAdd1 = true;					
							}
					}
					array1 = r1.getAs("orders");
					if(array1!= null) {
						for (int i2 = 0; i2 < array1.size(); i2++){
							Row r2 = (Row) array1.apply(i2);
							Bought bought2 = (Bought) bought1.clone();
							boolean toAdd2  = false;
							WrappedArray array2  = null;
							// 	attribute Order.orderID for field OrderID			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderID")) {
								if(nestedRow.getAs("OrderID")==null)
									bought2.getBoughtOrder().setOrderID(null);
								else{
									bought2.getBoughtOrder().setOrderID(Util.getIntegerValue(nestedRow.getAs("OrderID")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.orderDate for field OrderDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderDate")) {
								if(nestedRow.getAs("OrderDate")==null)
									bought2.getBoughtOrder().setOrderDate(null);
								else{
									bought2.getBoughtOrder().setOrderDate(Util.getLocalDateValue(nestedRow.getAs("OrderDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.requiredDate for field RequiredDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("RequiredDate")) {
								if(nestedRow.getAs("RequiredDate")==null)
									bought2.getBoughtOrder().setRequiredDate(null);
								else{
									bought2.getBoughtOrder().setRequiredDate(Util.getLocalDateValue(nestedRow.getAs("RequiredDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shippedDate for field ShippedDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShippedDate")) {
								if(nestedRow.getAs("ShippedDate")==null)
									bought2.getBoughtOrder().setShippedDate(null);
								else{
									bought2.getBoughtOrder().setShippedDate(Util.getLocalDateValue(nestedRow.getAs("ShippedDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.freight for field Freight			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Freight")) {
								if(nestedRow.getAs("Freight")==null)
									bought2.getBoughtOrder().setFreight(null);
								else{
									bought2.getBoughtOrder().setFreight(Util.getDoubleValue(nestedRow.getAs("Freight")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipName for field ShipName			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipName")) {
								if(nestedRow.getAs("ShipName")==null)
									bought2.getBoughtOrder().setShipName(null);
								else{
									bought2.getBoughtOrder().setShipName(Util.getStringValue(nestedRow.getAs("ShipName")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipAddress for field ShipAddress			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipAddress")) {
								if(nestedRow.getAs("ShipAddress")==null)
									bought2.getBoughtOrder().setShipAddress(null);
								else{
									bought2.getBoughtOrder().setShipAddress(Util.getStringValue(nestedRow.getAs("ShipAddress")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCity for field ShipCity			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCity")) {
								if(nestedRow.getAs("ShipCity")==null)
									bought2.getBoughtOrder().setShipCity(null);
								else{
									bought2.getBoughtOrder().setShipCity(Util.getStringValue(nestedRow.getAs("ShipCity")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipRegion for field ShipRegion			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipRegion")) {
								if(nestedRow.getAs("ShipRegion")==null)
									bought2.getBoughtOrder().setShipRegion(null);
								else{
									bought2.getBoughtOrder().setShipRegion(Util.getStringValue(nestedRow.getAs("ShipRegion")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipPostalCode for field ShipPostalCode			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipPostalCode")) {
								if(nestedRow.getAs("ShipPostalCode")==null)
									bought2.getBoughtOrder().setShipPostalCode(null);
								else{
									bought2.getBoughtOrder().setShipPostalCode(Util.getStringValue(nestedRow.getAs("ShipPostalCode")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCountry for field ShipCountry			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCountry")) {
								if(nestedRow.getAs("ShipCountry")==null)
									bought2.getBoughtOrder().setShipCountry(null);
								else{
									bought2.getBoughtOrder().setShipCountry(Util.getStringValue(nestedRow.getAs("ShipCountry")));
									toAdd2 = true;					
									}
							}
							if(toAdd2 && ((boughtOrder_condition == null || boughtOrder_refilter.booleanValue() || boughtOrder_condition.evaluate(bought2.getBoughtOrder()))&&(customer_condition == null || customer_refilter.booleanValue() || customer_condition.evaluate(bought2.getCustomer())))) {
								if(!(bought2.getBoughtOrder().equals(new Order())) && !(bought2.getCustomer().equals(new Customer())))
									list_res.add(bought2);
								addedInList = true;
							} 
							if(addedInList)
								toAdd1 = false;
						}
					}
					
					if(toAdd1 ) {
						if(!(bought1.getBoughtOrder().equals(new Order())) && !(bought1.getCustomer().equals(new Customer())))
							list_res.add(bought1);
						addedInList = true;
					} 
					
					
					
					return list_res.iterator();
		
			}, Encoders.bean(Bought.class));
			// TODO drop duplicates based on roles ids
			res= res.dropDuplicates(new String[]{"boughtOrder.orderID","customer.customerID"});
			return res;
	}
	
	public Dataset<Bought> getBoughtList(
		Condition<OrderAttribute> boughtOrder_condition,
		Condition<CustomerAttribute> customer_condition){
			BoughtServiceImpl boughtService = this;
			OrderService orderService = new OrderServiceImpl();  
			CustomerService customerService = new CustomerServiceImpl();
			MutableBoolean boughtOrder_refilter = new MutableBoolean(false);
			List<Dataset<Bought>> datasetsPOJO = new ArrayList<Dataset<Bought>>();
			boolean all_already_persisted = false;
			MutableBoolean customer_refilter = new MutableBoolean(false);
			
			org.apache.spark.sql.Column joinCondition = null;
			// For role 'boughtOrder' in reference 'customerRef'. A->B Scenario
			customer_refilter = new MutableBoolean(false);
			Dataset<OrderTDO> orderTDOcustomerRefboughtOrder = boughtService.getOrderTDOListBoughtOrderInCustomerRefInOrderShippingFromMyRedis(boughtOrder_condition, boughtOrder_refilter);
			Dataset<CustomerTDO> customerTDOcustomerRefcustomer = boughtService.getCustomerTDOListCustomerInCustomerRefInOrderShippingFromMyRedis(customer_condition, customer_refilter);
			
			Dataset<Row> res_customerRef_temp = orderTDOcustomerRefboughtOrder.join(customerTDOcustomerRefcustomer
					.withColumnRenamed("address", "Customer_address")
					.withColumnRenamed("city", "Customer_city")
					.withColumnRenamed("companyName", "Customer_companyName")
					.withColumnRenamed("contactName", "Customer_contactName")
					.withColumnRenamed("contactTitle", "Customer_contactTitle")
					.withColumnRenamed("country", "Customer_country")
					.withColumnRenamed("customerID", "Customer_customerID")
					.withColumnRenamed("fax", "Customer_fax")
					.withColumnRenamed("phone", "Customer_phone")
					.withColumnRenamed("postalCode", "Customer_postalCode")
					.withColumnRenamed("region", "Customer_region")
					.withColumnRenamed("logEvents", "Customer_logEvents"),
					orderTDOcustomerRefboughtOrder.col("myRedis_orderShipping_customerRef_source_CustomerRef").equalTo(customerTDOcustomerRefcustomer.col("myRedis_orderShipping_customerRef_target_CustomerID")));
		
			Dataset<Bought> res_customerRef = res_customerRef_temp.map(
				(MapFunction<Row, Bought>) r -> {
					Bought res = new Bought();
					Order A = new Order();
					Customer B = new Customer();
					A.setOrderID(Util.getIntegerValue(r.getAs("orderID")));
					A.setOrderDate(Util.getLocalDateValue(r.getAs("orderDate")));
					A.setRequiredDate(Util.getLocalDateValue(r.getAs("requiredDate")));
					A.setShippedDate(Util.getLocalDateValue(r.getAs("shippedDate")));
					A.setFreight(Util.getDoubleValue(r.getAs("freight")));
					A.setShipName(Util.getStringValue(r.getAs("shipName")));
					A.setShipAddress(Util.getStringValue(r.getAs("shipAddress")));
					A.setShipCity(Util.getStringValue(r.getAs("shipCity")));
					A.setShipRegion(Util.getStringValue(r.getAs("shipRegion")));
					A.setShipPostalCode(Util.getStringValue(r.getAs("shipPostalCode")));
					A.setShipCountry(Util.getStringValue(r.getAs("shipCountry")));
					A.setLogEvents((ArrayList<String>) ScalaUtil.javaList(r.getAs("logEvents")));
		
					B.setAddress(Util.getStringValue(r.getAs("Customer_address")));
					B.setCity(Util.getStringValue(r.getAs("Customer_city")));
					B.setCompanyName(Util.getStringValue(r.getAs("Customer_companyName")));
					B.setContactName(Util.getStringValue(r.getAs("Customer_contactName")));
					B.setContactTitle(Util.getStringValue(r.getAs("Customer_contactTitle")));
					B.setCountry(Util.getStringValue(r.getAs("Customer_country")));
					B.setCustomerID(Util.getStringValue(r.getAs("Customer_customerID")));
					B.setFax(Util.getStringValue(r.getAs("Customer_fax")));
					B.setPhone(Util.getStringValue(r.getAs("Customer_phone")));
					B.setPostalCode(Util.getStringValue(r.getAs("Customer_postalCode")));
					B.setRegion(Util.getStringValue(r.getAs("Customer_region")));
					B.setLogEvents((ArrayList<String>) ScalaUtil.javaList(r.getAs("Customer_logEvents")));
						
					res.setBoughtOrder(A);
					res.setCustomer(B);
					return res;
				},Encoders.bean(Bought.class)
			);
		
			datasetsPOJO.add(res_customerRef);
		
			
			Dataset<Bought> res_bought_boughtOrder;
			Dataset<Order> res_Order;
			// Role 'customer' mapped to EmbeddedObject 'orders' 'Order' containing 'Customer' 
			customer_refilter = new MutableBoolean(false);
			res_bought_boughtOrder = boughtService.getBoughtListInmyMongoDBCustomersorders(customer_condition, boughtOrder_condition, customer_refilter, boughtOrder_refilter);
			
			datasetsPOJO.add(res_bought_boughtOrder);
			
			
			//Join datasets or return 
			Dataset<Bought> res = fullOuterJoinsBought(datasetsPOJO);
			if(res == null)
				return null;
		
			Dataset<Order> lonelyBoughtOrder = null;
			Dataset<Customer> lonelyCustomer = null;
			
		
		
			
			if(boughtOrder_refilter.booleanValue() || customer_refilter.booleanValue())
				res = res.filter((FilterFunction<Bought>) r -> (boughtOrder_condition == null || boughtOrder_condition.evaluate(r.getBoughtOrder())) && (customer_condition == null || customer_condition.evaluate(r.getCustomer())));
			
		
			return res;
		
		}
	
	public Dataset<Bought> getBoughtListByBoughtOrderCondition(
		Condition<OrderAttribute> boughtOrder_condition
	){
		return getBoughtList(boughtOrder_condition, null);
	}
	
	public Bought getBoughtByBoughtOrder(Order boughtOrder) {
		Condition<OrderAttribute> cond = null;
		cond = Condition.simple(OrderAttribute.orderID, Operator.EQUALS, boughtOrder.getOrderID());
		Dataset<Bought> res = getBoughtListByBoughtOrderCondition(cond);
		List<Bought> list = res.collectAsList();
		if(list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	public Dataset<Bought> getBoughtListByCustomerCondition(
		Condition<CustomerAttribute> customer_condition
	){
		return getBoughtList(null, customer_condition);
	}
	
	public Dataset<Bought> getBoughtListByCustomer(Customer customer) {
		Condition<CustomerAttribute> cond = null;
		cond = Condition.simple(CustomerAttribute.customerID, Operator.EQUALS, customer.getCustomerID());
		Dataset<Bought> res = getBoughtListByCustomerCondition(cond);
	return res;
	}
	
	
	
	public void deleteBoughtList(
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition,
		conditions.Condition<conditions.CustomerAttribute> customer_condition){
			//TODO
		}
	
	public void deleteBoughtListByBoughtOrderCondition(
		conditions.Condition<conditions.OrderAttribute> boughtOrder_condition
	){
		deleteBoughtList(boughtOrder_condition, null);
	}
	
	public void deleteBoughtByBoughtOrder(pojo.Order boughtOrder) {
		// TODO using id for selecting
		return;
	}
	public void deleteBoughtListByCustomerCondition(
		conditions.Condition<conditions.CustomerAttribute> customer_condition
	){
		deleteBoughtList(null, customer_condition);
	}
	
	public void deleteBoughtListByCustomer(pojo.Customer customer) {
		// TODO using id for selecting
		return;
	}
		
}
