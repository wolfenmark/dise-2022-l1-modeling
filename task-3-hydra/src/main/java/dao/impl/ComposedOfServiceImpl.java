package dao.impl;

import exceptions.PhysicalStructureException;
import java.util.Arrays;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.apache.commons.lang3.StringUtils;
import util.Dataset;
import conditions.Condition;
import java.util.HashSet;
import java.util.Set;
import conditions.AndCondition;
import conditions.OrCondition;
import conditions.SimpleCondition;
import conditions.ComposedOfAttribute;
import conditions.Operator;
import tdo.*;
import pojo.*;
import tdo.OrderTDO;
import tdo.ComposedOfTDO;
import conditions.OrderAttribute;
import dao.services.OrderService;
import tdo.ProductTDO;
import tdo.ComposedOfTDO;
import conditions.ProductAttribute;
import dao.services.ProductService;
import java.util.List;
import java.util.ArrayList;
import util.ScalaUtil;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.commons.lang3.mutable.MutableBoolean;
import util.Util;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import util.Row;
import org.apache.spark.sql.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import util.WrappedArray;
import org.apache.spark.api.java.function.FlatMapFunction;
import dbconnection.SparkConnectionMgr;
import dbconnection.DBConnectionMgr;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.ArrayType;
import static com.mongodb.client.model.Updates.addToSet;
import org.bson.Document;
import org.bson.conversions.Bson;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

public class ComposedOfServiceImpl extends dao.services.ComposedOfService {
	static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ComposedOfServiceImpl.class);
	
	// method accessing the embedded object orders mapped to role order
	public Dataset<ComposedOf> getComposedOfListInmyMongoDBCustomersorders(Condition<OrderAttribute> order_condition, Condition<ProductAttribute> product_condition, MutableBoolean order_refilter, MutableBoolean product_refilter){	
			List<String> bsons = new ArrayList<String>();
			String bson = null;
			bson = OrderServiceImpl.getBSONMatchQueryInCustomersFromMyMongoDB(order_condition ,order_refilter);
			if(bson != null)
				bsons.add("{" + bson + "}");
			bson = ProductServiceImpl.getBSONMatchQueryInCustomersFromMyMongoDB(product_condition ,product_refilter);
			if(bson != null)
				bsons.add("{" + bson + "}");
		
			String bsonQuery = bsons.size() == 0 ? null : "{$match: { $and: [" + String.join(",", bsons) + "] }}";
		
			Dataset<Row> dataset = dbconnection.SparkConnectionMgr.getDatasetFromMongoDB("myMongoDB", "Customers", bsonQuery);
		
			Dataset<ComposedOf> res = dataset.flatMap((FlatMapFunction<Row, ComposedOf>) r -> {
					List<ComposedOf> list_res = new ArrayList<ComposedOf>();
					Integer groupIndex = null;
					String regex = null;
					String value = null;
					Pattern p = null;
					Matcher m = null;
					boolean matches = false;
					Row nestedRow = null;
		
					boolean addedInList = false;
					Row r1 = r;
					ComposedOf composedOf1 = new ComposedOf();
					composedOf1.setOrder(new Order());
					composedOf1.setProduct(new Product());
					
					boolean toAdd1  = false;
					WrappedArray array1  = null;
					array1 = r1.getAs("orders");
					if(array1!= null) {
						for (int i2 = 0; i2 < array1.size(); i2++){
							Row r2 = (Row) array1.apply(i2);
							ComposedOf composedOf2 = (ComposedOf) composedOf1.clone();
							boolean toAdd2  = false;
							WrappedArray array2  = null;
							// 	attribute Order.orderID for field OrderID			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderID")) {
								if(nestedRow.getAs("OrderID")==null)
									composedOf2.getOrder().setOrderID(null);
								else{
									composedOf2.getOrder().setOrderID(Util.getIntegerValue(nestedRow.getAs("OrderID")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.orderDate for field OrderDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderDate")) {
								if(nestedRow.getAs("OrderDate")==null)
									composedOf2.getOrder().setOrderDate(null);
								else{
									composedOf2.getOrder().setOrderDate(Util.getLocalDateValue(nestedRow.getAs("OrderDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.requiredDate for field RequiredDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("RequiredDate")) {
								if(nestedRow.getAs("RequiredDate")==null)
									composedOf2.getOrder().setRequiredDate(null);
								else{
									composedOf2.getOrder().setRequiredDate(Util.getLocalDateValue(nestedRow.getAs("RequiredDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shippedDate for field ShippedDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShippedDate")) {
								if(nestedRow.getAs("ShippedDate")==null)
									composedOf2.getOrder().setShippedDate(null);
								else{
									composedOf2.getOrder().setShippedDate(Util.getLocalDateValue(nestedRow.getAs("ShippedDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.freight for field Freight			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Freight")) {
								if(nestedRow.getAs("Freight")==null)
									composedOf2.getOrder().setFreight(null);
								else{
									composedOf2.getOrder().setFreight(Util.getDoubleValue(nestedRow.getAs("Freight")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipName for field ShipName			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipName")) {
								if(nestedRow.getAs("ShipName")==null)
									composedOf2.getOrder().setShipName(null);
								else{
									composedOf2.getOrder().setShipName(Util.getStringValue(nestedRow.getAs("ShipName")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipAddress for field ShipAddress			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipAddress")) {
								if(nestedRow.getAs("ShipAddress")==null)
									composedOf2.getOrder().setShipAddress(null);
								else{
									composedOf2.getOrder().setShipAddress(Util.getStringValue(nestedRow.getAs("ShipAddress")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCity for field ShipCity			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCity")) {
								if(nestedRow.getAs("ShipCity")==null)
									composedOf2.getOrder().setShipCity(null);
								else{
									composedOf2.getOrder().setShipCity(Util.getStringValue(nestedRow.getAs("ShipCity")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipRegion for field ShipRegion			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipRegion")) {
								if(nestedRow.getAs("ShipRegion")==null)
									composedOf2.getOrder().setShipRegion(null);
								else{
									composedOf2.getOrder().setShipRegion(Util.getStringValue(nestedRow.getAs("ShipRegion")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipPostalCode for field ShipPostalCode			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipPostalCode")) {
								if(nestedRow.getAs("ShipPostalCode")==null)
									composedOf2.getOrder().setShipPostalCode(null);
								else{
									composedOf2.getOrder().setShipPostalCode(Util.getStringValue(nestedRow.getAs("ShipPostalCode")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCountry for field ShipCountry			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCountry")) {
								if(nestedRow.getAs("ShipCountry")==null)
									composedOf2.getOrder().setShipCountry(null);
								else{
									composedOf2.getOrder().setShipCountry(Util.getStringValue(nestedRow.getAs("ShipCountry")));
									toAdd2 = true;					
									}
							}
							array2 = r2.getAs("products");
							if(array2!= null) {
								for (int i3 = 0; i3 < array2.size(); i3++){
									Row r3 = (Row) array2.apply(i3);
									ComposedOf composedOf3 = (ComposedOf) composedOf2.clone();
									boolean toAdd3  = false;
									WrappedArray array3  = null;
									// 	attribute Product.productID for field ProductID			
									nestedRow =  r3;
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ProductID")) {
										if(nestedRow.getAs("ProductID")==null)
											composedOf3.getProduct().setProductID(null);
										else{
											composedOf3.getProduct().setProductID(Util.getIntegerValue(nestedRow.getAs("ProductID")));
											toAdd3 = true;					
											}
									}
									// 	attribute Product.productName for field ProductName			
									nestedRow =  r3;
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ProductName")) {
										if(nestedRow.getAs("ProductName")==null)
											composedOf3.getProduct().setProductName(null);
										else{
											composedOf3.getProduct().setProductName(Util.getStringValue(nestedRow.getAs("ProductName")));
											toAdd3 = true;					
											}
									}
									// Field 'UnitPrice' is mapped to 'unitPrice' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("UnitPrice")) {
										if(nestedRow.getAs("UnitPrice")==null)
											composedOf3.setUnitPrice(null);
										else{
											composedOf3.setUnitPrice(Util.getDoubleValue(nestedRow.getAs("UnitPrice")));
											toAdd3 = true;					
											}
										}
									// Field 'Quantity' is mapped to 'quantity' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Quantity")) {
										if(nestedRow.getAs("Quantity")==null)
											composedOf3.setQuantity(null);
										else{
											composedOf3.setQuantity(Util.getIntegerValue(nestedRow.getAs("Quantity")));
											toAdd3 = true;					
											}
										}
									// Field 'Discount' is mapped to 'discount' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Discount")) {
										if(nestedRow.getAs("Discount")==null)
											composedOf3.setDiscount(null);
										else{
											composedOf3.setDiscount(Util.getDoubleValue(nestedRow.getAs("Discount")));
											toAdd3 = true;					
											}
										}
									if(toAdd3 && ((order_condition == null || order_refilter.booleanValue() || order_condition.evaluate(composedOf3.getOrder()))&&(product_condition == null || product_refilter.booleanValue() || product_condition.evaluate(composedOf3.getProduct())))) {
										if(!(composedOf3.getOrder().equals(new Order())) && !(composedOf3.getProduct().equals(new Product())))
											list_res.add(composedOf3);
										addedInList = true;
									} 
									if(addedInList)
										toAdd2 = false;
								}
							}
							
							if(toAdd2 && ((order_condition == null || order_refilter.booleanValue() || order_condition.evaluate(composedOf2.getOrder()))&&(product_condition == null || product_refilter.booleanValue() || product_condition.evaluate(composedOf2.getProduct())))) {
								if(!(composedOf2.getOrder().equals(new Order())) && !(composedOf2.getProduct().equals(new Product())))
									list_res.add(composedOf2);
								addedInList = true;
							} 
							if(addedInList)
								toAdd1 = false;
							
						}
					}
					
					if(toAdd1 ) {
						if(!(composedOf1.getOrder().equals(new Order())) && !(composedOf1.getProduct().equals(new Product())))
							list_res.add(composedOf1);
						addedInList = true;
					} 
					
					array1 = r1.getAs("orders");
					if(array1!= null) {
						for (int i2 = 0; i2 < array1.size(); i2++){
							Row r2 = (Row) array1.apply(i2);
							ComposedOf composedOf2 = (ComposedOf) composedOf1.clone();
							boolean toAdd2  = false;
							WrappedArray array2  = null;
							// 	attribute Order.orderID for field OrderID			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderID")) {
								if(nestedRow.getAs("OrderID")==null)
									composedOf2.getOrder().setOrderID(null);
								else{
									composedOf2.getOrder().setOrderID(Util.getIntegerValue(nestedRow.getAs("OrderID")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.orderDate for field OrderDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderDate")) {
								if(nestedRow.getAs("OrderDate")==null)
									composedOf2.getOrder().setOrderDate(null);
								else{
									composedOf2.getOrder().setOrderDate(Util.getLocalDateValue(nestedRow.getAs("OrderDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.requiredDate for field RequiredDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("RequiredDate")) {
								if(nestedRow.getAs("RequiredDate")==null)
									composedOf2.getOrder().setRequiredDate(null);
								else{
									composedOf2.getOrder().setRequiredDate(Util.getLocalDateValue(nestedRow.getAs("RequiredDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shippedDate for field ShippedDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShippedDate")) {
								if(nestedRow.getAs("ShippedDate")==null)
									composedOf2.getOrder().setShippedDate(null);
								else{
									composedOf2.getOrder().setShippedDate(Util.getLocalDateValue(nestedRow.getAs("ShippedDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.freight for field Freight			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Freight")) {
								if(nestedRow.getAs("Freight")==null)
									composedOf2.getOrder().setFreight(null);
								else{
									composedOf2.getOrder().setFreight(Util.getDoubleValue(nestedRow.getAs("Freight")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipName for field ShipName			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipName")) {
								if(nestedRow.getAs("ShipName")==null)
									composedOf2.getOrder().setShipName(null);
								else{
									composedOf2.getOrder().setShipName(Util.getStringValue(nestedRow.getAs("ShipName")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipAddress for field ShipAddress			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipAddress")) {
								if(nestedRow.getAs("ShipAddress")==null)
									composedOf2.getOrder().setShipAddress(null);
								else{
									composedOf2.getOrder().setShipAddress(Util.getStringValue(nestedRow.getAs("ShipAddress")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCity for field ShipCity			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCity")) {
								if(nestedRow.getAs("ShipCity")==null)
									composedOf2.getOrder().setShipCity(null);
								else{
									composedOf2.getOrder().setShipCity(Util.getStringValue(nestedRow.getAs("ShipCity")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipRegion for field ShipRegion			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipRegion")) {
								if(nestedRow.getAs("ShipRegion")==null)
									composedOf2.getOrder().setShipRegion(null);
								else{
									composedOf2.getOrder().setShipRegion(Util.getStringValue(nestedRow.getAs("ShipRegion")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipPostalCode for field ShipPostalCode			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipPostalCode")) {
								if(nestedRow.getAs("ShipPostalCode")==null)
									composedOf2.getOrder().setShipPostalCode(null);
								else{
									composedOf2.getOrder().setShipPostalCode(Util.getStringValue(nestedRow.getAs("ShipPostalCode")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCountry for field ShipCountry			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCountry")) {
								if(nestedRow.getAs("ShipCountry")==null)
									composedOf2.getOrder().setShipCountry(null);
								else{
									composedOf2.getOrder().setShipCountry(Util.getStringValue(nestedRow.getAs("ShipCountry")));
									toAdd2 = true;					
									}
							}
							array2 = r2.getAs("products");
							if(array2!= null) {
								for (int i3 = 0; i3 < array2.size(); i3++){
									Row r3 = (Row) array2.apply(i3);
									ComposedOf composedOf3 = (ComposedOf) composedOf2.clone();
									boolean toAdd3  = false;
									WrappedArray array3  = null;
									// 	attribute Product.productID for field ProductID			
									nestedRow =  r3;
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ProductID")) {
										if(nestedRow.getAs("ProductID")==null)
											composedOf3.getProduct().setProductID(null);
										else{
											composedOf3.getProduct().setProductID(Util.getIntegerValue(nestedRow.getAs("ProductID")));
											toAdd3 = true;					
											}
									}
									// 	attribute Product.productName for field ProductName			
									nestedRow =  r3;
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ProductName")) {
										if(nestedRow.getAs("ProductName")==null)
											composedOf3.getProduct().setProductName(null);
										else{
											composedOf3.getProduct().setProductName(Util.getStringValue(nestedRow.getAs("ProductName")));
											toAdd3 = true;					
											}
									}
									// Field 'UnitPrice' is mapped to 'unitPrice' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("UnitPrice")) {
										if(nestedRow.getAs("UnitPrice")==null)
											composedOf3.setUnitPrice(null);
										else{
											composedOf3.setUnitPrice(Util.getDoubleValue(nestedRow.getAs("UnitPrice")));
											toAdd3 = true;					
											}
										}
									// Field 'Quantity' is mapped to 'quantity' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Quantity")) {
										if(nestedRow.getAs("Quantity")==null)
											composedOf3.setQuantity(null);
										else{
											composedOf3.setQuantity(Util.getIntegerValue(nestedRow.getAs("Quantity")));
											toAdd3 = true;					
											}
										}
									// Field 'Discount' is mapped to 'discount' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Discount")) {
										if(nestedRow.getAs("Discount")==null)
											composedOf3.setDiscount(null);
										else{
											composedOf3.setDiscount(Util.getDoubleValue(nestedRow.getAs("Discount")));
											toAdd3 = true;					
											}
										}
									if(toAdd3 && ((order_condition == null || order_refilter.booleanValue() || order_condition.evaluate(composedOf3.getOrder()))&&(product_condition == null || product_refilter.booleanValue() || product_condition.evaluate(composedOf3.getProduct())))) {
										if(!(composedOf3.getOrder().equals(new Order())) && !(composedOf3.getProduct().equals(new Product())))
											list_res.add(composedOf3);
										addedInList = true;
									} 
									if(addedInList)
										toAdd2 = false;
								}
							}
							
							if(toAdd2 && ((order_condition == null || order_refilter.booleanValue() || order_condition.evaluate(composedOf2.getOrder()))&&(product_condition == null || product_refilter.booleanValue() || product_condition.evaluate(composedOf2.getProduct())))) {
								if(!(composedOf2.getOrder().equals(new Order())) && !(composedOf2.getProduct().equals(new Product())))
									list_res.add(composedOf2);
								addedInList = true;
							} 
							if(addedInList)
								toAdd1 = false;
							
						}
					}
					
					if(toAdd1 ) {
						if(!(composedOf1.getOrder().equals(new Order())) && !(composedOf1.getProduct().equals(new Product())))
							list_res.add(composedOf1);
						addedInList = true;
					} 
					
					
					
					return list_res.iterator();
		
			}, Encoders.bean(ComposedOf.class));
			// TODO drop duplicates based on roles ids
			res= res.dropDuplicates(new String[]{"order.orderID","product.productID"});
			return res;
	}
	
	// method accessing the embedded object products mapped to role product
	public Dataset<ComposedOf> getComposedOfListInmyMongoDBCustomersordersproducts(Condition<ProductAttribute> product_condition, Condition<OrderAttribute> order_condition, MutableBoolean product_refilter, MutableBoolean order_refilter){	
			List<String> bsons = new ArrayList<String>();
			String bson = null;
			bson = OrderServiceImpl.getBSONMatchQueryInCustomersFromMyMongoDB(order_condition ,order_refilter);
			if(bson != null)
				bsons.add("{" + bson + "}");
			bson = ProductServiceImpl.getBSONMatchQueryInCustomersFromMyMongoDB(product_condition ,product_refilter);
			if(bson != null)
				bsons.add("{" + bson + "}");
		
			String bsonQuery = bsons.size() == 0 ? null : "{$match: { $and: [" + String.join(",", bsons) + "] }}";
		
			Dataset<Row> dataset = dbconnection.SparkConnectionMgr.getDatasetFromMongoDB("myMongoDB", "Customers", bsonQuery);
		
			Dataset<ComposedOf> res = dataset.flatMap((FlatMapFunction<Row, ComposedOf>) r -> {
					List<ComposedOf> list_res = new ArrayList<ComposedOf>();
					Integer groupIndex = null;
					String regex = null;
					String value = null;
					Pattern p = null;
					Matcher m = null;
					boolean matches = false;
					Row nestedRow = null;
		
					boolean addedInList = false;
					Row r1 = r;
					ComposedOf composedOf1 = new ComposedOf();
					composedOf1.setOrder(new Order());
					composedOf1.setProduct(new Product());
					
					boolean toAdd1  = false;
					WrappedArray array1  = null;
					array1 = r1.getAs("orders");
					if(array1!= null) {
						for (int i2 = 0; i2 < array1.size(); i2++){
							Row r2 = (Row) array1.apply(i2);
							ComposedOf composedOf2 = (ComposedOf) composedOf1.clone();
							boolean toAdd2  = false;
							WrappedArray array2  = null;
							// 	attribute Order.orderID for field OrderID			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderID")) {
								if(nestedRow.getAs("OrderID")==null)
									composedOf2.getOrder().setOrderID(null);
								else{
									composedOf2.getOrder().setOrderID(Util.getIntegerValue(nestedRow.getAs("OrderID")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.orderDate for field OrderDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderDate")) {
								if(nestedRow.getAs("OrderDate")==null)
									composedOf2.getOrder().setOrderDate(null);
								else{
									composedOf2.getOrder().setOrderDate(Util.getLocalDateValue(nestedRow.getAs("OrderDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.requiredDate for field RequiredDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("RequiredDate")) {
								if(nestedRow.getAs("RequiredDate")==null)
									composedOf2.getOrder().setRequiredDate(null);
								else{
									composedOf2.getOrder().setRequiredDate(Util.getLocalDateValue(nestedRow.getAs("RequiredDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shippedDate for field ShippedDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShippedDate")) {
								if(nestedRow.getAs("ShippedDate")==null)
									composedOf2.getOrder().setShippedDate(null);
								else{
									composedOf2.getOrder().setShippedDate(Util.getLocalDateValue(nestedRow.getAs("ShippedDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.freight for field Freight			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Freight")) {
								if(nestedRow.getAs("Freight")==null)
									composedOf2.getOrder().setFreight(null);
								else{
									composedOf2.getOrder().setFreight(Util.getDoubleValue(nestedRow.getAs("Freight")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipName for field ShipName			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipName")) {
								if(nestedRow.getAs("ShipName")==null)
									composedOf2.getOrder().setShipName(null);
								else{
									composedOf2.getOrder().setShipName(Util.getStringValue(nestedRow.getAs("ShipName")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipAddress for field ShipAddress			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipAddress")) {
								if(nestedRow.getAs("ShipAddress")==null)
									composedOf2.getOrder().setShipAddress(null);
								else{
									composedOf2.getOrder().setShipAddress(Util.getStringValue(nestedRow.getAs("ShipAddress")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCity for field ShipCity			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCity")) {
								if(nestedRow.getAs("ShipCity")==null)
									composedOf2.getOrder().setShipCity(null);
								else{
									composedOf2.getOrder().setShipCity(Util.getStringValue(nestedRow.getAs("ShipCity")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipRegion for field ShipRegion			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipRegion")) {
								if(nestedRow.getAs("ShipRegion")==null)
									composedOf2.getOrder().setShipRegion(null);
								else{
									composedOf2.getOrder().setShipRegion(Util.getStringValue(nestedRow.getAs("ShipRegion")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipPostalCode for field ShipPostalCode			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipPostalCode")) {
								if(nestedRow.getAs("ShipPostalCode")==null)
									composedOf2.getOrder().setShipPostalCode(null);
								else{
									composedOf2.getOrder().setShipPostalCode(Util.getStringValue(nestedRow.getAs("ShipPostalCode")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCountry for field ShipCountry			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCountry")) {
								if(nestedRow.getAs("ShipCountry")==null)
									composedOf2.getOrder().setShipCountry(null);
								else{
									composedOf2.getOrder().setShipCountry(Util.getStringValue(nestedRow.getAs("ShipCountry")));
									toAdd2 = true;					
									}
							}
							array2 = r2.getAs("products");
							if(array2!= null) {
								for (int i3 = 0; i3 < array2.size(); i3++){
									Row r3 = (Row) array2.apply(i3);
									ComposedOf composedOf3 = (ComposedOf) composedOf2.clone();
									boolean toAdd3  = false;
									WrappedArray array3  = null;
									// 	attribute Product.productID for field ProductID			
									nestedRow =  r3;
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ProductID")) {
										if(nestedRow.getAs("ProductID")==null)
											composedOf3.getProduct().setProductID(null);
										else{
											composedOf3.getProduct().setProductID(Util.getIntegerValue(nestedRow.getAs("ProductID")));
											toAdd3 = true;					
											}
									}
									// 	attribute Product.productName for field ProductName			
									nestedRow =  r3;
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ProductName")) {
										if(nestedRow.getAs("ProductName")==null)
											composedOf3.getProduct().setProductName(null);
										else{
											composedOf3.getProduct().setProductName(Util.getStringValue(nestedRow.getAs("ProductName")));
											toAdd3 = true;					
											}
									}
									// Field 'UnitPrice' is mapped to 'unitPrice' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("UnitPrice")) {
										if(nestedRow.getAs("UnitPrice")==null)
											composedOf3.setUnitPrice(null);
										else{
											composedOf3.setUnitPrice(Util.getDoubleValue(nestedRow.getAs("UnitPrice")));
											toAdd3 = true;					
											}
										}
									// Field 'Quantity' is mapped to 'quantity' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Quantity")) {
										if(nestedRow.getAs("Quantity")==null)
											composedOf3.setQuantity(null);
										else{
											composedOf3.setQuantity(Util.getIntegerValue(nestedRow.getAs("Quantity")));
											toAdd3 = true;					
											}
										}
									// Field 'Discount' is mapped to 'discount' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Discount")) {
										if(nestedRow.getAs("Discount")==null)
											composedOf3.setDiscount(null);
										else{
											composedOf3.setDiscount(Util.getDoubleValue(nestedRow.getAs("Discount")));
											toAdd3 = true;					
											}
										}
									if(toAdd3 && ((order_condition == null || order_refilter.booleanValue() || order_condition.evaluate(composedOf3.getOrder()))&&(product_condition == null || product_refilter.booleanValue() || product_condition.evaluate(composedOf3.getProduct())))) {
										if(!(composedOf3.getOrder().equals(new Order())) && !(composedOf3.getProduct().equals(new Product())))
											list_res.add(composedOf3);
										addedInList = true;
									} 
									if(addedInList)
										toAdd2 = false;
								}
							}
							
							if(toAdd2 && ((order_condition == null || order_refilter.booleanValue() || order_condition.evaluate(composedOf2.getOrder()))&&(product_condition == null || product_refilter.booleanValue() || product_condition.evaluate(composedOf2.getProduct())))) {
								if(!(composedOf2.getOrder().equals(new Order())) && !(composedOf2.getProduct().equals(new Product())))
									list_res.add(composedOf2);
								addedInList = true;
							} 
							if(addedInList)
								toAdd1 = false;
							
						}
					}
					
					if(toAdd1 ) {
						if(!(composedOf1.getOrder().equals(new Order())) && !(composedOf1.getProduct().equals(new Product())))
							list_res.add(composedOf1);
						addedInList = true;
					} 
					
					array1 = r1.getAs("orders");
					if(array1!= null) {
						for (int i2 = 0; i2 < array1.size(); i2++){
							Row r2 = (Row) array1.apply(i2);
							ComposedOf composedOf2 = (ComposedOf) composedOf1.clone();
							boolean toAdd2  = false;
							WrappedArray array2  = null;
							// 	attribute Order.orderID for field OrderID			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderID")) {
								if(nestedRow.getAs("OrderID")==null)
									composedOf2.getOrder().setOrderID(null);
								else{
									composedOf2.getOrder().setOrderID(Util.getIntegerValue(nestedRow.getAs("OrderID")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.orderDate for field OrderDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("OrderDate")) {
								if(nestedRow.getAs("OrderDate")==null)
									composedOf2.getOrder().setOrderDate(null);
								else{
									composedOf2.getOrder().setOrderDate(Util.getLocalDateValue(nestedRow.getAs("OrderDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.requiredDate for field RequiredDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("RequiredDate")) {
								if(nestedRow.getAs("RequiredDate")==null)
									composedOf2.getOrder().setRequiredDate(null);
								else{
									composedOf2.getOrder().setRequiredDate(Util.getLocalDateValue(nestedRow.getAs("RequiredDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shippedDate for field ShippedDate			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShippedDate")) {
								if(nestedRow.getAs("ShippedDate")==null)
									composedOf2.getOrder().setShippedDate(null);
								else{
									composedOf2.getOrder().setShippedDate(Util.getLocalDateValue(nestedRow.getAs("ShippedDate")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.freight for field Freight			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Freight")) {
								if(nestedRow.getAs("Freight")==null)
									composedOf2.getOrder().setFreight(null);
								else{
									composedOf2.getOrder().setFreight(Util.getDoubleValue(nestedRow.getAs("Freight")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipName for field ShipName			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipName")) {
								if(nestedRow.getAs("ShipName")==null)
									composedOf2.getOrder().setShipName(null);
								else{
									composedOf2.getOrder().setShipName(Util.getStringValue(nestedRow.getAs("ShipName")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipAddress for field ShipAddress			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipAddress")) {
								if(nestedRow.getAs("ShipAddress")==null)
									composedOf2.getOrder().setShipAddress(null);
								else{
									composedOf2.getOrder().setShipAddress(Util.getStringValue(nestedRow.getAs("ShipAddress")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCity for field ShipCity			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCity")) {
								if(nestedRow.getAs("ShipCity")==null)
									composedOf2.getOrder().setShipCity(null);
								else{
									composedOf2.getOrder().setShipCity(Util.getStringValue(nestedRow.getAs("ShipCity")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipRegion for field ShipRegion			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipRegion")) {
								if(nestedRow.getAs("ShipRegion")==null)
									composedOf2.getOrder().setShipRegion(null);
								else{
									composedOf2.getOrder().setShipRegion(Util.getStringValue(nestedRow.getAs("ShipRegion")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipPostalCode for field ShipPostalCode			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipPostalCode")) {
								if(nestedRow.getAs("ShipPostalCode")==null)
									composedOf2.getOrder().setShipPostalCode(null);
								else{
									composedOf2.getOrder().setShipPostalCode(Util.getStringValue(nestedRow.getAs("ShipPostalCode")));
									toAdd2 = true;					
									}
							}
							// 	attribute Order.shipCountry for field ShipCountry			
							nestedRow =  r2;
							if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ShipCountry")) {
								if(nestedRow.getAs("ShipCountry")==null)
									composedOf2.getOrder().setShipCountry(null);
								else{
									composedOf2.getOrder().setShipCountry(Util.getStringValue(nestedRow.getAs("ShipCountry")));
									toAdd2 = true;					
									}
							}
							array2 = r2.getAs("products");
							if(array2!= null) {
								for (int i3 = 0; i3 < array2.size(); i3++){
									Row r3 = (Row) array2.apply(i3);
									ComposedOf composedOf3 = (ComposedOf) composedOf2.clone();
									boolean toAdd3  = false;
									WrappedArray array3  = null;
									// 	attribute Product.productID for field ProductID			
									nestedRow =  r3;
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ProductID")) {
										if(nestedRow.getAs("ProductID")==null)
											composedOf3.getProduct().setProductID(null);
										else{
											composedOf3.getProduct().setProductID(Util.getIntegerValue(nestedRow.getAs("ProductID")));
											toAdd3 = true;					
											}
									}
									// 	attribute Product.productName for field ProductName			
									nestedRow =  r3;
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("ProductName")) {
										if(nestedRow.getAs("ProductName")==null)
											composedOf3.getProduct().setProductName(null);
										else{
											composedOf3.getProduct().setProductName(Util.getStringValue(nestedRow.getAs("ProductName")));
											toAdd3 = true;					
											}
									}
									// Field 'UnitPrice' is mapped to 'unitPrice' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("UnitPrice")) {
										if(nestedRow.getAs("UnitPrice")==null)
											composedOf3.setUnitPrice(null);
										else{
											composedOf3.setUnitPrice(Util.getDoubleValue(nestedRow.getAs("UnitPrice")));
											toAdd3 = true;					
											}
										}
									// Field 'Quantity' is mapped to 'quantity' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Quantity")) {
										if(nestedRow.getAs("Quantity")==null)
											composedOf3.setQuantity(null);
										else{
											composedOf3.setQuantity(Util.getIntegerValue(nestedRow.getAs("Quantity")));
											toAdd3 = true;					
											}
										}
									// Field 'Discount' is mapped to 'discount' attribute of rel 'composedOf' 
									if(nestedRow != null && Arrays.asList(nestedRow.schema().fieldNames()).contains("Discount")) {
										if(nestedRow.getAs("Discount")==null)
											composedOf3.setDiscount(null);
										else{
											composedOf3.setDiscount(Util.getDoubleValue(nestedRow.getAs("Discount")));
											toAdd3 = true;					
											}
										}
									if(toAdd3 && ((order_condition == null || order_refilter.booleanValue() || order_condition.evaluate(composedOf3.getOrder()))&&(product_condition == null || product_refilter.booleanValue() || product_condition.evaluate(composedOf3.getProduct())))) {
										if(!(composedOf3.getOrder().equals(new Order())) && !(composedOf3.getProduct().equals(new Product())))
											list_res.add(composedOf3);
										addedInList = true;
									} 
									if(addedInList)
										toAdd2 = false;
								}
							}
							
							if(toAdd2 && ((order_condition == null || order_refilter.booleanValue() || order_condition.evaluate(composedOf2.getOrder()))&&(product_condition == null || product_refilter.booleanValue() || product_condition.evaluate(composedOf2.getProduct())))) {
								if(!(composedOf2.getOrder().equals(new Order())) && !(composedOf2.getProduct().equals(new Product())))
									list_res.add(composedOf2);
								addedInList = true;
							} 
							if(addedInList)
								toAdd1 = false;
							
						}
					}
					
					if(toAdd1 ) {
						if(!(composedOf1.getOrder().equals(new Order())) && !(composedOf1.getProduct().equals(new Product())))
							list_res.add(composedOf1);
						addedInList = true;
					} 
					
					
					
					return list_res.iterator();
		
			}, Encoders.bean(ComposedOf.class));
			// TODO drop duplicates based on roles ids
			res= res.dropDuplicates(new String[]{"order.orderID","product.productID"});
			return res;
	}
	
	public Dataset<ComposedOf> getComposedOfList(
		Condition<OrderAttribute> order_condition,
		Condition<ProductAttribute> product_condition,
		Condition<ComposedOfAttribute> composedOf_condition
	){
		ComposedOfServiceImpl composedOfService = this;
		OrderService orderService = new OrderServiceImpl();  
		ProductService productService = new ProductServiceImpl();
		MutableBoolean order_refilter = new MutableBoolean(false);
		List<Dataset<ComposedOf>> datasetsPOJO = new ArrayList<Dataset<ComposedOf>>();
		boolean all_already_persisted = false;
		MutableBoolean product_refilter = new MutableBoolean(false);
		MutableBoolean composedOf_refilter = new MutableBoolean(false);
		org.apache.spark.sql.Column joinCondition = null;
	
		
		Dataset<ComposedOf> res_composedOf_order;
		Dataset<Order> res_Order;
		// Role 'order' mapped to EmbeddedObject 'orders' - 'Product' containing 'Order'
		product_refilter = new MutableBoolean(false);
		res_composedOf_order = composedOfService.getComposedOfListInmyMongoDBCustomersorders(order_condition, product_condition, order_refilter, product_refilter);
	 	
		datasetsPOJO.add(res_composedOf_order);
		// Role 'product' mapped to EmbeddedObject 'products' 'Order' containing 'Product' 
		product_refilter = new MutableBoolean(false);
		res_composedOf_order = composedOfService.getComposedOfListInmyMongoDBCustomersordersproducts(product_condition, order_condition, product_refilter, order_refilter);
		
		datasetsPOJO.add(res_composedOf_order);
		
		
		//Join datasets or return 
		Dataset<ComposedOf> res = fullOuterJoinsComposedOf(datasetsPOJO);
		if(res == null)
			return null;
	
		Dataset<Order> lonelyOrder = null;
		Dataset<Product> lonelyProduct = null;
		
		List<Dataset<Order>> lonelyorderList = new ArrayList<Dataset<Order>>();
		lonelyorderList.add(orderService.getOrderListInOrderShippingFromMyRedis(order_condition, new MutableBoolean(false)));
		lonelyOrder = OrderService.fullOuterJoinsOrder(lonelyorderList);
		if(lonelyOrder != null) {
			res = fullLeftOuterJoinBetweenComposedOfAndOrder(res, lonelyOrder);
		}	
	
		List<Dataset<Product>> lonelyproductList = new ArrayList<Dataset<Product>>();
		lonelyproductList.add(productService.getProductListInProductsFromReldata(product_condition, new MutableBoolean(false)));
		lonelyproductList.add(productService.getProductListInSuppliersFromMyMongoDB(product_condition, new MutableBoolean(false)));
		lonelyProduct = ProductService.fullOuterJoinsProduct(lonelyproductList);
		if(lonelyProduct != null) {
			res = fullLeftOuterJoinBetweenComposedOfAndProduct(res, lonelyProduct);
		}	
	
		
		if(order_refilter.booleanValue() || product_refilter.booleanValue() || composedOf_refilter.booleanValue())
			res = res.filter((FilterFunction<ComposedOf>) r -> (order_condition == null || order_condition.evaluate(r.getOrder())) && (product_condition == null || product_condition.evaluate(r.getProduct())) && (composedOf_condition == null || composedOf_condition.evaluate(r)));
		
	
		return res;
	
	}
	
	public Dataset<ComposedOf> getComposedOfListByOrderCondition(
		Condition<OrderAttribute> order_condition
	){
		return getComposedOfList(order_condition, null, null);
	}
	
	public Dataset<ComposedOf> getComposedOfListByOrder(Order order) {
		Condition<OrderAttribute> cond = null;
		cond = Condition.simple(OrderAttribute.orderID, Operator.EQUALS, order.getOrderID());
		Dataset<ComposedOf> res = getComposedOfListByOrderCondition(cond);
	return res;
	}
	public Dataset<ComposedOf> getComposedOfListByProductCondition(
		Condition<ProductAttribute> product_condition
	){
		return getComposedOfList(null, product_condition, null);
	}
	
	public Dataset<ComposedOf> getComposedOfListByProduct(Product product) {
		Condition<ProductAttribute> cond = null;
		cond = Condition.simple(ProductAttribute.productID, Operator.EQUALS, product.getProductID());
		Dataset<ComposedOf> res = getComposedOfListByProductCondition(cond);
	return res;
	}
	
	public Dataset<ComposedOf> getComposedOfListByComposedOfCondition(
		Condition<ComposedOfAttribute> composedOf_condition
	){
		return getComposedOfList(null, null, composedOf_condition);
	}
	
	public void insertComposedOf(ComposedOf composedOf){
		//Link entities in join structures.
		// Update embedded structures mapped to non mandatory roles.
		insertComposedOfInEmbeddedStructCustomersInMyMongoDB(composedOf);
		// Update ref fields mapped to non mandatory roles. 
	}
	
	
	public 	boolean insertComposedOfInEmbeddedStructCustomersInMyMongoDB(ComposedOf composedOf){
	 	// Rel 'composedOf' Insert in embedded structure 'Customers'
	
		Order order = composedOf.getOrder();
		Product product = composedOf.getProduct();
		Bson filter= new Document();
		Bson updateOp;
		String addToSet;
		List<String> fieldName= new ArrayList();
		List<Bson> arrayFilterCond = new ArrayList();
		Document docorders_1 = new Document();
		
		// level 1 ascending
		 // Rel attributes. Only simple ones.  
		docorders_1.append("UnitPrice",composedOf.getUnitPrice());
		docorders_1.append("Quantity",composedOf.getQuantity());
		docorders_1.append("Discount",composedOf.getDiscount());
		filter = eq("OrderID",order.getOrderID());
		updateOp = addToSet("orders", docorders_1);
		DBConnectionMgr.update(filter, updateOp, "Customers", "myMongoDB");					
		return true;
	}
	
	
	
	
	public void updateComposedOfList(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition,
		conditions.SetClause<conditions.ComposedOfAttribute> set
	){
		//TODO
	}
	
	public void updateComposedOfListByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.SetClause<conditions.ComposedOfAttribute> set
	){
		updateComposedOfList(order_condition, null, null, set);
	}
	
	public void updateComposedOfListByOrder(pojo.Order order, conditions.SetClause<conditions.ComposedOfAttribute> set) {
		// TODO using id for selecting
		return;
	}
	public void updateComposedOfListByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.SetClause<conditions.ComposedOfAttribute> set
	){
		updateComposedOfList(null, product_condition, null, set);
	}
	
	public void updateComposedOfListByProduct(pojo.Product product, conditions.SetClause<conditions.ComposedOfAttribute> set) {
		// TODO using id for selecting
		return;
	}
	
	public void updateComposedOfListByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition,
		conditions.SetClause<conditions.ComposedOfAttribute> set
	){
		updateComposedOfList(null, null, composedOf_condition, set);
	}
	
	public void deleteComposedOfList(
		conditions.Condition<conditions.OrderAttribute> order_condition,
		conditions.Condition<conditions.ProductAttribute> product_condition,
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition){
			//TODO
		}
	
	public void deleteComposedOfListByOrderCondition(
		conditions.Condition<conditions.OrderAttribute> order_condition
	){
		deleteComposedOfList(order_condition, null, null);
	}
	
	public void deleteComposedOfListByOrder(pojo.Order order) {
		// TODO using id for selecting
		return;
	}
	public void deleteComposedOfListByProductCondition(
		conditions.Condition<conditions.ProductAttribute> product_condition
	){
		deleteComposedOfList(null, product_condition, null);
	}
	
	public void deleteComposedOfListByProduct(pojo.Product product) {
		// TODO using id for selecting
		return;
	}
	
	public void deleteComposedOfListByComposedOfCondition(
		conditions.Condition<conditions.ComposedOfAttribute> composedOf_condition
	){
		deleteComposedOfList(null, null, composedOf_condition);
	}
		
}
