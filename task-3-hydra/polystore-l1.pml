// Group 1: Timothy Holt, Marco Raglianti

conceptual schema cs {
	entity type Employee {
    	employeeID : int,
    	lastName: string,
    	firstName : string,
    	title : string,
    	titleOfCourtesy : string,
    	birthDate : date,	// should be datetime from reldata reversed but with time always 0:00:00
    	hireDate : date,	// should be datetime from reldata reversed but with time always 0:00:00
    	address : string,
    	city : string,
    	region : string,
    	postalCode : string,
    	country : string,
    	homePhone : string,
    	extension : string,
    	photo : blob,
    	notes : text,
    	photoPath : string,
    	salary : float
		identifier { employeeID }
	}

	entity type Customer {
		address : string,
		city : string,
		companyName : string,
		contactName : string,
		contactTitle : string,
		country : string,
		customerID : string,
		fax [0-1] : string,
		phone : string, 
		postalCode : string,
		region [0-1] : string
		identifier { customerID }
	}
	
	entity type Order {
		orderID : int,
		orderDate : date,
		requiredDate : date,
		shippedDate : date,
		freight : float,
		shipName : string,
		shipAddress : string,
		shipCity : string,
		shipRegion [0-1] : string,
		shipPostalCode : string,
		shipCountry : string
		identifier { orderID }
	}
    
	entity type Product {
    	productID : int,
    	productName : string,
    	supplierRef : int,
    	quantityPerUnit : string,
    	unitPrice : float,
    	unitsInStock : int,
    	unitsOnOrder : int,
    	reorderLevel : int,
    	discontinued : bool
    	identifier { productID }
	}
	
	entity type Supplier {
		supplierID : int,
		address : string,
		city : string,
		companyName : string,
		contactName : string, 
		contactTitle : string,
		country : string,
		fax [0-1] : string,
		homePage [0-1] : string,
		phone : string,
		postalCode : string,
		region [0-1] : string
		identifier { supplierID }
	}
	

	relationship type bought {
		boughtOrder[1] : Order,
		customer[0-N] : Customer
    }
    relationship type sold {
    	order[1] : Order,
    	employee[0-N] : Employee
    }
    relationship type composedOf {
    	order[1-N] : Order,
    	product[0-N] : Product,
    	unitPrice : float,
		quantity : int,
		discount : float	// seems unused so far, always 0
    }
    relationship type supplies {
    	product[1] : Product,
    	supplier[0-N] : Supplier
    }
}


physical schemas {
	relational schema reldata : reldata {
		table Employees {
			columns {
			    EmployeeID,
			    LastName,
	    		FirstName,
			    Title,
			    TitleOfCourtesy,
			    BirthDate,
			    HireDate,
			    Address,
			    City,
			    Region,
			    PostalCode,
			    Country,
			    HomePhone,
			    Extension,
			    Photo,
			    Notes,
			    PhotoPath,
			    Salary
			}
			index { LastName }
			index { PostalCode }
		}


		table Products {
			columns {
				ProductID,
			    ProductName,
    			SupplierRef,
			    QuantityPerUnit,
			    UnitPrice,
			    UnitsInStock,
			    UnitsOnOrder,
			    ReorderLevel,
			    Discontinued
		    }
		    references {
		    	supplierRef : SupplierRef -> myMongoDB.Suppliers.SupplierID
		    }
			index { ProductName }
    	}
	}

	document schema myMongoDB : myMongoDB {
		collection Customers {
			fields {
				CustomerID,
				Address,
				City,
				CompanyName,
				ContactName,
				ContactTitle,
				Country,
				Fax,
				Phone,
				PostalCode,
				Region,
				orders[0-N] {
					OrderID,
					OrderDate,
					RequiredDate,
					ShippedDate,
					Freight,
					ShipName,
					ShipAddress,
					ShipCity,
					ShipRegion,
					ShipPostalCode,
					ShipCountry,
					products[0-N] {
	    				ProductID,
			    		ProductName,
			    		UnitPrice,
			    		Quantity,
			    		Discount	
					}
					
					
				}
			}
		}
		
		collection Suppliers {
			fields {
				SupplierID,
				Address,
				City,
				CompanyName,
				ContactName,
				ContactTitle,
				Country,
				Fax,
				HomePage,
				Phone,
				PostalCode,
				Region,
				products[0-N]
			}
		}
	}
	key value schema myRedis : myRedis {
		kvpairs orderShipping {
			key : "ORDER:"[orderid],
			value : hash {
				OrderDate,
				RequiredDate,
				ShippedDate,
				Freight,
				ShipName,
				ShipAddress,
				ShipCity,
				ShipRegion,
				ShipPostalCode,
				ShipCountry,
				CustomerRef,
				EmployeeRef
			}
			references { 
				customerRef : CustomerRef -> myMongoDB.Customers.CustomerID
				employeeRef : EmployeeRef -> reldata.Employees.EmployeeID
			}
		}
		
	}
}

mapping rules {
	cs.Employee(employeeID, lastName, firstName, title, titleOfCourtesy, birthDate,
		hireDate, address, city, region, postalCode, country, homePhone, extension,
		photo, notes, photoPath, salary)
	-> reldata.Employees(EmployeeID, LastName, FirstName, Title, TitleOfCourtesy, BirthDate,
		HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension,
		Photo, Notes, PhotoPath, Salary),
	
	cs.Customer(customerID, address, city, companyName, contactName, contactTitle, country,
		fax, phone, postalCode, region)
	-> myMongoDB.Customers(CustomerID, Address, City, CompanyName, ContactName, ContactTitle, Country,
		Fax, Phone, PostalCode, Region),
		
	cs.Order(orderID, orderDate, requiredDate, shippedDate, freight, shipName, shipAddress, shipCity,
		shipRegion, shipPostalCode, shipCountry)
	-> myRedis.orderShipping(orderid, OrderDate, RequiredDate, ShippedDate, Freight, ShipName, ShipAddress, ShipCity,
		ShipRegion, ShipPostalCode, ShipCountry),
	
	cs.Order(orderID, orderDate, requiredDate, shippedDate, freight, shipName, shipAddress, shipCity,
		shipRegion, shipPostalCode, shipCountry)
	-> myMongoDB.Customers.orders(OrderID, OrderDate, RequiredDate, ShippedDate, Freight, ShipName, ShipAddress, ShipCity,
		ShipRegion, ShipPostalCode, ShipCountry),
	
	cs.Product(productID, productName, supplierRef, quantityPerUnit, unitPrice, unitsInStock,
		unitsOnOrder, reorderLevel, discontinued)
	-> reldata.Products(ProductID, ProductName, SupplierRef, QuantityPerUnit, UnitPrice, UnitsInStock,
		UnitsOnOrder, ReorderLevel, Discontinued),
	
	cs.Supplier(supplierID, address, city, companyName, contactName, contactTitle, country,
		fax, homePage, phone, postalCode, region)
	-> myMongoDB.Suppliers(SupplierID, Address, City, CompanyName, ContactName, ContactTitle, Country,
		Fax, HomePage, Phone, PostalCode, Region),
		
	cs.Product(productID) -> myMongoDB.Suppliers(products),

	cs.bought.boughtOrder -> myRedis.orderShipping.customerRef,
	cs.bought.customer -> myMongoDB.Customers.orders(),
	
	cs.sold.order -> myRedis.orderShipping.employeeRef,
	
	cs.supplies.product -> reldata.Products.supplierRef,
	
	cs.Product(productID, productName) -> myMongoDB.Customers.orders.products(ProductID, ProductName),

	
	cs.composedOf.order -> myMongoDB.Customers.orders(),
	cs.composedOf.product -> myMongoDB.Customers.orders.products(),
	rel : cs.composedOf(unitPrice, quantity, discount) -> myMongoDB.Customers.orders.products(UnitPrice, Quantity, Discount)
}

databases {
	mysql reldata {
		host: "hydra.unamurcs.be"
		port: 33062
		dbname : "reldata"
		password : "password"
		login : "root"
	}
	
	redis myRedis {
		host:"hydra.unamurcs.be"
		port:63792
		dbname : "myRedis"
		password : "password"
		login : "root"
	}
	
	mongodb myMongoDB {
		host:"hydra.unamurcs.be"
		port: 27012
		dbname : "myMongoDB"
	}

}
	