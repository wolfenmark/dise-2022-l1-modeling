package pojo;

public class Product {
	public Integer ProductID;
	public String ProductName;
	public Integer SupplierRef;
	public String QuantityPerUnit;
	public Float ProductUnitPrice;
	public Integer UnitsInStock;
	public Integer UnitsOnOrder;
	public Integer ReorderLevel;
	public Boolean Discontinued;
	
	public String toString() {
		String out = "ProductID: " + ProductID + ", " +
				"ProductName: " + ProductName + ", " +
				"SupplierRef: " + SupplierRef + ", " +
				"QuantityPerUnit: " + QuantityPerUnit + ", " +
				"ProductUnitPrice: " + ProductUnitPrice + ", " +
				"UnitsInStock: " + UnitsInStock + ", " +
				"UnitsOnOrder: " + UnitsOnOrder + ", " +
				"ReorderLevel: " + ReorderLevel + ", " +
				"Discontinued: " + Discontinued;
		return out;
	}
}
