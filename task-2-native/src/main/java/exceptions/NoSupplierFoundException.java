/**
 * 
 */
package exceptions;

/**
 * @author wolfenmark and tabholt
 *
 */
public class NoSupplierFoundException extends Exception {
	public NoSupplierFoundException(String error) {
		super(error);
	}
}
